<?php
/*
  DATABASE CONNECTION
  FORMAT: type:user:pass@server/dbname
  HISTORY:
    2012-12-13 extracted from site.php
    2019-08-14 created example from live config file
*/

$ksDBserver	= "localhost";
$ksDBuser	= "vc-bot";
$ksDBpass	= '';
$ksDBschema	= "vbzcart";
define('KS_DB_VBZCART','mysql:'.$ksDBuser.':'.$ksDBpass.'@'.$ksDBserver.'/'.$ksDBschema);
