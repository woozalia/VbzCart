<?php
/*
  PURPOSE: VbzCart index file
*/

$fErrLevel = 0
    | E_ALL
    | E_STRICT
    ;
error_reporting($fErrLevel);
if (!ini_get('display_errors')) {
    ini_set('display_errors', 1);
}

$kfpAcctRoot = dirname(__FILE__,2);
require($kfpAcctRoot.'/site/config/vbzcart/portable/start.php');
