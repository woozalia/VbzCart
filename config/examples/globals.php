<?php
class vcGlobalsFinal extends vcGlobals {

    // admin settings
    
    /*----
      RETURNS: ID of user who should have root permissions regardless of other permissions setting
      USAGE: normally, set to 1 when giving yourself admin powers at setup
    */
    public function GetUserID_forRoot() {
	return 1;	// first user is currently $deity
    }
    
    // TEXT

    /*----
      RETURNS: HTML-formatted message to appear at the top of all pages
      OVERRIDEABLE
    */
    public function GetMarkup_forSitewideTopMessage() {
	return <<<__END__

<table width=100% id="site-message" style="background: #660000; border: solid red 2px;">
  <tr>
    <td align=center>
      <span style="color: white; font-weight: normal; font-family: sans-serif;">
      This is a development instance of <a href="https://htyp.org/VbzCart">VbzCart</a>; no orders will be fulfilled,
      and please don't use actual credit card numbers.
      <br>Remember: <b>VbzCart &ndash; The Right Place for All Your Unfulfillment Needs!&trade;</b>
      </span>
    </td>
  </tr>
</table>
__END__;
    }
    public function GetText_SiteName() {
	return 'vbz.net';
    }
    public function GetText_SiteName_short() {
	return 'VBZ';
    }
    public function GetAppKeyString() {	// TODO: rename to GetText_AppKey(), ocelot
	return 'vbzcart';
    }
    public function GetText_EmailDomain() {
	return 'vbz.net';
    }
    // format for converting order sequence to a string
    protected function GetOrderNumberFormat() : string {
	return '%05u';
    }
    // order number prefix for current batch of orders
    protected function GetOrderNumberPrefix() : string {
	return 'Iy';
    }
    // order number prefix sorting order, in SQL format
    public function GetOrderNumberPrefixSorter() : string {
	return 'NULL';
    }
    protected function GetTopicNumberFormat() : string {
	return '%04u';
    }
    
    // EMAIL
    
    /*----
      RETURNS: template string for order-confirmation email sender
    */
    public function GetTemplate_forOrderEmailSender() : string {
	return
	  $this->GetText_SiteName()
	  .' order system <order-{{'
	  .$this->GetVarName_forOrderNumber()
	  .'}}@'
	  .$this->GetText_EmailDomain()
	  .'>'
	  ;
    }
    public function GetEmailAddress_forPrintout() : string {
	return 'orders-'.date('Y').'@'.$this->GetText_EmailDomain();
    }
    public function GetTemplate_forOrderEmailSubject() : string {
	return 'ORDER #{{'.$this->GetVarName_forOrderNumber().'}} at '.$this->GetText_SiteName();
    }
    /*----
      TODO: This should be replaced by a whole-email template (and eventually one for HTML email).
      WAS: KS_TPLT_ORDER_EMAIL_MSG_TOP
    */
    public function GetTemplate_EmailTopMessage() : string {
	$sSite = $this->GetText_SiteName();
	return <<<__END__
This is an automatic confirmation email from $sSite
to let you know that we have received your order.

If you have any questions, you may respond to this email
  or contact us by any of several methods listed here:
__END__
  ."\n".$this->GetWebPath_forContactPage()."\n\nORDER #{{ord-num}}"
	  ;
    }
    
    // FILE PATHS
    
    protected function GetFilePath_forSiteFolders() {
	global $kfpSiteFiles;
	return $kfpSiteFiles;
    }
     
    // WEB PATHS
    
    /*----
      RETURNS: the web path for a particular image repository, identified by $sRepo
      NOTE: Not sure we want to keep this; it's a relic of deciding to put the images in one place,
	then putting them in a bulk upload area, then putting them in MediaWiki...
      HISTORY:
	2019-09-11 This replaces the fm_folder table
	2019-10-06 Modified function name and parameters -
	  now returns the whole array instead of expecting a repository ID.
    */
    public function GetWebPaths_forImages() : array {
	return array(
	  1 => '/ufiles/cat/titles/',	// original image server
	  2 => '/wiki/wikiup/',		// vbzwiki images
	  3 => '/ufiles/cat/bulk/'	// bulk upload area
	);
    }
    protected function GetWebPath_forAppBase() {
	return '/';
    }
    
    // WEB PATHS: FIGURING
    
    // ACTION: Returns the current path relative to the app base URL
    public function GetWebPath_Current_AppRelative() {
	return fcURL::PathRelativeTo($this->GetWebPath_forAppBase());
    }
    protected function GetWebPath_forPaymentIcons() {
	return $this->GetWebPath_forIcons().'cards/';
    }
    public function GetWebSpec_forVisaCard() {
	return $this->GetWebPath_forPaymentIcons().'logo_ccVisa.gif';
    }
    public function GetWebSpec_forMasterCard() {
	return $this->GetWebPath_forPaymentIcons().'logo_ccMC.gif';
    }
    public function GetWebSpec_forAmexCard() {
	return $this->GetWebPath_forPaymentIcons().'logo_ccAmex.gif';
    }
    public function GetWebSpec_forDiscoverCard() {
	return $this->GetWebPath_forPaymentIcons().'logo_ccDiscover.gif';
    }
}
