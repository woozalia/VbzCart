<?php
/*
  PURPOSE: VbzCart page-type dispatcher
    Defines URL bases for each section, mostly.
    Normally this doesn't need per-site configuring.
  INPUT: $kfpAcctRoot should be set to the root for the domain account,
    typically the folder above "public_html". This is done most easily
    by index.php.
  HISTORY:
    2019-08-15 moved most of this from index.php
    2019-08-16 created vcGlobalsFinal (replaces vcGlobalsApp in main.php)
*/

//$kfpAcctRoot = dirname(__FILE__,2);
//$kfpSiteFiles = dirname(__FILE__,3);
$kfpSiteFiles = $kfpAcctRoot.'/site';
$kfpConfig = $kfpSiteFiles.'/config';
$kfpConfigFerreteria = $kfpConfig.'/ferreteria';
$kfpConfigVbzCart = $kfpConfig.'/vbzcart';

//require($kfpConfigFerreteria.'/const.php');	// Ferreteria configuration
require($kfpConfigVbzCart.'/local/creds.php');	// database credentials

use ferreteria\classloader\cLoader;

//fcCodeModule::SetDebugMode(TRUE);
require($kfpSiteFiles.'/ferreteria/start.php');		// Ferreteria startup
cLoader::MakeLibrary($kfpSiteFiles,'vbzcart.base','VbzCart/lib/@lib.php');	// /home/vbz-net/site/VbzCart/lib/@lib.php
//require($kfpSiteFiles.'/VbzCart/lib/@libs.php');	// register vbzcart's libraries

cLoader::LoadLibrary('vbzcart.base');
cLoader::LoadLibrary('ferreteria.login');
cLoader::LoadLibrary('ferreteria.forms');
cLoader::LoadLibrary('ferreteria.node');

require($kfpConfigVbzCart.'/local/globals.php');	// include global settings for this instance
$og = new vcGlobalsFinal();	// create the globls object

// get first segment of path -- that determines which type of page we're doing
$wpInfo = $og->GetWebPath_Current_AppRelative();
$arPath = fcURL::ParsePath($wpInfo);

if (count($arPath) == 0) {
    $sPageKey = '';
} else {
    $sPageKey = array_keys($arPath)[0];
}

if (count($arPath) == 0) {
    // 2019-06-15 not sure if this is actually necessary
    $sPageKey = '';
}
// TODO: really, vcApp should dispatch these so the constants can be configured in one place
//	currently duplicated in e.g. GetWebPath_forCartPage(), GetWebPath_forCatalogPages(), GetWebPath_forSearchPages()
switch ($sPageKey) {
  case '':
    $oApp = new vcAppShop_home();
    break;
  case 'admin':
    $oApp = new vcAppAdmin();
    break;
  case 'cart':
    $oApp = new vcAppShop_cart();
    break;
  case 'cat':
    $oApp = new vcAppShop_catalog();
    break;
  case 'checkout':
    $oApp = new vcAppShop_checkout();
    break;
  case 'search':
    $oApp = new vcAppShop_search();
    break;
  case 'static':
    $oApp = new vcAppStatic();
    break;
  case 'topic':
    $oApp = new vcAppShop_topic();
    break;
  default:
    echo 'Requested section "'.$sPageKey.'" is not recognized.';
    return;
}
$oApp->SetBasePath($sPageKey);
$oApp->Go();
