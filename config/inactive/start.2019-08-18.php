<?php
/*
  PURPOSE: VbzCart page-type dispatcher
    Defines URL bases for each section, mostly.
    Normally this doesn't need per-site configuring.
  INPUT: $kfpAcctRoot should be set to the root for the domain account,
    typically the folder above "public_html". This is done most easily
    by index.php.
  HISTORY:
    2019-08-15 moved most of this from index.php
    2019-08-16 created vcGlobalsFinal (replaces vcGlobalsApp in main.php)
*/

//$kfpAcctRoot = dirname(__FILE__,2);
//$kfpSiteFiles = dirname(__FILE__,3);
$kfpSiteFiles = $kfpAcctRoot.'/site';
$kfpConfig = $kfpSiteFiles.'/config';
$kfpConfigFerreteria = $kfpConfig.'/ferreteria';
$kfpConfigVbzCart = $kfpConfig.'/vbzcart';

//require($kfpConfigFerreteria.'/const.php');	// Ferreteria configuration
require($kfpConfigVbzCart.'/local/creds.php');	// database credentials

//fcCodeModule::SetDebugMode(TRUE);
require($kfpSiteFiles.'/ferreteria/start.php');		// Ferreteria startup
require($kfpSiteFiles.'/VbzCart/lib/@libs.php');	// register vbzcart's libraries

fcCodeLibrary::Load_byName('ferreteria.login');
fcCodeLibrary::Load_byName('ferreteria.forms.2');
fcCodeLibrary::Load_byName('ferreteria.node');

class vcGlobalsFinal extends vcGlobals {

    // TEXT

    public function GetText_SiteName() {
	return 'vbz.net';
    }
    public function GetText_SiteName_short() {
	return 'VBZ';
    }
    public function GetAppKeyString() {	// TODO: rename to GetText_AppKey(), ocelot
	return 'vbzcart';
    }
    public function GetText_EmailDomain() {
	return 'vbz.net';
    }
    
    // FILE PATHS
    
    protected function GetFilePath_forSiteFolders() {
	global $kfpSiteFiles;
	return $kfpSiteFiles;
    }
     
    // WEB PATHS
    
    protected function GetWebPath_forAppBase() {
	return '/';
    }
    
    // WEB PATHS: FIGURING
    
    // ACTION: Returns the current path relative to the app base URL
    public function GetWebPath_Current_AppRelative() {
	return fcURL::PathRelativeTo($this->GetWebPath_forAppBase());
    }
    protected function GetWebPath_forPaymentIcons() {
	return $this->GetWebPath_forIcons().'cards/';
    }
    public function GetWebSpec_forVisaCard() {
	return $this->GetWebPath_forPaymentIcons().'logo_ccVisa.gif';
    }
    public function GetWebSpec_forMasterCard() {
	return $this->GetWebPath_forPaymentIcons().'logo_ccMC.gif';
    }
    public function GetWebSpec_forAmexCard() {
	return $this->GetWebPath_forPaymentIcons().'logo_ccAmex.gif';
    }
    public function GetWebSpec_forDiscoverCard() {
	return $this->GetWebPath_forPaymentIcons().'logo_ccDiscover.gif';
    }
}
$og = new vcGlobalsFinal();	// create the globls object

// get first segment of path -- that determines which type of page we're doing
$wpInfo = $og->GetWebPath_Current_AppRelative();
$arPath = fcURL::ParsePath($wpInfo);

if (count($arPath) == 0) {
    $sPageKey = '';
} else {
    $sPageKey = array_keys($arPath)[0];
}

if (count($arPath) == 0) {
    // 2019-06-15 not sure if this is actually necessary
    $sPageKey = '';
}

switch ($sPageKey) {
  case '':
    $oApp = new vcAppShop_home();
    break;
  case 'admin':
    $oApp = new vcAppAdmin();
    break;
  case 'cart':
    $oApp = new vcAppShop_cart();
    break;
  case 'cat':
    $oApp = new vcAppShop_catalog();
    break;
  case 'checkout':
    $oApp = new vcAppShop_checkout();
    break;
  case 'search':
    $oApp = new vcAppShop_search();
    break;
  case 'static':
    $oApp = new vcAppStatic();
    break;
  case 'topic':
    $oApp = new vcAppShop_topic();
    break;
  default:
    echo 'Requested section "'.$sPageKey.'" is not recognized.';
    return;
}
$oApp->SetBasePath($sPageKey);
$oApp->Go();
