<?php namespace Woozalia\VbzCart;
/*
 * PURPOSE: very minimally defines where to find things needed for startup
 * TODO: 2023-10-17 work out how to make this configurable without messing up the repo
 * HISTORY:
 *  2023-10-16 started
 *  2023-10-17 changed to just a variable
 */

$fpFerreteria = dirname(__DIR__,2).'/ferreteria';

