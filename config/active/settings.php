<?php namespace Woozalia\VbzCart\Config;

use Woozalia\Ferret\Config\caSettings;
use Woozalia\Ferret\Diag\Wrap\CodeViewer\Internal\cViewer;
use Woozalia\Ferret\Config\Defs\cMySQL;

/**::::
  PURPOSE: Constants for permissions
    These need to match whatever is defined in the KS_TABLE_USER_PERMISSION table.
  HISTORY:
    2014-01-05 created
    2015-04-18 moved from dropins/users/const.php to data/site-admin.php
      This needs to be more elegant somehow.
    2023-10-17 rewriting to use Settings class
*/
class cSettings extends caSettings {

    // ++ PATHS: FILE ++ //

    // path to the VbzCart repository
    public function FP_Repo()       : string { return dirname(__DIR__,2); }
    public function FP_Public()     : string { return $this->FP_Repo().'/public'; }
    public function FP_Static()     : string { return $this->FP_Public().'/static'; }
    public function FP_Array_CodeFiles() : array {
        return [
          'git'       => '/var/www/site/git',
          'mw:1.39.3' => '/var/www/site/mw/mediawiki-1.39.3',
          'mw:sp:ci'  => '/var/www/site/mw/ext/ClassInspector'
          ];

    }

    // ++ PATHS: WEB ++ //

    public function WP_AppBase()    : string { return '/vbz'; }
    public function WP_CodeViewer() : string { return '/phpview'; } // 2023-10-17 not yet implemented
    public function WP_Wiki()       : string { return 'https://wiki.vbz.net'; }
    public function WP_Images()     : array {
        return [
          1 => '/static/img/cat/titles/', // original image server
          2 => '/wiki/wikiup/',           // vbzwiki images
          3 => '/static/img/cat/bulk/',   // bulk upload area
          ];
    }

    // -- PATHS -- //
    // ++ CLASSES ++ //

    protected function CodeViewerClass()  : string { return cViewer::class; }
    #protected function DefClasses() : array { return [cMySQL::class]; }

    // -- CLASSES -- //
    // ++ TEXT ++ //

    public function ShortName() : string { return 'vbz'; }

    // -- TEXT -- //
    // ++ ADMIN ++ //

    /**----
      RETURNS: ID of user who should have root permissions regardless of other permissions setting
      USAGE: normally, set to 1 when giving yourself admin powers at setup
    */
    public function UserID_forRoot() { return 1;	} // first user created is currently $deity

    // -- ADMIN -- //
}

/* 2023-10-17 TODO: redefine these as cSettings methods as needed.
define('ID_USER_ROOT',1);	// user with this ID has all permissions - uncomment when giving yourself admin powers at setup
define('ID_GROUP_USERS',2);	// group to which all users automatically belong
define('KB_USE_ANONYMOUS_ROOT',FALSE);	// set to TRUE if nonexistent permissions are preventing login
define('KS_PERM_FE_ROOT','*');	// "must be root" - not counted as a "needed" permission
define('KS_NEW_REC','new');	// pathArgs string to use for new entries

//define('KWP_REL_LOGOUT','?exit');	// this needs to be defined in site.php so checkout can use it
*/
