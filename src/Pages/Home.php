<?php namespace Woozalia\VbzCart\Pages;

use Woozalia\Ferret\Config\csGlobals;
use Woozalia\Ferret\IO\Request\cPiece;
use Woozalia\Ferret\String\csStringFx;
use Woozalia\VbzCart\Data\Requests\Title\cQuantities as cTitleQuantities;
use Woozalia\VbzCart\Data\Requests\Title\cStatistics as cTitleStatistics;
use Woozalia\VbzCart\Pages\Title\cSummary as cTitleSummary;
use Woozalia\VbzCart\Responders\caShop as caShopResponder;

/**::::
 * PURPOSE: root Kiosk - handles first segment of URI and displays home page if no request
 * HISTORY:
 *  2023-10-19 started
 */
class cHome extends caShop {

    // CALLBACK; CEMENT Web\caPage
    protected function DoContent() {
        $urlHomeAsPfx = $this->AppURLPrefix();
        $sItems = $this->RenderRandomItems();
        $sOut = <<<__END__
<table><td style="background-color: rgba(0,0,64,0.8);">
$sItems
</td></tr></table>
__END__;

        csGlobals::Screen()->Write("$sOut<a class=content href='$urlHomeAsPfx/diag'>diagnostic page</a>");
    }
    protected function RenderRandomItems() : string {
        #$tq = $this->StockItemQuery();
        $db = csGlobals::Database();

        $oDataReq = new cTitleQuantities;

        #$t = $this->StockTitleQuery();
        $oRes = $db->HandleRequest($oDataReq);
        #$rs = $t->SelectRecords_forTitleStockStatus();
        $rs = $oRes->Deck()->GetIt();
        $q = $rs->RowCount();

        $nToGet = 10;	// change as needed

        $sPlur = csStringFx::Pluralize($q);
        $out = "<div class=content>There are currently <b>$q</b> different title$sPlur in stock.<br>Here are as many as $nToGet of them. (Reload the page for more.)</div><br>";

    //	$out .= "<b>SQL AVAIL</b>: ".$rs->sql.'<br>';

        // copy data to array
        while (($osCard = $rs->NextRow())->HasIt()) {
            $arTitles[] = $osCard->GetIt();
        }

        $arRand = array_rand($arTitles,$nToGet);
        shuffle($arRand);	// array_rand returns chosen items in order, so randomize

        // load up information about each of the titles

        $sqlIDs = NULL;
        // build a list of the IDs
        foreach($arRand as $idx => $idxRand) {
            $ocTitle = $arTitles[$idxRand];
            $arTitle = $ocTitle->GetAll();
            $idTitle = $arTitle['ID_Title'];
            $sqlIDs .= is_null($sqlIDs)?($idTitle):(','.$idTitle);
        }
        // fetch extended status information for all titles listed
        $sqlFilt = "ID_Title IN ($sqlIDs)";
        #$tTiInfo = $this->TitleInfoQuery();	// (vcqtTitlesInfo)
        $tTiInfo = new cTitleStatistics($sqlFilt);
        #$sql = $tTiInfo->SQL_forStockStatus_byTitle($sqlFilt);
        #$rs = $tTiInfo->FetchRecords($sql);	// (vcqrTitleInfo)

        $oRes = $db->HandleRequest($tTiInfo);
        $rs = $oRes->Deck()->GetIt();

        //$out .= "<b>SQL FINAL</b>: $sql<br>";

        // display the results
        $oTitlePage = new cTitleSummary;
        $oTitleRes = caShopResponder::ResourceFor('title');  // 2023-10-29 TODO: There should be a less arbitrary way of referencing this.
        $oTitlePage->Resource()->SetIt($oTitleRes); // ...and, really, why doesn't the Page know how to find its own Resource?
        while (($osCard = $rs->NextRow())->HasIt()) {
            $oCard = $osCard->GetIt();
            $oTitlePage->Card()->SetIt($oCard);
            $out .= $oTitlePage->RenderImages_withLink_andSummary();
        }

        return $out;
    }
    /*
    protected function StockItemQuery() {
        return $this->GetDatabase()->MakeTableWrapper('vcqtStockItemsInfo');
    }
    protected function StockTitleQuery() {
        return $this->GetDatabase()->MakeTableWrapper('vcqtStockTitlesInfo');
    }
    */
}
