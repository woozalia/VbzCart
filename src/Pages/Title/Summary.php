<?php namespace Woozalia\VbzCart\Pages\Title;

use Woozalia\Ferret\Data\Mem\Val\csMoney;
use Woozalia\VbzCart\Pages\cTitle;
use Woozalia\VbzCart\Data\Static\Image\eSizes;

/**::::
 * PURPOSE: display class for Title Summary data
 * HISTORY:
 *  2023-10-29 created from code currently in Title Summary Card class
 */
class cSummary extends cTitle {

    // ++ OUTPUT ++ //

    /**----
      HISTORY:
        2018-02-16 created for home page random-titles display
    */
    public function RenderImages_withLink_andSummary() : string {
        $oCard = $this->Card()->GetIt();
        $htTitle = $oCard->NameString();
        $htStatus = $this->RenderCatalogSummary_HTML();
        $qStock = $oCard->QtyInStock();
        $htStock = "$qStock in stock";
        $htPopup = "&ldquo;$htTitle&rdquo;%attr%\n$htStatus\n$htStock";
        $htImgs = $this->RenderImages_forRow($htPopup,eSizes::Thumb);
        $htTitle = $this->ShopLink($htImgs);
        return $htTitle;
    }
    protected function RenderCatalogSummary_HTML() : string {
        $oCard = $this->Card()->GetIt();
        $out =
          $oCard->CatalogTypesList()
          .' - '
          .$oCard->CatalogOptionsList()
          .' ('
          .$this->RenderPriceRange()
          .')'
          ;
        return $out;
    }
    protected function RenderPriceRange() : string {
        $oCard = $this->Card()->GetIt();
        $nPrcLo = $oCard->PriceMinimum();
        $nPrcHi = $oCard->PriceMaximum();
        if ($nPrcLo == $nPrcHi) {
          $sPrice = csMoney::Format_withSymbol($nPrcLo);
        } else {
          $ftLo = csMoney::Format_withSymbol($nPrcLo);
          $ftHi = csMoney::Format_withSymbol($nPrcHi);
          $sPrice = "$ftLo - $ftHi";
        }
        return $sPrice;
    }

    // -- OUTPUT -- //

}
