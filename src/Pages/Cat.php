<?php namespace Woozalia\VbzCart\Pages;

use Woozalia\Ferret\Data\Mem\Result\cObj;
use Woozalia\Ferret\Web\Page\iCarded;
use Woozalia\Ferret\Web\Page\tCarded;

/**::::
 * PURPOSE: legacy-style catalog number lookup
 * HISTORY:
 *  2023-10-19 rebuilding from scratch with pieces of the original code
 */
class cCat extends caShop implements iCarded {
    use tCarded;
    // CALLBACK; CEMENT Web\caPage
    protected function DoContent() {} // not sure what this did; check archive.org

}
