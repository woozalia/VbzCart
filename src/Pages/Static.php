<?php namespace Woozalia\VbzCart\Pages;

use Woozalia\VbzCart\Config\cSettings;
use Woozalia\Ferret\Kiosk\caResponder;

/**::::
 * PURPOSE: This is a bit of a kluge to avoid having to do lots of specific .htaccess config stuff
 *  It's basically a proxy for indexed folders in the /static/ folder.
 * HISTORY:
 *  2023-10-22 created
 */
class cStatic extends caResponder {
    protected function DoResponses() {
        #$oReqSeq = $this->Request()->Seq();
        $uriFile = $this->Request()->EncodeToString($this->Request());
        $fsFile = cSettings::Me()->FP_Static().$uriFile;
        #echo "URI: [$uriFile] FS: [$fsFile]<br>";
        // for now, let's assume that any "missing" image is a folder with an index.php:
        include $fsFile.'/index.php';
    }
}
