<?php namespace Woozalia\VbzCart\Pages;

use Woozalia\Ferret\Config\csGlobals;
use Woozalia\Ferret\Web\Page\caStandard;
use Woozalia\VbzCart\Config\cSettings;

/**::::
 * PURPOSE: Page layout for shopping (catalog browsing and cart) pages ...at least, that's the tentative plan.
 * HISTORY:
 *  2023-10-19 rebuilding from scratch with pieces of the original code
 */
abstract class caShop extends caStandard {  // 2023-11-01 Might have to break Carded off into a trait/interface.

    protected function DoHeader() {
        $sOut =
          $this->SiteTopNotice()
          .$this->MainHeader()
          ;
        csGlobals::Screen()->Write($sOut);
    }
    protected function DoSidebar() {
    }
    #protected function DoContent();
    protected function DoFooter() {
        // As of the last live version, there doesn't seem to have been a footer. Maybe we'll do one later.
    }

    // ++ HEADER BITS ++ //

    protected function SiteTopNotice() : string {
        return <<<__END__
<table width="100%" id="site-message" style="background: #660000; border: solid red 2px;">
  <tr>
    <td align="center">
      <span style="color: white; font-weight: bold; font-family: sans-serif;">VBZ.NET IS CURRENTLY DOWN - I am <a style="color: yellow;" href="https://toot.cat/tags/vbzcart">working</a> to fix it. Really.</span>
      </span>
    </td>
  </tr>
</table>
__END__;

        // for when the catalog pages are working again
        /*
        return <<<__END__
<table width="100%" id="site-message" style="background: #660000; border: solid red 2px;">
  <tr>
    <td align="center">
      <span style="color: white; font-weight: bold; font-family: sans-serif;">VBZ.NET IS CURRENTLY DOWN - most of the catalog works,
        but the checkout process is <a style="color: yellow;" href="/wiki/User:Woozle/blog/2013/10/06/1339/Updates_and_such">being rebuilt</a>.
        <br>I apologize for the nuisance, and am <a style="color: yellow;" href="https://toot.cat/tags/vbzcart">working</a> to fix it.
      </span>
    </td>
  </tr>
</table>
__END__;
*/
    }
    protected function MainHeader() : string {
        $urlHomeAsPfx = $this->AppURLPrefix();
        $urlHomeSolo = $this->AppHomeURL();
        $urlWiki = $this->WikiURL();
        return <<<__END__
<table width="100%" id="cont-header-outer" class="border" cellpadding="5"><tr><td>
<table width="100%" id="cont-header-inner" class="hdr" cellpadding="2"><tr>
<td>
<a href="$urlHomeSolo"><img align="left" border="0" src="$urlHomeAsPfx/static/img/logos/v/" title="vbz.net home" alt="V"></a>

<span class="pretitle"><b><a href="$urlHomeSolo">vbz.net</a></b>: hello and welcome to....</span><br>
<span class="page-title">The Temporary Test Page</span>
</td>

<td align="right">
<a href="$urlHomeSolo"><img border="0" src="$urlHomeAsPfx/static/img/icons/home.050pxh.png" title="vbz.net home page" alt="home page"></a><a href="$urlHomeAsPfx/search/"><img border="0" src="$urlHomeAsPfx/static/img/icons/search.050pxh.png" title="search page" alt="search page"></a><a href="$urlHomeAsPfx/cart/"><img border="0" src="$urlHomeAsPfx/static/img/icons/cart.050pxh.png" title="shopping cart" alt="shopping cart"></a><a href="$urlWiki/help"><img border="0" src="$urlHomeAsPfx/static/img/icons//help.050pxh.png" title="help!" alt="help"></a>
</td>
<!-- END RIGHT HEADER -->
</tr></table>
</td></tr></table>
__END__;
    }
    protected function NavigationBar() : string {
        $urlHomeAsPfx = $this->AppURLPrefix();
        $urlHomeSolo = $this->AppHomeURL();
        $urlWiki = $this->WikiURL();
        return <<<__END__
<span class="menu-text">
  <table class="border" align="left" cellpadding="3" bgcolor="#000000">
    <tr><td>
      <table class="sidebar" bgcolor="#ffffff" cellpadding="5">
        <tr><td>
          <table border="0" class="menu-title" width="100%">
            <tr><td class="menu-title"><a href="$urlHomeSolo">Home</a></td></tr>
          </table>
          <form action="$urlHomeAsPfx/search/">Search all items:<br>
            <input size="10" name="search">
            <table width="100%"><tr>
              <td><small><a href="$urlHomeAsPfx/search/">advanced</a></small></td>
              <td align="right"><input type="submit" value="Go"></td>
            </tr></table>
          </form>
          <hr>
          <big>Catalog:</big>
          <span class="menu-text">
            <li><a href="$urlHomeAsPfx/cat/" title="our suppliers, and what we carry from each one" class="menu-link">Suppliers</a></li>
            <li><a href="$urlHomeAsPfx/topic/" title="topic master index (topics are like category tags)" class="menu-link">Topics</a></li>
          </span>
          <hr>
          <big>Wiki pages:</big>
          <span class="menu-text">
            <li>[[ <a href="$urlWiki/help" title="help main index" class="menu-link">Help</a> ]]</li>
            <li>[[ <a href="$urlWiki/contact" title="contact vbz.net" class="menu-link">Contact</a> ]]</li>
          </span>
        </td></tr>
      </table>
    </td></tr>
  </table>
</span>
__END__;
    }

    // -- HEADER BITS -- //
    // ++ INTERNAL SERVICES ++ //

    protected function WikiURL() : string { return cSettings::Me()->WP_Wiki(); }

    // -- INTERNAL SERVICES -- //
}
