<?php namespace Woozalia\VbzCart\Pages;

use Woozalia\Ferret\Config\csGlobals;

/**::::
 * PURPOSE: page with diagnostic info -- partly just for demonstration
 */
class cDiag extends caShop {
    // CALLBACK; CEMENT Wed\caPage
    protected function DoContent() {
        $sOut = "<table class=content>\n";
        foreach ($_SERVER as $sKey => $sVal) {
            $sOut .= "<tr><td align=right>$sKey</td><td>$sVal</td>\n";
        }
        $sOut .= "</table>\n";
        csGlobals::Screen()->Write("Server data: $sOut");
    }
}
