<?php namespace Woozalia\VbzCart\Pages;

use Woozalia\Ferret\IO\Screen\HTML\csElem;
use Woozalia\Ferret\Web\Page\iCarded;
use Woozalia\Ferret\Web\Page\tCarded;

/**::::
 * PURPOSE: for rendering catalog images -- mainly components, not whole pages
 * HISTORY:
 *  2023-11-01 rebuilding
 */
class cImage implements iCarded {
    use tCarded;

    /**----
      QUESTION: Does anyone actually pass anything in $arAttr? (TODO)
      INPUT: $sTitle - Title description (append view string to this)
      HISTORY:
        2018-02-17 adding in template option %attr% for more flexible format
    */
    public function RenderInline_row(string $sTitle, eSizes $eSize, array $arAttr=NULL) : string {

        $oCard = $this->Card()->GetIt();

        // ++RenderSingle()

        $arAtIm['class'] = 'image-title-'.$eSize->value;
        $sView = $oCard->GetIt('AttrDispl');

        $ksAttrKey = '%attr%';

        // if attrib key not found, put it at the end
        if (strpos($sTitle,$ksAttrKey) === FALSE) {
            $sTitle .= $ksAttrKey;
        }

        // format the attrib replacement string
        if (empty($sView)) {
            $sAttr = '';
        } else {
            $sAttr = " ($sView)";
        }

        // replace the attrib key
        $sTitle = str_replace($ksAttrKey,$sAttr,$sTitle);

        $arAtIm['title'] = $sTitle;
        $arAtIm['src'] = $oCard->WebSpec();
        $htAtIm = csElem::ArrayToAttrs($arAtIm);

        $htImg = "\n<img$htAtIm />";

        return $htImg;
    }
}
