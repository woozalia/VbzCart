<?php namespace Woozalia\VbzCart\Pages;

use Woozalia\Ferret\Config\csGlobals;
use Woozalia\Ferret\IO\Request\cData;
use Woozalia\Ferret\IO\Screen\HTML\csElem;
use Woozalia\VbzCart\Config\cSettings;
use Woozalia\VbzCart\Data\Static\Image\eSizes;
use Woozalia\VbzCart\Responders\cTitle as cTitleResponder;

/**::::
 * PURPOSE: catalog Title detail page
 * HISTORY:
 *  2023-10-21 rebuilding
 */
class cTitle extends cCat {
    // ++ OUTPUT ++ //

    // PUBLIC so Image records can access it
    public function ShopURL() {
        $wpApp = cSettings::Me()->WP_AppBase();
        #$wpInfo = $this->ShopURL_part();
        #$oTitleRoute = cTitleResponder::Route()->GetIt();
        #$oRes = $this->Resource();
        #$oRts = $oRes->Routes();  // keyed array
        $oData = new cData;
        $oData->AddPair('title',$this->Card()->GetIt()->TitleID()); // untested // 2023-11-01 Surely there's an object which knows we use 'title'...
        $uriPage = csGlobals::WebURICodec()->EncodeToString($oData);  // untested
        return $wpApp.$uriPage;
    }
    public function ShopLink($sShow,array $arAttr=NULL) {
        $url = $this->ShopURL();
        $htAttr = csElem::ArrayToAttrs($arAttr);
        $htID = $this->RenderIDAttr();
        return '<a'.$htAttr.' href="'.$url.'" id='.$htID.'>'.$sShow.'</a>';
    }
    /**
     * THINKING: This helps to make text and image links highlight in sync. I think.
     * HISTORY:
     *  2018-02-10 moved from vcrShopTitle to vtrTitle_shop and made it PROTECTED because nothing else is using it anymore
     *  2023-11-01 updating class for Ferreteria v0.6
     */
    protected function RenderIDAttr() {
        $id = $this->Card()->GetIt()->TitleID();
        return "title-$id";
    }

    /**----
      HISTORY:
        2017-03-17 written (currently used only by admin fx(), but why isn't it used for the shopping page?)
        2018-02-15 made public for home page to use
      PUBLIC so home page can use it to display stuff
    */
    public function RenderImages_forRow(string $sPopup,eSizes $eSize=eSizes::Small) : string {
        $oCard = $this->Card()->GetIt();
        $rsIm = $oCard->ImageRecords_forRow($eSize);
        return $rsIm->RenderInline_rows($sPopup,$eSize);
    }

    // -- OUTPUT -- //
}
