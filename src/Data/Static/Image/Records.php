<?php namespace Woozalia\VbzCart\Data\Static\Image;

use Woozalia\Ferret\Config\csGlobals;
use Woozalia\VbzCart\Data\Requests\cImage;

/**::::
 * PURPOSE: static class which returns Records (Responses) for Image-related stuff
 * HISTORY:
 *  2023-10-26 building from bits of legacy VbzCart code
 */
class csRecords {
    /**----
      RETURNS: recordset of images for the given title in the given size
      HISTORY:
        2013-11-17 split off from Thumbnails(), which kind of shouldn't exist
        2016-02-18 renamed from Records_forTitle() to ActiveRecords_forTitle()
      INPUT:
        idTitle = ID of title to look up
        eSize = size of image wanted
    */
    static public function ActiveRecords_forTitle(int $idTitle,eSizes $eSize) {
        $sSize = $eSize->value;
        $sqlFilt = "(ID_Title=$idTitle) AND (Ab_Size='$sSize') AND isActive";
        $tImage = new cImage(" WHERE $sqlFilt ORDER BY AttrSort");
        $db = csGlobals::Database();
        $rs = $db->HandleRequest($tImage);
        return $rs;
    }
}
