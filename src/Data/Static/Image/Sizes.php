<?php namespace Woozalia\VbzCart\Data\Static\Image;

/**::::
 * USAGE: The string-values are used in the image table as well as in filenames.
 * HISTORY:
 *  2023-10-26 written, based on constants in previous version
 */
enum eSizes : string {
    case Thumb = 'th';
    case Small = 'sm';
    case Large = 'big';
    case Zoom  = 'zoom';
}
