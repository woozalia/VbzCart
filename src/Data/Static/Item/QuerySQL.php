<?php namespace Woozalia\VbzCart\Data\Static\Item;
/**::::
 * PURPOSE: static class which calculates SQL for complex queries involving Items and Stock
 * HISTORY:
 *  2023-10-26 building from bits of legacy VbzCart code
 */
class csQuerySQL {
    // TESTED 2018-02-16
    static protected function SQL_forStockStatus_byLine() : string {
        $fs = __DIR__.'/qryStockStatus_byLine.sql';
        $sql = file_get_contents($fs);
        return $sql;
    }
    // TESTED 2018-02-16
    static public function SQL_forStockStatus_byItem() : string {
        $sqlCore = self::SQL_forStockStatus_byLine();

        $sql = <<<__END__
SELECT
  ID_Item,
  QtyInStock,
  i.PriceSell,
  i.CatSfx,
  i.ID_Title,
  i.ID_ItTyp,
  i.ID_ItOpt
FROM
  (SELECT
    ID_Item,
    SUM(QtyForSale) AS QtyInStock
  FROM ($sqlCore) AS sl
  GROUP BY ID_Item) AS si
LEFT JOIN cat_items AS i ON i.ID=si.ID_Item
__END__;
        return $sql;
    }

    // -- SQL -- //

}
