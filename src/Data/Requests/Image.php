<?php namespace Woozalia\VbzCart\Data\Requests;

use Woozalia\Ferret\Data\Base\Request\caSelect;
use Woozalia\VbzCart\Data\Cards\cImage as cImageCard;
use Woozalia\VbzCart\Data\Results\cImages as cImageResult;

class cImage extends caSelect {
    // OVERRIDE
    public function ResultClass()   : string { return cImageResult::class; }
    public function CardClass()     : string { return cImageCard::class; }
    protected function SourceSQL()  : string { return 'cat_images'; }
}
