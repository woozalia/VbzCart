<?php namespace Woozalia\VbzCart\Data\Requests\Title;

use Woozalia\Ferret\Data\Base\Request\caRead;
use Woozalia\Ferret\Data\Mem\Result\ifVar;
use Woozalia\Ferret\Data\Mem\Result\cWriteableString;
use Woozalia\VbzCart\Data\Cards\Title\cSummary as cSummaryCard;
use Woozalia\VbzCart\Data\Static\Item\csQuerySQL as csItemQuerySQL;

class cStatistics extends caRead {

    // ++ CONFIG ++ //

    // OVERRIDE
    public function CardClass() : string { return cSummaryCard::class; }   // single-row class

    // -- CONFIG -- //
    // ++ SETUP ++ //

    public function __construct(string $sqlWhere) {
        $this->osFilt = new cWriteableString($sqlWhere);
    }

    private $osFilt;  public function Where()       : ifVar   { return $this->osFilt; }

    // -- SETUP -- //
    // ++ OUTPUT ++ //

    /**
     * HISTORY:
     *  2023-10-25 adapted from title.info.php: SQL_forStockStatus_byTitle()
     */
    public function SQL() : string {
        $osFilt = $this->Where();
        $sqlWhere = $osFilt->HasIt() ? ('WHERE '.$osFilt->GetIt()) : '';
        $sqlCore = csItemQuerySQL::SQL_forStockStatus_byItem();
        $sql = <<<__END__
SELECT
  t.ID,
  t.ID_Dept,
  t.ID_Supp,
  QtyInStock,
  PriceMin,
  PriceMax,
  t.Name,
  t.CatKey,
  ItTypes,
  ItOptions
FROM
  (SELECT
    ID_Title,
    SUM(QtyInStock) AS QtyInStock,
    MIN(PriceSell) AS PriceMin,
    MAX(PriceSell) AS PriceMax,
    GROUP_CONCAT(DISTINCT itt.NameSng ORDER BY itt.Sort SEPARATOR ', ') AS ItTypes,
    GROUP_CONCAT(DISTINCT iop.CatKey ORDER BY iop.Sort SEPARATOR ',') AS ItOptions
  FROM ($sqlCore) AS si
  JOIN cat_ittyps AS itt ON si.ID_ItTyp=itt.ID
  JOIN cat_ioptns AS iop ON si.ID_ItOpt=iop.ID
  GROUP BY ID_Title) AS st
LEFT JOIN cat_titles AS t ON t.ID=st.ID_Title
$sqlWhere
__END__;
        return $sql;
    }

    // -- OUTPUT -- //
}
