<?php namespace Woozalia\VbzCart\Data\Stock;

use Woozalia\Ferret\Data\Base\Request\caRead;

/**::::
  PURPOSE: queries that return useful information about stock by catalog item
  HISTORY:
    2017-03-23 SQL_forItemStatus(): (re)written; will probably need more output fields. Not tested.
    2017-04-10 SQL_forItemStatus(): Testing now. Needed a filter argument; for now, I'm making this required --
      but if there's good reason, it could be optional.
    2017-05-07 splitting stock.info.php into stock-item.info.php and stock-line.info.php
    2023-10-24 class rewrite for Ferreteria v0.6
    2023-10-26 Actually, this is not (yet?) needed...
*/
class cItemStatistics_NOT_USED extends caRead {

    // ++ SQL ++ //

    // OVERRIDE
    protected function FieldsSQL() : string {
        return <<<__END__

  ID_Item,
  SUM(sl.QtyForSale) AS QtyForSale,
  SUM(sl.QtyForShip) AS QtyForShip,
  SUM(sl.QtyExisting) AS QtyExisting
__END__;
    }
    // CEMENT
    protected function SourceSQL() : string { return vcqtStockLinesInfo::SQL_forLineStatus(); }
    // DEFAULT
    protected function EndingSQL() : string {
        return <<<__END__
 AS sl
WHERE $sqlFilt
GROUP BY sl.ID_Item
__END__;

    }

    // ++ SQL: subqueries ++ //

}
