<?php namespace Woozalia\VbzCart\Data\Results;

use Woozalia\Ferret\Data\Base\Result\cSelect;
use Woozalia\VbzCart\Data\Static\Image\eSizes;

class cImages extends cSelect {
    /*-----
      ACTION: Generate the HTML code to display an image for the current recordset
      INPUT: $sTitle - Title description
      HISTORY:
        2013-11-17
          * Renamed from Image_HTML() to RenderInline()
          * Moved from vbz-cat-image.php to vbz-cat-image-ui.php
        2023-10-26 adapting/updating for Ferreteria v0.6
    */
    public function RenderInline_rows(string $sTitle,eSizes $eSize,array $arAttr=NULL) : string {
        $out = '';
        $oDeck = $this->Deck()->GetIt();
        $oDeck->RewindIt();
        while (($osCard = $oDeck->NextRow())->HasIt()) {
            $oCard = $osCard->GetIt();
            $out .= "\n".$oCard->RenderInline_row($sTitle,$eSize,$arAttr);
        }
        return $out;
    }

}
