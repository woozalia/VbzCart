<?php namespace Woozalia\VbzCart\Data\Cards;

use Woozalia\Ferret\Data\Base\cCard;
use Woozalia\Ferret\IO\Screen\HTML\csElem;
use Woozalia\VbzCart\Config\cSettings;
use Woozalia\VbzCart\Data\Static\Image\eSizes;

class cImage extends cCard {

    // ++ FIELDS ++ //

    protected function WebSpec() : string {
        $idFolder = $this->GetIt('ID_Folder');
        $wsFile = $this->GetIt('Spec');
        $wpFolder = cSettings::Me()->WP_Images()[$idFolder];
        return $wpFolder;
    }

    // -- FIELDS -- //
    // ++ OUTPUT ++ //


    // -- OUTPUT -- //

}
