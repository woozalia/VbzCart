<?php namespace Woozalia\VbzCart\Data\Cards\Title;

use Woozalia\Ferret\Data\Base\cCard;
use Woozalia\Ferret\Data\Mem\Val\csMoney;
use Woozalia\VbzCart\Data\Cards\cTitle;
use Woozalia\VbzCart\Data\Static\Image\eSizes;

/**::::
 * HISTORY:
 *  2023-10-26 rebuilding from title.info.php:vcqrTitleInfo
 */
class cSummary extends cTitle {

    // ++ FIELDS ++ //

    public function QtyInStock()         : int    { return $this->GetIt('QtyInStock'); }
    public function CatalogTypesList()   : string { return $this->GetIt('ItTypes'); }
    public function CatalogOptionsList() : string { return $this->GetIt('ItOptions'); }
    public function PriceMinimum()       : string { return $this->GetIt('PriceMin'); }
    public function PriceMaximum()       : string { return $this->GetIt('PriceMax'); }

    // -- FIELDS -- //
}
/* 2023-10-26 original code
class vcqrTitleInfo extends vcBasicRecordset {
    use vtrTitle;
    use vtrTitle_shop;
    use vctrTitleInfo;
    use vtTableAccess_ImagesInfo;

    // ++ FIELD VALUES ++ //

    public function CatNum() {
	return $this->GetFieldValue('CatNum');
    }
    protected function QtyInStock() {
	return $this->GetFieldValue('QtyInStock');
    }

    // 2018-04-16 Apparently some queries generate one and some generate the other. TODO: FIX THIS

    protected function QtyAvailable() {
	//throw new exception('Call CountForSale() instead of QtyAvailable().');	// 2018-02-17
	return $this->GetFieldValue('CountForSale');					// 2018-04-18 was QtyAvail
    }
    protected function CountForSale() {
	throw new exception('Call QtyAvailable() instead of CountForSale().');	// 2018-04-16
	return $this->GetFieldValue('CountForSale');
    }

    // --

    // NOTE: This is a calculated field.
    protected function CatalogOptionsList() {
	return $this->GetFieldValue('ItOptions');
    }

    // -- FIELD VALUES -- //
    // ++ FIELD CALCULATIONS ++ //

    protected function TitleQuoted() {
	return '&ldquo;'.$this->TitleString().'&rdquo;';
    }
    // 2018-02-17 This may be overcalculating: doesn't CountForSale() include QtyInStock()>0?
    public function IsForSale() {
	return ($this->QtyInStock() > 0) || ($this->QtyAvailable() > 0);
    }
    /*
      INPUT: recordset data from $this->Table()->ExhibitRecords()
      OUTPUT: item's availability status: options, prices, stock
    */ /* call RenderSummary_html() instead
    protected function StatusLine_HTML() {
	return 'StatusLine_HTML(): to be written';
    }
    protected function StatusLine_Text() {
	return 'StatusLine_Text(): to be written';
    } */
    /*----
      INPUT: recordset data from $this->Table()->ExhibitRecords()
      OUTPUT: summary line: title (quoted), availability status
    * /
    public function SummaryLine_HTML_line() {
	$sCatNum = $this->CatNum();
	$htLink = $this->ShopLink($sCatNum);
	$sName = $this->NameString();
	$sStats = $this->RenderStatus_HTML_line();
	return "\n$htLink &ldquo;$sName&rdquo; &ndash; $sStats<br>";
    }
    /*----
      OUTPUT: SummaryLine as table row (HTML)
      NOTE: NOT YET TESTED
    * /
    public function SummaryLine_HTML_row() {
	$sCatNum = $this->CatNum();
	$htLink = $this->ShopLink($sCatNum);
	$sName = $this->NameString();
	$sStats = $this->RenderStatus_HTML_cells();
	return "\n<tr><td>$htLink</td><td>&ldquo;$sName&rdquo;</td>$sStats</tr>";
    }
    public function SummaryLine_Text() {
	$sCatNum = $this->CatNum();
	$sName = $this->NameString();
	$sStats = $this->RenderStatus_text();
	return "$sCatNum &ldquo;$sName&rdquo; - $sStats<br>";
    }
    public function ImageTitle() {
	return $this->SummaryLine_Text();
    }
    protected function TitleHREF() {
	$sCatPath = $this->GetFieldValue('CatPath');
	//$sCatPath = $this->CatPage();
	$url = vcGlobals::Me()->GetWebPath_forCatalogPages().$sCatPath;
	$id = $this->GetKeyValue();
	$htHref = "<a href='$url'>";
	return $htHref;
    }
    protected function RenderCatalogSummary_HTML() {
	$out =
	  $this->CatalogTypesList()
	  .' - '
	  .$this->CatalogOptionsList()
	  .' ('
	  .$this->RenderPriceRange()
	  .')'
	  ;
	return $out;
    }
    protected function RenderStatus_HTML_line() {
	$qInStock = $this->QtyInStock();
	$out = $this->RenderCatalogSummary_HTML();
	if ($qInStock > 0) {
	    $out .= " &ndash; $qInStock in stock";
	}
	return $out;
    }
    /*----
      OUTPUT: RenderStatus_HTML as table cells
    * /
    protected function RenderStatus_HTML_cells() {
	$qInStock = $this->QtyInStock();
	$out = "<td>"
	  .$this->RenderPriceRange()
	  .'</td><td>'
	  .$this->CatalogOptionsList()
	  .'</td>'
	  ;
	if ($qInStock > 0) {
	    $out .= "<td>$qInStock in stock</td>";
	}
	return $out;
    }
    /*----
      NOTE: This seems to basically duplicate SummaryLine_HTML_line(),
	but using a slightly different set of fields.
      TODO: (2016-04-10) straighten this out so we don't have two functions basically
	doing the same thing.
    * /
    protected function RenderSummary_HTML_line() {
	$out = "\n"
	  .$this->TitleHREF()
	  .$this->CatNum()
	  .' &ldquo;'
	  .$this->NameString()
	  .'&rdquo;</a>: '
	  .$this->RenderStatus_HTML_line()
	  .'<br>'
	  ;
	return $out;
    }
    static private $isOdd=FALSE;
    protected function RenderSummary_HTML_row() {
	self::$isOdd = !self::$isOdd;
	$cssClass = self::$isOdd?'catalog-stripe':'catalog';
	$out = "\n<tr class='$cssClass'>"
	  .'<td>'.$this->TitleHREF().$this->CatNum().'</a></td>'
	  .'<td>&ldquo;'.$this->TitleHREF().$this->TitleString().'</a>&rdquo;</td>'
	  .$this->RenderStatus_HTML()
	  .'</tr>'
	  ;
	return $out;
    }
    /*----
      HISTORY:
	2018-02-16 created for home page random-titles display
    * /
    public function RenderImages_withLink_andSummary() {
	$htTitle = $this->NameString();
	$htStatus = $this->RenderCatalogSummary_HTML();
	$qStock = $this->QtyInStock();
	$htStock = "$qStock in stock";
	$htPopup = "&ldquo;$htTitle&rdquo;%attr%\n$htStatus\n$htStock";
	$htImgs = $this->RenderImages_forRow($htPopup,vctImages::SIZE_THUMB);
	$htTitle = $this->ShopLink($htImgs);
	return $htTitle;
    }
    protected function RenderSummary_inactive() {
	$sText = $this->CatNum()
	.' &ldquo;'
	.$this->NameString()
	.'&rdquo;'
	;
	return $sText;
    }

    // -- FIELD CALCULATIONS -- //
    // ++ ARRAYS ++ //

    // 2018-02-10 if anything is still calling this, it will error out
    protected function ItemStats() {
	return $this->Table()->ItemStats()[$this->KeyValue];
    }
    /* 2018-02-08 This appears to be unused.
    protected function ItemsForSale() {
	return $this->ItemStats()['QtyForSale'];
    } * /
    // ACTION: compile master array of all titles in recordset
    public function CompileMaster() {
	// 2016-11-06 This should now accomplish the same thing as the old code:
	$ar = $this->FetchRows_asKeyedArray();

	/* 2016-11-06 old code
	if ($this->HasRows()) {
	    $sKeyName = $this->GetKeyName();
	    while ($this->NextRow()) {
		$row = $this->GetFieldValues();
		$id = $row[$sKeyName];
		unset($row[$sKeyName]);
		$ar[$id] = $row;
	    }
	} else {
	  $ar = NULL;
	} * /

	return $ar;
    }
    /*----
      ACTION: add additional info for active titles
      RETURNS: structured array
    * /
    public function CompileActive(array $ar) {
	$arKeysAll = array_keys($ar);

	// 2016-11-06 new code
	$arActive = $this->FetchRows_asKeyedArray();
	if (is_null($arActive)) {
	    // nothing is active
	    $arKeysActive = array();
	    // ...so all are retired:
	    $arKeysRetired = $arKeysAll;
	} else {
	    // some things are active
	    // active rows have additional fields which need to be merged into the master array:
	    foreach ($arActive as $id => $arRow) {
		$ar[$id] = array_merge($ar[$id],$arRow);
	    }

	    // calculate the retired key list:
	    $arKeysActive = array_keys($arActive);			// get keys for active rows
	    $arKeysRetired = array_diff($arKeysAll,$arKeysActive);	// the retired list is [all minus active]
	}

	/* 2016-11-06 old code
	if ($this->HasRows()) {
	    $sKeyName = $this->GetKeyName();
	    while ($this->NextRow()) {
		$row = $this->GetFieldValues();
		$id = $row[$sKeyName];
		unset($row[$sKeyName]);
		$ar[$id] = array_merge($ar[$id],$row);
		$arKeysActive[] = $id;
	    }
	    $arKeysRetired = array_diff($arKeysAll,$arKeysActive);
	} else {
	    $arKeysRetired = $arKeysAll;
	}
	* /

	$arOut['data'] = $ar;
	$arOut['active'] = $arKeysActive;
	$arOut['retired'] = $arKeysRetired;

	return $arOut;
    }
    /*
      ASSUMES: There is at least one row
      OUTPUT: via Stats_ForSaleText(), Stats_RetiredText()
    * /
      /* 2018-02-07 apparently nothing is using these?
    public function GatherStats() {
	while ($this->NextRow()) {
	    $sCatNum = $this->CatNum();
	    $htLink = $this->ShopLink($sCatNum);
	    $sName = $this->NameString();
	    $sStats = $this->ItemSummary();

	    $htTitle = "\n$htLink &ldquo;$sName&rdquo;";
	    $qAct = $this->ItemsForSale();	// TODO: make sure this isn't another lookup
	    if ($qAct > 0) {
		$htForSaleTxt .= $htTitle.' - '.$sStats.'<br>';
	    } else {
		if (!is_null($htRetiredTxt)) {
		    $htRetiredTxt .= ' / ';
		}
		$htRetiredTxt .= $htTitle;
	    }
	}
	$this->Stats_ForSaleText($htForSaleTxt);
	$this->Stats_RetiredText($htRetiredTxt);
    }
    private $sStatsForSale;
    public function Stats_ForSaleText($s=NULL) {
	if (!is_null($s)) {
	    $this->$sStatsForSale = $s;
	}
	return $this->$sStatsForSale;
    }
    private $sStatsRetired;
    public function Stats_RetiredText($s=NULL) {
	if (!is_null($s)) {
	    $this->$sStatsRetired = $s;
	}
	return $this->$sStatsRetired;
    }
    */
    /*----
      INPUT:
	* $ar: output from $this->StatsArray_for*()
      OUTPUT: returns array
	array['act']['text']: text listing of available titles
	array['act']['imgs']: thumbnails of available titles
	array['ret']['imgs']: thumbnails of unavailable titles
    * /
    public function RenderTitleResults(array $ar) {
	$arRows = $ar['data'];
	if (array_key_exists('active',$ar)) {
	    $arActive = $this->RenderActiveTitlesResult($ar['active'],$arRows);
	} else {
	    $arActive = NULL;
	}
	if (array_key_exists('retired',$ar)) {
	    $ht = $this->RenderRetiredTitlesResult($ar['retired'],$arRows);
	} else {
	    $ht = NULL;
	}
	$arOut = array(
	  'act' => $arActive,
	  'ret' => $ht
	  );
	return $arOut;
    }
    protected function RenderActiveTitlesResult(array $arIDs,array $arRows) {
	$htText = NULL;
	$htImgs = NULL;
	if (count($arIDs) > 0) {
	    foreach ($arIDs as $id) {
		$this->UnloadCurrentRow();
		$this->SetFieldValues($arRows[$id]);
		$this->SetKeyValue($id);
		$arRes = $this->RenderActiveTitleResult();
		$htText .= $arRes['text'];
		$htImgs .= $arRes['imgs'];
	    }
	}

	if ($this->RenderOption_UseTable()) {
	    $htText = "\n<table class='catalog-summary'>"
	      ."\n<tr><th>Catalog #</th><th>Name</th><th>$ Price</th><th>Options</th></tr>"
	      .$htText
	      ."\n</tr></table>"
	      ;
	}

	$arOut = array(
	  'text' => $htText,
	  'imgs' => $htImgs
	  );
	return $arOut;
    }

    // -- ARRAYS -- //
    // ++ UI ELEMENTS ++ //

    public function RenderImages() {
	$outYes = $outNo = NULL;
	while ($this->NextRow()->GetIsFound()) {
	    $htTitle = $this->RenderImages_withLink_andSummary();
	    if ($this->IsForSale()) {
		$outYes .= $htTitle;
	    } else {
		$outNo .= $htTitle;
	    }
	}

	if (is_null($outYes)) {
	    $htYes = '<span class=content>There are currently no titles available for this topic.</spaN>';
	} else {
	    $oSection = new vcHideableSection('hide-available','Titles Available',$outYes);
	    $htYes = $oSection->Render();
	}
	if (is_null($outNo)) {
	    $htNo = NULL;	// no point in mentioning lack of unavailable titles
	} else {
	    $oSection = new vcHideableSection('show-retired','Titles NOT available',$outNo);
	    $oSection->SetDefaultHide(TRUE);
	    $htNo = $oSection->Render();
	}
	return $htYes.$htNo;
     }

    // 2016-04-10 Not sure if this is actually useful. Changed to FALSE.
    static private $bRendOpt_UseTable=FALSE;
    public function RenderOption_UseTable() {
	return self::$bRendOpt_UseTable;
    }

    // USED BY: Search page
    public function RenderThumbs_forRow(array $arImRow) {
	$rcImg = $this->ImageInfoQuery()->SpawnRecordset();
	$htImg = NULL;
	$sTitle = $this->RenderSummary_text();
	foreach ($arImRow as $idImg => $arImg) {
	    $rcImg->SetFieldValues($arImg);
	    $htImg .= $rcImg->RenderInline_row($sTitle,vctImages::SIZE_THUMB);
	}
	$htHref = $this->TitleHREF();
	return $htHref.$htImg.'</a>';
    }//* /
    protected function RenderImages_withLink($sText) {
	$row = $this->GetFieldValues();
	$out = NULL;
	if (array_key_exists('@img',$row)) {	// if there's image data...
	    $arImgs = $row['@img'];
	    $rcImg = $this->ImageInfoQuery()->SpawnRecordset();
	    $htImg = NULL;
	    foreach ($arImgs as $idImg => $arImgRow) {
		$rcImg->SetFieldValues($arImgRow);
		$htImg .= $rcImg->RenderSingle($sText,vctImages::SIZE_THUMB);
	    }
	    $out .= $this->TitleHREF().$htImg."</a>\n";
	}
	return $out;
    }
    protected function RenderActiveTitleResult() {
	$sText = $this->RenderSummary_text();
	$arOut['text'] = $this->RenderSummary_html_line();
	$arOut['imgs'] = $this->RenderImages_withLink($sText);
	//* /
	return $arOut;
    }
    protected function RenderRetiredTitlesResult(array $arIDs,array $arRows) {
	$htImgs = NULL;
	if (count($arIDs) > 0) {
	    foreach ($arIDs as $id) {
		$this->UnloadCurrentRow();	// otherwise we get images being assigned to imageless titles
		$this->SetFieldValues($arRows[$id]);
		$htImgs .= $this->RenderRetiredTitleResult();
	    }
	}
	return $htImgs;
    }
    protected function RenderRetiredTitleResult() {
	$sText = $this->RenderSummary_inactive();
	$out = $this->RenderImages_withLink($sText);
	return $out;
    }
}
*/

