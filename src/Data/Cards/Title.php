<?php namespace Woozalia\VbzCart\Data\Cards;

use Woozalia\Ferret\Data\Base\cCard;
use Woozalia\VbzCart\Config\cSettings;
use Woozalia\VbzCart\Data\Static\Image\csRecords as csImageRecords;
use Woozalia\VbzCart\Data\Static\Image\eSizes;
use Woozalia\VbzCart\Pages\cTitle as cTitlePage;

/**::::
 * TODO: 2023-10-28 Some of this stuff probably belongs in cTitlePage.
 * HISTORY:
 *  2023-10-26 rebuilding from title.php:vtrTitle_shop
 */
class cTitle extends cCard {

    // ++ FIELDS ++ //

    public function TitleID()     : int     { return $this->GetIt('ID'); }
    public function NameString()  : string  { return $this->GetIt('Name'); }

    // ++ RECORDS ++ //

    public function ImageRecords_forRow(eSizes $eSize) {
        $rsImgs = csImageRecords::ActiveRecords_forTitle($this->TitleID(),$eSize);
        return $rsImgs;
    }

    // -- RECORDS -- //
}

