<?php namespace Woozalia\VbzCart\Responders;

use Woozalia\Ferret\Kiosk\cResource;
use Woozalia\Ferret\Kiosk\Resource\cRoot as cResourceRoot;
use Woozalia\Ferret\Kiosk\Responder\caStandard;
use Woozalia\VbzCart\csApp;

abstract class caShop extends caStandard {

    // ++ CONFIG ++ //

    static private $arRspCls = [  // Responder classes
      'cat'     => cCat::class,
      'diag'    => cDiag::class,
      'static'  => cStatic::class,
      'supp'    => cSupp::class,
      'title'   => cTitle::class,
      ];

    // ++ SETUP ++ //

    /**----
     * CEMENT caResponder
     * NOTE: Needs to be in the parent class for any Responders that process a Route.
     */
    protected function RegisterRoutes() {
        $oRoot = new cResourceRoot($this->Request());
        csApp::HomeRoute()->SetIt($oRoot);
        /*
        new cResource('cat'   ,cCat::class    ,$oRoot);
        new cResource('diag'  ,cDiag::class   ,$oRoot);
        new cResource('static',cStatic::class ,$oRoot);
        new cResource('supp'  ,cSupp::class   ,$oRoot);
        cTitle::Route()->SetIt(new cResource('title' ,cTitle::class  ,$oRoot));
        */
    }

    // -- SETUP -- //
    // ++ RESOURCES ++ //

    static private $arRspObj = [];
    static public function ResourceFor(string $sSlug) : cResource {
        if (array_key_exists($sSlug,self::$arRspObj)) {
            $oRes = self::$arResObj[$sSlug];
        } else {
            #$csRes = self::$arResCls[$sSlug];
            $oRoot = csApp::HomeRoute()->GetIt();
            #$oRes = new cResource($sSlug,$csRes,$oRoot);
            $oRes = new cResource($sSlug,$oRoot);
            self::$arRspObj[$sSlug] = $oRes;
        }
        return $oRes;
    }

    // -- RESOURCES -- //
}
