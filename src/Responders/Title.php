<?php namespace Woozalia\VbzCart\Responders;

use Woozalia\VbzCart\Pages\cTitle as cTitlePage;

class cTitle extends caShop {
    // CALLBACK; CEMENT Kiosk\caResponder
    #protected function RegisterRoutes() {}
    protected function DoPage() {
        $oPage = new cTitlePage;
        $oReq = $this->Request();

        // TODO: get data from Request, send it to Page
        echo $oReq->DumpDiags();

        $oPage->DoPage();
    }
}
