<?php namespace Woozalia\VbzCart;

use Woozalia\Ferret\Config\csGlobals;
use Woozalia\Ferret\Config\Defs\cMySQL;
use Woozalia\Ferret\Data\Base\caDatabase;
use Woozalia\Ferret\Data\Mem\Result\cObj;
use Woozalia\Ferret\Diag\Wrap\CodeViewer\Internal\cViewer;
use Woozalia\Ferret\IO\Screen\cOut;
use Woozalia\Ferret\String\cDataMarks;
use Woozalia\Ferret\Web\cURI;
use Woozalia\Ferret\Web\csaApp;
use Woozalia\Ferret\Web\URI\cSite as cSiteURI;
use Woozalia\VbzCart\Config\cSettings;
use Woozalia\VbzCart\Responders\cHome as cHomeResponder;

class csApp extends csaApp {
    static protected function SetupClasses() {
        #echo 'GOT TO '.__FILE__.' line '.__LINE__.'<br>';
        new cSettings;  // instantiate the Settings class we want to use
        // instantiate other needed Global objects
        csGlobals::_Screen(new cOut);
        csGlobals::_WebURIMarks(new cDataMarks('/',':','!'));
        csGlobals::_WebURICodec(new cSiteURI);
        csGlobals::_CodeViewer(new cViewer);
        csGlobals::_Database(caDatabase::FromSlug('vbzcart'));
        self::$osHome = new cObj;
    }
    static protected function HandleRequest() {
        #echo 'GOT TO '.__FILE__.' line '.__LINE__.'<br>';
        $oCodec = csGlobals::WebURICodec();
        $oData = $oCodec->GetDataFromRequest();    // use actual web request URI
        $oResp = new cHomeResponder($oData);
        $oResp->Go();
    }
    /*
    // 2023-10-21 This probably doesn't need to be a method.
    static protected function NewInputURI() { return new cSiteURI(self::WebURIMarks()); }
    static public function NewOutputURI() { return new cURI(self::WebURIMarks()); }
    */

    static private $osHome;   static public function HomeRoute()  : cObj { return self::$osHome; }
}
