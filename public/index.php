<?php namespace Woozalia\VbzCart;
/*
  PURPOSE: VbzCart public HTML application index
*/

use FilesystemIterator; // PHP native class
use Throwable;          // PHP native class
use Woozalia\Ferret\csLoader;
use Woozalia\Ferret\Diag\Wrap\cErrorWrapper;

$fErrLevel = 0
    | E_ALL
    | E_STRICT
    ;
error_reporting($fErrLevel);
if (!ini_get('display_errors')) {
    ini_set('display_errors', 1);
}

// start up Ferreteria

$fpConfigBase = dirname(__DIR__).'/config';

// Find out where Ferreteria is:
require_once($fpConfigBase.'/where.inc.php');
// Start Ferreteria:
require $fpFerreteria.'/start.php';

// Now that Ferreteria and the classoid autoloader are running, we can handle errors better:

try {
    // Find out where the active config files are:
    $fpConfigActive = $fpConfigBase.'/active';
    #echo "CONFIG FOLDER: [$fpConfig]<br>";

    // Load every file in the active configs folder:
    $onDir = new FilesystemIterator( $fpConfigActive.'/', FilesystemIterator::SKIP_DOTS );
    while($onDir->valid()) {
        $onFile = $onDir->current();
        // output is "FILESPEC => FILENAME"
        #echo $onDir->key() . " => " . $onFile->getFilename() . "<br>";
        include $onDir->key();
        $onDir->next();
    }
    // Tell the loader where the VbzCart classoids are:
    $fpSelfRepo = dirname(__DIR__);
    csLoader::SetNSpacePath(__NAMESPACE__,$fpSelfRepo.'/src');

    // Start the application (VbzCart):
    csApp::Go();

} catch(Throwable $e) {
    try {
        $oError = cErrorWrapper::FromThrowable($e);
        echo $oError->RenderReadout();
    } catch(Throwable $e) {
        echo "Error-catcher not working. Unprocessed error:<br><pre>$e</pre>";
    }
}

/*
$kfpAcctRoot = dirname($_SERVER['CONTEXT_DOCUMENT_ROOT']);		// change if httpd server doesn't return correct path
require($kfpAcctRoot.'/site/config/vbzcart/portable/start.php');	// change if keeping config or start.php in a different place
*/
