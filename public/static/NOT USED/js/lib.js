function OpenBig() {
	var sx, sy, wx, wy, wxd, wyd, loc_str;
	sx = screen.availWidth;
	sy = screen.availHeight;
	wx = sx * 0.15;
	wy = sy * 0.15;
	wxd = sx - (2 * wx);
	wyd = sy - (2 * wy);

// need to find a way to detect if window already exists or not, then move it if it's new
// also need a way to get only the integer portion of a number

	loc_str = "screenX=" + wx + ",screenY=" + wy + ",width=" + wxd + ",height=" + wyd + ",outerWidth=" + wxd + ",outerHeight=" + wyd;

	bigwin = window.open("","big","dependent=1,directories=0,location=0,resizable=1,scrollbars=1," + loc_str)
	bigwin.focus();
	return 0;	// don't do the html
}

function PopupReturn() {
// Closes the popup and returns focus to the opening window
	window.opener.focus();
	window.close();
	return 0;
}
