<?php
$list = file('index.lst',FILE_IGNORE_NEW_LINES || FILE_SKIP_EMPTY_LINES || FILE_TEXT);
$arLen = count($list);
$arIdx = rand(0,$arLen-1);
$fn = trim($list[$arIdx]);
//echo 'file='.$fn;
if (function_exists('mime_content_type')) {
// if this exists, then finfo_file() may not work even if it also exists
    $mtype = mime_content_type($fn);
} else {
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mtype = finfo_file($finfo, $fn);
    finfo_close($finfo);
}
//echo '<br>mime='.$mtype;
$fs = filesize($fn);
header("Status: 200 Ok");
header("Content-type: $mtype");
header("Content-length: $fs");
$fp = fopen($fn, 'rb');
fpassthru($fp);