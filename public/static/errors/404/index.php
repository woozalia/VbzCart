<?php
$list = file('index.lst',FILE_IGNORE_NEW_LINES || FILE_SKIP_EMPTY_LINES || FILE_TEXT);
$arLen = count($list);
$arIdx = rand(0,$arLen-1);
$fn = trim($list[$arIdx]);
//echo 'file='.$fn;
if (function_exists('mime_content_type')) {
// if this exists, then finfo_file() may not work even if it also exists
    $mtype = mime_content_type($fn);
} else {
    $finfo = finfo_open(FILEINFO_MIME_TYPE);
    $mtype = finfo_file($finfo, $fn);
    finfo_close($finfo);
}

$out = <<<__END__
<html>
<head>
<title>The Dreaded 404 Page @ vbz.net</title>
<link rel="stylesheet" href="/tools/styles/browse.css" media="screen" />
</head>
<body>
<p />
<center>
<table height=100% width=40%><tr>
<td style="vertical-align: middle; text-align: center;">
<img src="/tools/errors/404/$fn" title="four oh four, page not found (it wasn't my turn to watch it!)" style="margin-top: auto;">
<div class=section-header>
We are terribly sorry, but the page you arrived at does not seem to exist.<br>The webmonster will be flogged promptly.
</div>
</td>
</tr></table>
</center>
</body>
</html>
__END__;

echo $out;

/*
$fs = filesize($fn);
header("Status: 200 Ok");
header("Content-type: $mtype");
header("Content-length: $fs");
$fp = fopen($fn, 'rb');
fpassthru($fp);
*/