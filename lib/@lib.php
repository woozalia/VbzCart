<?php namespace ferreteria\classloader;
/*
PURPOSE: define locations for libraries using modloader.php, and load parts of Ferreteria that are always needed
FILE SET: VbzCart libraries
HISTORY:
  2013-08-29 created
  2014-01-20 moved MediaWiki-specific files into separate config-libs.php
  2018-01-31 now assuming that Ferreteria base has already been loaded
*/

$oLib = $this;

cLoader::LoadLibrary('ferreteria.data');

$oLib->SetBasePath(__DIR__);

$om = $oLib->MakeModule('globals.php');
  $om->AddClass('vcGlobals');
$om = $oLib->MakeModule('session.php');
  $om->AddClass('vcUserSessions');
$om = $oLib->MakeModule('settings.php');
  $om->AddClass('vctSettings');
$om = $oLib->MakeModule('user.php');
  $om->AddClass('vcUserTable');
  $om->AddClass('vcUserRecord');
  $om->AddClass('clsEmailAuth');
$om = $oLib->MakeModule('vbz-crypt.php');
  $om->AddClass('vcCipher');

$om = $oLib->MakeModule('vbz-data.php');
  $om->AddClass('vcBasicTable');
  $om->AddClass('vcShopTable');
  $om->AddClass('vcAdminRecordset');

$oLib->IncludeIndex('/app','@lib.php');
$oLib->IncludeIndex('/cart','@lib.php');
$oLib->IncludeIndex('/logic','@lib.php');
$oLib->IncludeIndex('/page','@lib.php');
$oLib->IncludeIndex('/shop','@lib.php');
