<?php
/*
  PURPOSE: adds globals specific to VbzCart
  HISTORY:
    2017-03-14 started
    2017-05-12 shopping nav icons
  NOTE: see ferreteria/globals.php MakeWebPath_forAppPath() for a discussion of beginning/ending slashes.
  TODO 2019-08-16: replace remaining global constants with Global object methods
*/
define('KSF_CART_ITEM_ARRAY_NAME','item');
define('KSF_CART_ITEM_PFX',KSF_CART_ITEM_ARRAY_NAME.'[');
define('KSF_CART_ITEM_SFX',']');

/*----
  ABSTRACT: needs GetFilePath_forSiteFolders(), GetWebPath_forAppBase()
  NOTE (probably outdated): All of these can be overridden by the site config globals.
*/
abstract class vcGlobals extends fcGlobals {

    // admin settings (should probably be in a separate file)
    
    /*----
      RETURNS: code-key of permission that means "only root can do this"
      WAS: KS_PERM_FE_ROOT
    */
    public function GetString_Permission_RequireRoot() {
	return '*';
    }
    // USAGE: set to TRUE if nonexistent permissions are preventing log-in (SECURITY RISK!! USE ONLY BRIEFLY)
    public function GetFlag_UseAnonymousRoot() {
	return FALSE;
    }

    // control names

    protected function GetItemControlNamePrefix() {
	return 'item';
    }
    public function MakeItemControlName($sItemTag) {
	return $this->GetItemControlNamePrefix()."[$sItemTag]";
    }
    // 2017-05-14 There's probably a more consistent way of doing this, but for now...
    public function GetCartItemsInput() {
	return $_POST[$this->GetItemControlNamePrefix()];
    }
    public function GetButtonName_AddToCart() {
	return 'btnAddItems';
    }
    public function FoundInputButton_AddToCart() {
	return fcHTTP::Request()->GetBool($this->GetButtonName_AddToCart());
    }
    public function GetName_DropDown_ShipToAddresses() {
	return 'recip-choice-id';
    }
    
    // displayed text
    
    public function GetText_forNullDescription() {	// was KS_DESCR_IS_NULL
	return '(none)';
    }
    public function GetText_forBlankDescription() {	// was KS_DESCR_IS_BLANK
	return '(blank)';
    }
    public function GetMarkup_forNullDescription() {	// was KHT_DESCR_IS_NULL
	return '<span style="color: grey; font-style: italic;">'
	  .$this->GetText_forNullDescription()
	  .'</span>'
	  ;
    }
    public function GetMarkup_forBlankDescriotion() {	// was KHT_DESCR_IS_BLANK
	return '<span style="color: grey; font-style: italic;">'
	  .$this->GetText_forBlankDescription()
	  .'</span>'
	  ;
    }
    // WAS: KHTML_TITLE_EXISTS_NO_ITEMS
    public function GetMarkup_forNoItems() {
	// This could eventually be an "info" icon
	$wpIcon = $this->GetWebSpec_forWarningIcon();
	return <<<__END__
<div class=warning-message>
  <img src="$wpIcon" alt="alert"> This title exists in our catalog, but no items are currently available for it.
</div>
__END__;
    }
    // WAS: KHT_CART_MSG
    public function GetText_forContactUsMessage() {
	$wpContact = $this->GetWebPath_forContactPage();
	return "Please <a href='$wpContact'>let us know</a> if you have any questions.";
    }
    /*----
      RETURNS: HTML-formatted message to appear at the top of all pages
      OVERRIDEABLE
    */
    public function GetMarkup_forSitewideTopMessage() {
	return '';
    }
    
    // formats
    
    // OVERRIDEABLE
    public function MakeOrderNumber($nSeq) : string {
	$sSeq = sprintf($this->GetOrderNumberFormat(),$nSeq);
	$sNum = $this->GetOrderNumberPrefix().$sSeq;
	return $sNum;
    }
    // format for converting order sequence to a string
    abstract protected function GetOrderNumberFormat() : string;
    // order number prefix for current batch of orders
    abstract protected function GetOrderNumberPrefix() : string;
    // order number prefix sorting order, in SQL format
    abstract public function GetOrderNumberPrefixSorter() : string;
    
    abstract protected function GetTopicNumberFormat() : string;
    public function GetString_forTopicNumber($id) : string {
	return sprintf($this->GetTopicNumberFormat(),$id);
    }
    public function GetVarName_forOrderNumber() : string {
	return 'ord-num';
    }
    
    // email
    
    /*----
      RETURNS: template string for order-confirmation email sender
      WAS: KS_TEXT_EMAIL_ADDR_SELF
    */
    abstract public function GetTemplate_forOrderEmailSender() : string;
    abstract public function GetEmailAddress_forPrintout() : string;
    // RETURNS: template string for order-confirmation email subject
    abstract protected function GetTemplate_forOrderEmailSubject() : string;
    /*----
      TODO: This should be replaced by a whole-email template (and eventually one for HTML email).
      WAS: KS_TPLT_ORDER_EMAIL_MSG_TOP
    */
    abstract public function GetTemplate_EmailTopMessage() : string;

    // web paths

    /*----
      RETURNS: the web path for a particular image repository, identified by $sRepo
      NOTE: Not sure we want to keep this; it's a relic of deciding to put the images in one place,
	then putting them in a bulk upload area, then putting them in MediaWiki...
    */
    abstract public function GetWebPaths_forImages() : array;
    /*----
      NOTE: Static files don't *have* to be all under one folder, but
	it seems like a good starting assumption.
	Can be overridden by app globals class.
    */
    protected function GetWebPath_forStaticFiles() {
	return $this->GetWebPath_forAppBase().'static/';
    }
      protected function GetWebPath_forStaticImages() {
	  return $this->GetWebPath_forStaticFiles().'img/';
      }
	protected function GetWebPath_forIcons() {
	    return $this->GetWebPath_forStaticImages().'icons/';
	}
      public function GetWebPath_forStyleSheets() {
	  return $this->GetWebPath_forStaticFiles().'css/';
      }
      protected function GetWebPath_JavaScript() {
	  return $this->GetWebPath_forStaticFiles().'js/';
      }
	public function GetWebPath_DTree() {
	    return $this->GetWebPath_JavaScript().'dtree/';
	}
    /*----
      PURPOSE: default home page to which to return the user
      OVERRIDEABLE
    */
    public function GetWebPath_forHome() {
	return $this->GetWebPath_forAppBase();
    }
    public function GetWebPath_forCartPage() {
	return $this->GetWebPath_forAppBase().'cart/';
    }
    public function GetWebPath_forCatalogPages() {
	return $this->GetWebPath_forAppBase().'cat/';
    }
    public function GetWebPath_forSearchPages() {
	return $this->GetWebPath_forAppBase().'search/';
    }
    // TODO: merge with GetWebPath_forStaticFiles()
    public function GetWebPath_forStaticPages() {
	return $this->GetWebPath_forAppBase().'static/';
    }
    public function GetWebPath_forStockPages() {
	return $this->GetWebPath_forAppBase().'stock/';
    }
    public function GetWebPath_forTopicPages() {
	return $this->GetWebPath_forAppBase().'topic/';
    }
    // ASSUMES: Site defaults to https, so we don't have to enforce it at checkout
    public function GetWebPath_forCheckoutPage() {
	return $this->GetWebPath_forAppBase().'checkout/';
    }
    // ASSUMES: Site defaults to https, so we don't have to enforce it at checkout
    public function GetWebPath_forLoginPage() {
	return $this->GetWebPath_forCheckoutPage().'login/';
    }
    
    protected function GetWebPath_forPublicWikiPages() {
	return $this->GetWebPath_forAppBase().'wiki/';
    }
      public function GetWebPath_forPublicWikiPage($sTitle) {
	  return $this->GetWebPath_forPublicWikiPages().$sTitle;
      }
      public function GetWebPath_forHelpPage() {
	  return $this->GetWebPath_forPublicWikiPage('help');
      }
      public function GetWebPath_forContactPage() {
	  return $this->GetWebPath_forPublicWikiPage('contact');
      }

    protected function GetWebPath_forPrivateWikiPages() {
	return $this->GetWebPath_forAppBase().'corp/wiki/';
    }
      public function GetWebPath_forPrivateWikiPage($sTitle) {
	  return $this->GetWebPath_forPrivateWikiPages().$sTitle;
      }
      protected function GetWebPath_forAdminDocs($sTopic) {
	  return $this->GetWebPath_forPrivateWikiPage('VbzCart/'.$sTopic);
      }
      public function GetWebPath_forAdminDocs_TransactionType($sType) {
	  return $this->GetWebPath_forAdminDocs('transaction/types/'.$sType);
      }
    /* 2019-06-16 probably redundant now, unless we need inter-section links
    public function GetWebPath_forAdminPages() {
	return $this->GetWebPath_forAppBase().'admin/';
    }*/
    // These could be anywhere -- ideally the site would define them...
    // ...but I'm planning to obsolete these soon, so hard-wiring for now.
    /* 2017-05-13 If these are being used, they shouldn't be
    public function GetWebPath_forPrivateWiki() {
	return '/corp/wiki/';
    }
    public function GetWebPath_forPublicWiki() {
	return '/wiki/';
    }
    */
    
    // individual files

    public function GetWebPath_forSiteLogo() {
	return $this->GetWebPath_forStaticImages().'logos/v/';
    }
    // RETURNS: alt-text for the site logo (used in non-graphical contexts)
    public function GetString_forSiteLogoText() {
	return 'V';
    }
    // status icons
    public function GetWebSpec_forSuccessIcon() {
	return $this->GetWebPath_forIcons().'button-green-check-20px.png';
    }
    public function GetWebSpec_forWarningIcon() {
	return $this->GetWebPath_forIcons().'button-yellowish-i-20px.png';
    }
    public function GetWebSpec_forErrorIcon() {
	return $this->GetWebPath_forIcons().'button-red-X.20px.png';
    }
    // widget icons
    public function GetWebSpec_RightPointer() {
    	return $this->GetWebPath_forIcons().'arr1-r.gif';
    }
    public function GetWebSpec_DownPointer() {
	return $this->GetWebPath_forIcons().'arr1-d.gif';
    }
    // site nav icons
    public function GetWebSpec_forHomeIcon() {
    	return $this->GetWebPath_forIcons().'home.050pxh.png';
    }
    public function GetWebSpec_forSearchIcon() {
    	return $this->GetWebPath_forIcons().'search.050pxh.png';
    }
    public function GetWebSpec_forCartIcon() {
    	return $this->GetWebPath_forIcons().'cart.050pxh.png';
    }
    public function GetWebSpec_forHelpIcon() {
    	return $this->GetWebPath_forIcons().'/help.050pxh.png';
    }

}
