<?php
/*
  HISTORY:
    2019-03-28 ckout-content.php split off from ckout.php
*/
/*::::
 FORM CAPTURE FUNCTIONS:
    * CaptureCart()
    * CaptureShipping()
    * CaptureBilling()

 CHECKOUT PAGE RENDERING FUNCTIONS (2019-04-15 These have mostly been moved elsewhere.)
    * RenderCart()	- display the shopping cart
    * RenderShipping()	- user enters shipping info
      * RenderPayType()	- subform for selecting payment method
    * RenderBilling()	- user enters billing/payment info
    * RenderConfirm()	- confirm all information entered
      * RenderOrder($iEditable)
    * ReceiveOrder()	- convert cart fields to order record; send email, show receipt

  HISTORY:
    2018-03-19 Can't do rendering in OnRunCalculations() because then it happens before some
      important calculations in other nodes are ready. Ideally there should probably be
      OnRunCalculations_afterSubs() or something, but for now I'm kluging this by calculating
      the render right before returning it.
*/
class vcPageContent_ckout extends vcPageContent {

    // ++ EVENTS ++ //

    /*----
      CEMENT
      ACTION: currently nothing
    */
    protected function OnRunCalculations() {
    echo 'GOT TO '.__METHOD__.'<br>';
	$this->ProcessPage();	// added 2018-07-22
    }
    /*----
      OVERRIDE
      SEE 2018-03-19 note in class history
    */
    public function Render() {
	$this->SetValue( $this->RenderContent() );
	return parent::Render();
    }

    // -- EVENTS -- //
    // ++ OPTIONS ++ //
    
    private $doNavCtrl;	// hide navigation buttons on final confirmation page
    protected function SetShowNavigation($b) {
	$this->doNavCtrl = $b;
    }
    protected function GetShowNavigation() {
	return $this->doNavCtrl;
    }
    private $doBackBtn;
    protected function SetShowBackButton($b) {
	$this->doBackBtn = $b;
    }
    protected function GetShowBackButton() {
	return $this->doBackBtn;
    }
    private $doRefrBtn;
    protected function SetShowRefreshButton($b) {
	$this->doRefrBtn = $b;
    }
    protected function GetShowRefreshButton() {
	return $this->doRefrBtn;
    }
    
    // -- OPTIONS -- //
    // ++ INTERNAL STATES ++ //
    
    private $sPgReq = NULL;
    protected function SetPageKey_Request($sPage) {
	$this->sPgReq = $sPage;
    }
    protected function GetPageKey_Request() {
	return $this->sPgReq;
    }
    
    private $sPgShow = KSQ_PAGE_SHIP;
    public function GetPageKey_forShow() {
	return $this->sPgShow;
    }
    public function SetPageKey_forShow($sSet) {
	$this->sPgShow = $sSet;
    }
    
    private $sSeqData = NULL;
    protected function SetFormSeqData($s) {
	$this->sSeqData = $s;
    }
    protected function GetFormSeqData() {
	return $this->sSeqData;
    }
    protected function IsFormSeq_AnAdvanceRequest($sPageReq) {
	return !is_null($this->sSeqData) && ($sPageReq > $this->GetFormSeqData());
    }

    // -- INTERNAL STATES -- //
    // ++ CLASSES ++ //

    protected function CartsClass() {
	return 'vctShopCarts';
    }

    // -- CLASSES -- //
    // ++ TABLES ++ //
    
    protected function CartTable() {
	return $this->GetDatabase()->MakeTableWrapper($this->CartsClass());
    }
    
    // -- TABLES -- //
    // ++ RECORDS ++ //

    protected function OrderRecord() {
	$rcCart = $this->CartRecord();
	return $rcCart->OrderRecord_orDie();
    }
    /*----
      RETURNS: the current cart record, if usable; otherwise NULL.
    */
    public function GetCartRecord_ifWriteable() {
	$rcSess = $this->GetSessionRecord();
	return $rcSess->GetCartRecord_ifWriteable();
    }
    /*----
      RETURNS: the current cart record, if usable.
	If not usable, throws an exception.
      HISTORY:
	2016-03-09 Was named CartRecord_current_orError() and was in vbz-page.php,
	  but I moved it here and renamed it to just CartRecord() since it is only
	  used in the checkout process, which always wants only the current cart.
    */
    protected function GetCartRecord() {
	$rcCart = $this->GetCartRecord_ifWriteable();
	if (is_null($rcCart)) {
	    throw new exception('We somehow arrived at checkout without a cart being set.');
	}
	return $rcCart;
    }

    // -- RECORDS -- //
    // ++ INPUT ++ //
    
    private $oForm = NULL;
    // PUBLIC because lots of other objects need to access it
    public function GetFormObject() {	// should it be called GetBlobFormObject()?
        if (is_null($this->oForm)) {
            $this->oForm = new vcCartForm('cart');
            $sBlob = $this->GetCartRecord()->GetSerialBlob();
            $this->oForm->GetBlobField()->SetString($sBlob);
        }
        return $this->oForm;
    }
    
    protected function GetFieldsManager() {
	return $this->GetCartRecord()->FieldsManager();
    }
    
    // -- INPUT -- //
    // ++ INPUT PAGES ++ //

    protected function CaptureCart() {
	return $this->CartTable()->HandleCartFormInput();	// check for any cart data changed
    }
    protected function CaptureShipping() {
	$oMgr = $this->GetFieldsManager();
	$oMgr->CaptureShippingPage();
    }    
    protected function CaptureBilling() {
	$oMgr = $this->GetFieldsManager();
	$oMgr->CaptureBillingPage();
    }

    // -- INPUT PAGES -- //
    // ++ INPUT PAGE DETECT ++ //
    
    /*----
      HISTORY:
	2016-03-27 Making this read-only - it gets the string from $_POST by itself,
	  and does any necessary defaulting
	  Also making protected until we know who else needs it.
	2018-07-22 Moved from Page class to Content class.
    */
    protected function GetPageKey_forData() {
	// KSQ_PAGE_CART is the default - it's the only page that doesn't identify itself
	return fcHTTP::Request()->GetString(KSQ_ARG_PAGE_DATA,KSQ_PAGE_CART);
    }
    
    /*----
      ACTION: Checks form input to see which (if any) navigation button was pressed,
	and therefore which form is being requested for display next.
	
	Does not enforce rules about whether to advance or not.
    */
    protected function DetectPageRequest() {
	$gotPgDest = FALSE;
	if (fcHTTP::Request()->GetBool(KSQ_ARG_PAGE_DEST)) {
	    // 2019-04-28 under what circumstances does this happen?
	    $gotPgDest = TRUE;
	    $sPgReq = fcHTTP::Request()->GetString(KSQ_ARG_PAGE_DEST);
	}
	
	// DEBUGGING
	//echo 'DETECTING PAGE REQUEST...<br>';
	
	if (!$gotPgDest) {
	// destination page unknown, so calculate it from data/source page:
	    if (fcHTTP::Request()->GetBool('btn-go-prev')) {
	    
		// PREVIOUS page has been requested
	    
		switch ($this->GetPageKey_forData()) {
		  case KSQ_PAGE_CART:
		    $sPgReq = KSQ_PAGE_CART;	// can't go back any further
		    break;
		  case KSQ_PAGE_SHIP:
		    $sPgReq = KSQ_PAGE_CART;
		    break;
		  case KSQ_PAGE_PAY:
		    $sPgReq = KSQ_PAGE_SHIP;
		    break;
		  case KSQ_PAGE_CONF:
		    $sPgReq = KSQ_PAGE_PAY;
		    break;
		  default:	// source page name not recognized; default to cart
		    $sPgReq = KSQ_PAGE_CART;
		}
	    } elseif (fcHTTP::Request()->GetBool('btn-go-next')) {
	    
		// NEXT page has been requested
	    
		switch ($this->GetPageKey_forData()) {
		  case KSQ_PAGE_CART:
		    $sPgReq = KSQ_PAGE_SHIP;
		    break;
		  case KSQ_PAGE_SHIP:
		    $sPgReq = KSQ_PAGE_PAY;
		    break;
		  case KSQ_PAGE_PAY:
		    $sPgReq = KSQ_PAGE_CONF;
		    break;
		  case KSQ_PAGE_CONF:
		    $sPgReq = KSQ_PAGE_RCPT;
		    break;
		  default:	// not sure how we got here; use default
		    $sPgReq = KSQ_PAGE_DEFAULT;
		}
	    } elseif (fcHTTP::Request()->GetBool('btn-go-same')) {
	    
		// SAME page requested
	    
		$sPgReq = $this->PageKey_forData();
	    } elseif (fcHTTP::Request()->GetBool('btn-go-order')) {
	    
		// SUBMIT ORDER page requested
	    
		$sPgReq = KSQ_PAGE_RCPT;		// receipt page - submits order too
	    } else {
	    
		// no request found - go to default
	    
		$sPgReq = KSQ_PAGE_DEFAULT;	// default page to display
	    }
	}
	$this->SetPageKey_Request($sPgReq);
    }
    /*----
      ACTION: Based on what page the user is *requesting* (as determined by DetectPageRequest())
	and the current conditions (mainly: was the previous page's form filled out properly?),
	determine what page to display next.
    */
    protected function HandlePageRequest() {
	// figure out if user is trying to advance, and if so whether to allow it:
	$sPgReq = $this->GetPageKey_Request();

	// DEBUGGING
	//echo "PAGE KEY FOR SHOW = [$sPgReq]<br>";
	
	// TODO: why don't we just say $nPgReq = $sPgReq, after checking for a valid $sPgReq?
	//  ANSWER: Because nPgReq has to be an ordinal, so we can tell which direction we're moving.
	switch ($sPgReq) {
	  case KSQ_PAGE_CART:	$nPgReq = KI_SEQ_CART;	break;
	  case KSQ_PAGE_SHIP:	$nPgReq = KI_SEQ_SHIP;	break;
	  case KSQ_PAGE_PAY:	$nPgReq = KI_SEQ_PAY;	break;
	  case KSQ_PAGE_CONF:	$nPgReq = KI_SEQ_CONF;	break;
	  case KSQ_PAGE_RCPT:	$nPgReq = KI_SEQ_RCPT;	break;
//	  default: $nPgReq = 0;	break;
	  default: throw new exception("Requested page key is [$sPgReq]. How did this happen?");
	}
	//echo "FORMSEQDATA=[".$this->formSeqData."] FORMSEQSHOW=[$nPgReq]<br>";
	//$didRequestAdvance = !is_null($this->formSeqData) && ($nPgReq > $this->GetFormSeqData());
	$didRequestAdvance = $this->IsFormSeq_AnAdvanceRequest($nPgReq);
	//$this->DidRequestAdvance($didRequestAdvance);

	// 2019-05-04 somehow we need to retrieve the missing-list here now.
	throw new exception('2019-05-05 working here');
	// DEBUGGING
	$this->GetFormObject()->DumpControls();
	
/*	
	$arMissed = $this
	  ->GetCartRecord()
	  ->FieldsManager()		// object that manages checkout data forms
	  ->GetMissingArray()		// array of required fields the user hasn't filled in, so we can complain
	  ; */
	if (count($arMissed) > 0) {
	    $okToAdvance = FALSE;
	    if ($didRequestAdvance) {
		// The user tried to advance, so alert user to any missing fields:
		$sList = NULL;
		foreach ($arMissed as $ctrl) {
		    $sAlias = $ctrl->DisplayAlias();
		    if (!is_null($sList)) {
			$sList .= ', ';
		    }
		    $sList .= $sAlias;
		}
		$sMsg = '<b>Some information is missing</b> &ndash; we really need the following items in order to process your order correctly: <b>'
		  .$sList
		  .'</b>'
		  ;
		  //echo 'ARMISSED:'.fcArray::Render($arMissed);
		  //echo 'COUNT: '.count($arMissed).'<br>';
		//die('MISSED STUFF: '.$sMsg);
		$this->FormAlertMessage($sMsg);
	    }
	} else {
	    // For now, we can move on if there aren't any fields missing.
	    $okToAdvance = TRUE;
	    // Later there might be other reasons not to advance.
	}
	
	//echo "REQ ADVANCE?[$didRequestAdvance] OK TO ADVANCE?[$okToAdvance]";
	if ($didRequestAdvance && !$okToAdvance) {
	    // user tried to advance when it wasn't ok -- stay on the same page
	    $this->SetPageKey_forShow($this->GetPageKey_forData());
	}
    }
    /*----
      PURPOSE: core form processing & page rendering
      ACTIONS:
	* dispatches to ParseInput_Login() & HandleInput_Login() to render/handle login controls
	* logs a Cart event to record which page was displayed
	* sets PageKey_forShow()
	* dispatches to RenderPage() to render the page
      HISTORY:
	2018-07-21
	  * ParseInput_Login() and HandleInput_Login() were defined in files which have now
	    been removed from VbzCart. This *should* now be handled automatically by Ferreteria
	    if the right page component class is used.
	  * Also, PageKey_forShow_default() no longer exists; I'm going to assume
	    that we can just use PageKey_forShow().
      TODO: Consider whether maybe this functionality should be moved into $oContent.
    */
    protected function ProcessPage() {
	$gotPgDest = FALSE;
	$gotPgSrce = FALSE;
	
	// 2019-04-28 Should this bit be moved into HandlePageRequest()?
	$sKey = fcHTTP::Request()->GetString(KSQ_ARG_PAGE_DEST);
	if (is_null($sKey)) {
	    $this->SetPageKey_forShow(KSQ_PAGE_SHIP);
	} else {
	    $gotPgDest = TRUE;
	    $this->SetPageKey_forShow($sKey);
	}
	// [end of "this bit"]

	// get actual page to display
	$sShow = $this->GetPageKey_forShow();
	
	/*
	$arEv = array(
	  fcrEvent::KF_CODE		=> 'PG-IN',
	  fcrEvent::KF_DESCR_FINISH	=> 'showing page "'.$sShow.'"',
	  fcrEvent::KF_WHERE		=> 'HandleInput()',
	  ); */
	$rcCart = $this->GetCartRecord_ifWriteable();
	if (is_null($rcCart)) {
	    $sMsg = fcApp::Me()->MessagesString();
	    $wpCart = vcGlobals::Me()->GetWebPath_forCartPage();
	    if (is_null($sMsg)) {
		// 2016-04-24 This can happen when browser fingerprint changes.
		//throw new exception('In checkout with no current cart set, but there are no error messages.');
		$sMsg = "You don't seem to have a shopping cart yet, so you can't check out. If you had one before, your IP address or browser version may have changed.";
		fcHTTP::DisplayOnReturn($sMsg);
		fcHTTP::Redirect($wpCart);
	    } else {
		fcHTTP::DisplayOnReturn($sMsg);
		fcHTTP::Redirect($wpCart);
	    }
	}
	//$rcEv = $rcCart->CreateEvent($arEv);
	$rcEv = $rcCart->CreateEvent('PG-IN',"showing page '$sShow'");	// 2018-07-22 can add array (3rd arg) with additional info if needed
	$this->doBackBtn = TRUE;
	$this->doRefrBtn = FALSE;
	
	$this->DetectPageRequest();
	$this->HandlePageRequest();
	
	$this->CapturePage();

	//$this->GetSkinObject()->Content('main',$this->RenderPage());
    }

    // -- INPUT PAGE DETECT -- //
    // ++ INPUT DISPATCH ++ //
    
    /*
      Each of these functions dispatches to page-specific methods.
	* ProcessPage() dispatches to CapturePage() and RenderPage()
	  * CapturePage() dispatches to page-specific Capture*() methods, and also
	    sets the Form Alert to show any missing fields.
	  * RenderPage() dispatches to RenderContent()
	    * RenderContent() dispatches to page-specific Render*() methods
    */
    
    /*----
      ACTION: receives form input and determines where to save it
	enforces form rules -- mainly, don't allow advance until all required fields are filled in
      OUTPUT:
	* logs Cart event recording which page's form was received
	* sets $this->PageKey_forShow()
	* sets $this->DidRequestAdvance()
      REQUIRES: Input needs to have been parsed so we know what page's data we're capturing
    */
    protected function CapturePage() {
	$sPKeyData = $this->GetPageKey_forData();

	// log as system event
	/*
	$arEv = array(
	  fcrEvent::KF_CODE		=> 'CK-PG-REQ',	// checkout page request
	  fcrEvent::KF_DESCR_START	=> 'saving data from page '.$sPKeyData,
	  fcrEvent::KF_WHERE		=> 'ckout.CapturePage',
	  fcrEvent::KF_PARAMS		=> array(
	    'pgData'	=> $sPKeyData,
	    'pgShow'	=> $this->GetPageKey_forShow()
	    )
	  );
	*/
	
	// log cart progress
	$arEv = array(
	    'pgData'	=> $sPKeyData,
	    'pgShow'	=> $this->GetPageKey_forShow()
	    );
	$rcCart = $this->GetCartRecord();
	$rcSysEv = $rcCart->CreateEvent(
	  'CK-PG-REQ',				// CODE: checkout page request
	  'saving data from page '.$sPKeyData,	// TEXT
	  $arEv					// DATA
	  );
	  
	// DEBUGGING
	//echo "PAGE KEY FOR DATA = [$sPKeyData]<br>";
	//$arStat = $this->GetFormObject()->Receive($_POST);

	$out = NULL;	// no processing needed
	switch ($sPKeyData) {
	  case KSQ_PAGE_CART:	// shopping cart
	    $out = $this->CaptureCart();
	    $this->formSeqData = KI_SEQ_CART;	// 0
	    break;
	  case KSQ_PAGE_SHIP:	// shipping information
	    // 2019-05-05 this seems to be redundant now
	    //$out = $this->CaptureShipping();
	    $this->formSeqData = KI_SEQ_SHIP;	// 1
	    break;
	  case KSQ_PAGE_PAY:	// billing information
	    $out = $this->CaptureBilling();
	    $this->formSeqData = KI_SEQ_PAY;	// 2
	    break;
	  case KSQ_PAGE_CONF:	// confirmation
	    $out = NULL;	// no processing needed
	    $this->formSeqData = KI_SEQ_CONF;	// 3
	    break;
	  default:
	    $this->formSeqData = NULL;
	    // more likely to be a hacking attempt than an internal error:
	    $out = "Cannot save data from unknown page [$sPKeyData]";
	    $arEv = array(
	      fcrEvent::KF_DESCR_FINISH	=> 'page not recognized',
	      fcrEvent::KF_IS_ERROR	=> TRUE,
	      );
	    $rcSysEv->Finish($arEv);
	}

	$rcCart->UpdateBlob();
	
	// controls need to get added by this point
	echo 'RECEIVING FORM IN ['.$this->GetName().']<br>';
	$oRes = $this->GetFormObject()->Receive($_POST);	// returns fcFormResult
	
	$this->GetFormObject()->Save();

	return $out;
    }

    // -- INPUT DISPATCH -- //
    // ++ OUTPUT PAGES ++ //

    /*----
      PURPOSE: This displays the cart page in the checkout sequence.
	It only shows up if the user navigates back from the shipping page.
      HISTORY:
	2018-09-17 moved from Page object to Content object
    */
    public function RenderCart() {
	$rcCart = $this->GetCartRecord();
	if ($rcCart->HasLines()) {
	    // this is the read-only confirmation version of the cart
	    $htCore = $rcCart->Render(FALSE);
	    return $htCore;
	} else {
	    return 'No items in cart!';
	    // TODO: log this as a critical error - how did the user get to the checkout with no items?
	}
    }
    /*----
      ACTION: Renders all sections of the shipping information entry page:
	* basic contact info (email, mainly) section
	* shipping section
	* payment type selection
    */
    /* 2019-05-30 obsolete
    protected function RenderShippingPage() {
	$oMgr = $this->GetFieldsManager();
	return $oMgr->RenderShippingPage();
    }
    protected function RenderBillingPage() {
	$oMgr = $this->GetFieldsManager();
	return $oMgr->RenderBillingPage();
    }*/

    // -- OUTPUT PAGES -- //
    // ++ OUTPUT DISPATCH ++ //

    /*----
      ACTION: 
      OUTPUT:
	$doBackBtn: if TRUE, show the BACK navigation button
	$doRefrBtn:
	$doNavCtrl:
    */
    protected function RenderContent() {
	$out = NULL;
	$this->SetShowNavigation(TRUE);	// default
	//$doNav = TRUE;

	$sKeyShow = $this->GetPageKey_forShow();
	switch ($sKeyShow) {
	  case KSQ_PAGE_CART:	// shopping cart
	    $this->SetShowBackButton(FALSE);
	    $this->SetShowRefreshButton(TRUE);
	    $htMain = $this->RenderCart();
	    break;
	  case KSQ_PAGE_SHIP:	// shipping information
	    $htMain = $this->RenderShippingPage();
	    break;
	  case KSQ_PAGE_PAY:	// billing information
	    $htMain = $this->RenderBillingPage();
	    break;
	  case KSQ_PAGE_CONF:	// confirm order
	    $this->SetShowNavigation(FALSE);
	    //$doNav = FALSE;
	    $htMain = $this->RenderConfirm();
	    break;
	  case KSQ_PAGE_RCPT:	// order receipt
	    $this->SetShowNavigation(FALSE);
	    //$doNav = FALSE;
	    $this->ReceiveOrder();		// mark order as received
	    $htMain = $this->RenderReceipt();	// display receipt & send by email
	    break;
	  default:
// The normal shopping cart does not specify a target within the checkout sequence
// ...so show the first page which follows the cart page:
	    $htMain = $this->RenderShippingPage();
	}
	
	//$htNavStatus = $this->RenderNavigationStatus();
	
	$out =
	  $this->RenderContentHeader()
	  //.$htNavStatus
	  .$htMain
	  //.$htNavButtons
	  .$this->RenderContentFooter()
	  ;
	return $out;
    }
    
    // -- OUTPUT DISPATCH -- //
    // ++ OUTPUT PIECES ++ //
    
    /*----
      PURPOSE: Render top part of {form and outer table, including <td>}
      TODO: move this stuff into the skin, somehow
      CALLED BY: $this->HandleInput()
    */
    protected function RenderContentHeader() {
	$sWhere = __METHOD__."() in ".__FILE__;

	if ($this->GetShowNavigation()) {
	    $htNav = $this->RenderNavigationButtons();
	} else {
	    $htNav = '';
	}
//	$urlTarg = KURL_CKOUT;

	$out = <<<__END__

<!-- vv $sWhere vv -->
<form method=post name=checkout>
<table class="form-block" id="page-ckout">
$htNav
<tr>
<td>
<!-- ^^ $sWhere ^^ -->
__END__;
	return $out;
    }
    /*----
      PURPOSE: Stuff to close out the page contents
      ACTION: Close table row opened in RenderContentHdr(), display standard buttons, close outer table and form
    */
    protected function RenderContentFooter() {
	$sWhere = __METHOD__."() in ".__FILE__;;
	$out = "\n<!-- vv $sWhere vv -->\n"
	  //. $this->RenderNavButtons()
	  ;

	$rcCart = $this->GetCartRecord();
	$idSess = $this->GetSessionRecord()->GetKeyValue();
	$idCart = $rcCart->GetKeyValue();
	$idOrd = $rcCart->GetOrderID();
	$sOrd = ($idOrd == 0)?'':' Order ID: <b>'.$idOrd.'</b>';
	if ($this->doNavCtrl) {
	    $htLine = NULL;
	} else {
	    $htLine = '<hr>';	// TODO: Make this a Skin function (again)
	}
	$htNav = $this->RenderNavigationStatus();

	$out .= <<<__END__
</td></tr>
<tr><td align=center>
$htNav
</td></tr>
</table>
$htLine
<span class="footer-stats">Cart ID: <b>$idCart</b> Session ID: <b>$idSess</b>$sOrd</span>
</form>

<!-- ^^ $sWhere ^^ -->

__END__;

	return $out;
    }
    protected function RenderNavigationStatus() {
	$oNav = $this->CreateNavigationBar();
	// call this after CreateNavBar so child classes can insert stuff first
	//$oPage = vcApp::Me()->GetPageObject();
	$sPage = $this->GetPageKey_forShow();
	$oNav->States($sPage,1,3);
	$oNav->GetNode($sPage)->State(2);
	//return $this->GetSkinObject()->RenderNavbar_H($oNav);	// pretty sure that won't work...
	return $oNav->Render();
    }
    protected function RenderNavigationButtons() {
	if ($this->doBackBtn) {
	    $htBackBtn = '<input type=submit name="btn-go-prev" value="&lt; Back">';
	} else {
	    $htBackBtn = '';
	}
	if ($this->doRefrBtn) {
	    $htRefrBtn = '<input type=submit name="btn-go-same" value="Update">';
	} else {
	    $htRefrBtn = '';
	}
	$out =
	  $this->RenderFormAlert()	// if any
	  .'<tr><td colspan='.KI_CKOUT_COLUMN_COUNT.' align=center bgcolor=ffffff class=section-title>'
	  .'<input type=hidden name="'.KSQ_ARG_PAGE_DATA.'" value="'.$this->GetPageKey_forShow().'">'
	  .$htBackBtn.$htRefrBtn
	  .'<input type=submit name="btn-go-next" value="Next &gt;">'
	  ;
	return $out;
    }
    /*----
      HISTORY:
	2016-03-18 Changing this from a message specific to missing-form-elements to a general
	  form-alert display. Renamed from RenderMissing() to RenderFormAlert().
	
	  The text "<b>Please fill in the following</b>: " used to be prepended, but now that
	  should be included in FormAlertMessage().
    */
    protected function RenderFormAlert() {
	$nCols = KI_CKOUT_COLUMN_COUNT;
	$sMsg = $this->FormAlertMessage();
	if (is_null($sMsg)) {
	    $out = NULL;
	} else {
	    // NOTE: I've been unable to get the icon to align nicely with the text without using a table.
	    $out = "<tr><td colspan=$nCols>\n<table>\n<tr><td><img src=".'"'.KWP_ICON_ALERT
	      .'"></td><td valign=middle><span class=alert style="background: yellow">'
	      //.$this->MissingString()
	      .$sMsg
	      ."</span></td></tr>\n</table>\n</td></tr>";
	}
	return $out;
    }
    private $htFormAlert;
    protected function FormAlertMessage($ht=NULL) {
	if (is_null($ht)) {
	    if (!isset($this->htFormAlert)) {
		$this->htFormAlert = NULL;
	    }
	} else {
	    $this->htFormAlert = $ht;
	}
	return $this->htFormAlert;
    }

    // -- OUTPUT PIECES -- //
    // ++ OUTPUT COMPONENT ++ //

    protected function CreateNavigationBar() {
	$oNav = new fcNavbar_flat();
	  $oi = new fcNavText($oNav,KSQ_PAGE_CART,'Cart');
	  $oi = new fcNavText($oNav,KSQ_PAGE_SHIP,'Shipping');
	  $oi = new fcNavText($oNav,KSQ_PAGE_PAY,'Payment');
	  $oi = new fcNavText($oNav,KSQ_PAGE_CONF,'Final Check');
	  $oi = new fcNavText($oNav,KSQ_PAGE_RCPT,'Receipt');
	$oNav->Decorate('','',' &rarr; ');
	$oNav->CSSClass('nav-item-past',1);
	$oNav->CSSClass('nav-item-active',2);
	$oNav->CSSClass('nav-item-todo',3);
	return $oNav;
    }
    
    // -- OUTPUT COMPONENT -- //

}
