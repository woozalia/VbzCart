<?php
/*
  PURPOSE: some utility classes for building forms
    These might eventually get ported back into Ferreteria when I figure out how they fit in.
  HISTORY:
    2019-06-05 separated out from form.php to fields.php
*/
abstract class vcFormElement {
    // ++ SETUP ++ //

    public function __construct($sText) {
	$this->SetText($sText);
    }

    private $sText;
    public function SetText($s) {
	$this->sText = $s;
    }
    protected function GetText() {
	return $this->sText;
    }
    
    abstract public function Render();
}
abstract class vcFormFieldTemplate extends vcFormElement {

    // ++ SETUP ++ //

    public function __construct($sName,$sLabel) {
	$this->SetName($sName);
	$this->SetText($sLabel);
    }

    private $sName;
    protected function SetName($s) {
	$this->sName = $s;
    }
    protected function GetName() {
	return $this->sName;
    }
            
    // -- SETUP -- //
}
class vcFormFieldTemplate_inline extends vcFormFieldTemplate {

    // ++ SETUP ++ //

    public function __construct($sName,$sLabel,$sAfter=NULL) {
	parent::__construct($sName,$sLabel);
	$this->SetAfter($sAfter);
    }

    private $sAfter;
    public function SetAfter($s) {
	$this->sAfter = $s;
    }
    protected function GetAfter() {
	return $this->sAfter;
    }

    // -- SETUP -- //
    // ++ RENDERING ++ //
    
    public function Render() {
	$sLabel = $this->GetText();
	$sName = $this->GetName();
	$sAfter = $this->GetAfter();
	return "\n  <tr><td align=right>$sLabel:</td><td>[[$sName]]$sAfter</td></tr>";
    }
    
    // -- RENDERING -- //
}
class vcFormFieldTemplate_block extends vcFormFieldTemplate {
    public function Render() {
	$sLabel = $this->GetText();
	$sName = $this->GetName();
	$out = "\n<tr><td align=center colspan=2>"
	  .$sLabel.':'
	  ."\n<br>[[$sName]]"
	  .'</td></tr>'
	  ;
	return $out;
    }
}
class vcFormFieldTemplate_raw extends vcFormElement {
    public function Render() {
	$sText = $this->GetText();
	$out = "\n<tr><td align=center colspan=2>"
	  .$sText
	  .'</td></tr>'
	  ;
	return $out;
    }
}
class vcFormFields {

    // ++ SETUP ++ //

    public function __construct(array $arFields) {
	$this->SetFields($arFields);
    }

    private $oZone;
    protected function SetCountryZone(vcShipCountry $oZone) {
	$this->oZone = $oZone;
    }
    protected function GetCountryZone() : vcShipCountry {
	return $this->oZone;
    }

    private $arFields;
    protected function SetFields(array $ar) {
	$this->arFields = $ar;
    }
    protected function GetFields() : array {
	return $this->arFields;
    }
    protected function GetField($sName) {
	return $this->arFields[$sName];
    }
    
    // -- SETUP -- //
    // ++ RENDER ++ //
    
    protected function RenderFields() {
	$out = NULL;
	foreach($this->GetFields() as $sName => $oField) {
	    $out .= $oField->Render();
	}
	return $out;
    }
    // -- RENDER -- //
    // ++ TEMPLATE ++ //
    
    public function BuildTemplateString() {
	return
	  "\n<table class='form-block' id='name-address'>"
	  .$this->RenderFields()
	  ."\n</table>"
	  ;
    }
    public function BuildTemplate() {
	$sTplt = $this->BuildTemplateString();
	return new fcTemplate_array('[[',']]',$sTplt);
    }

    // -- TEMPLATE -- //
}
