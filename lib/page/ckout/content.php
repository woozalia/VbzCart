<?php
/*
  HISTORY:
    2019-03-28 ckout-content.php split off from ckout.php
    2019-05-05 reconstructing piece by piece, mostly based on old version (now in "../NOT USED")
*/
require('options.php');
require('form.php');

use \vbzcart\checkout\cOptions as vcCheckoutOptions;

class vcPageContent_ckout extends vcPageContent {
    use vtCheckoutForm;	// this is kinda where the form is configured

    const KS_INTYPE_EXISTING = 'old';
    const KS_INTYPE_NEWENTRY = 'new';
    
    /*
      HISTORY:
	2016-11-01 Recreating these with what seem like sane values; not sure what happened to the originals.
	2019-07-16 moved from page.php (where they were global constants) to vcPageContent_ckout as class constants
    */
    const KSQ_ARG_PAGE_DEST = 'dest';
    const KSQ_ARG_PAGE_DATA = 'data';

    // ++ EVENTS ++ //

    /*----
      CEMENT
      ACTION: currently nothing
    */
    protected function OnRunCalculations() {
	$this->DoPage();	// added 2018-07-22
    }
    /*----
      OVERRIDE
      SEE 2018-03-19 note in class history
    */
    public function Render() {
	$this->SetValue( $this->RenderContent() );
	return parent::Render();
    }

    // -- EVENTS -- //
    // ++ OPTIONS ++ //
    
    private $doNavCtrl;	// hide navigation buttons on final confirmation page
    protected function SetShowNavigation($b) {
	$this->doNavCtrl = $b;
    }
    protected function GetShowNavigation() {
	return $this->doNavCtrl;
    }

    protected function SetLeftNavigationIsRefresh() {
	$this->SetLeftNavigationText('Update');
	$this->SetLeftNavigationName('btn-go-same');
    }
    protected function SetNavigationForConfirm() {
	$this->SetLeftNavigationText('&lt; Make Changes');
	$this->SetRightNavigationText('Place the Order!');
    }
    
    private $sLeftNavText = '&lt; Back';	// default
    protected function SetLeftNavigationText(string $s) {
	$this->sLeftNavText = $s;
    }
    protected function GetLeftNavigationText() : string {
	return $this->sLeftNavText;
    }
    
    private $sLeftNavName = 'btn-go-prev';	// default
    protected function SetLeftNavigationName(string $s) {
	$this->sLeftNavName = $s;
    }
    protected function GetLeftNavigationName() : string {
	return $this->sLeftNavName;
    }
    
    private $sRightNavText = 'Next &gt;';
    protected function SetRightNavigationText(string $s) {
	$this->sRightNavText = $s;
    }
    protected function GetRightNavigationText() : string {
	return $this->sRightNavText;
    }
    
    protected function GetRightNavigationName() : string {
	return 'btn-go-next';	// currently never changes
    }
    
    // -- OPTIONS -- //
    // ++ INTERNAL STATES ++ //

    private $oRecvStat = NULL;
    private $isLoaded = FALSE;	// old values loaded from db?
    /*
    protected function SetFormStats(fcFormResult $oRecvStat) {
	$this->oRecvStat = $oRecvStat;
    }*/
    protected function SetIsLoaded() {
	$this->isLoaded = TRUE;
    }
    protected function GetFormStats() {
	if ($this->GetIsPageDataExpected()) {
	    if (!$this->isLoaded) {
		throw new exception('VbzCart internal error: receiving new data before old data has been loaded from db.');
	    }
	    if (is_null($this->oRecvStat)) {
	    //echo 'RECEIVING FORM DATA...<br>';
		$oStat = $this->GetFormBlobject()->Receive($_POST);	// returns fcFormResult
	    //echo 'RECEIVED.<br>';
		//$this->SetFormStats($oStat);
		$this->GetFormBlobject()->Save();
		$this->oRecvStat = $oStat;
	    }
	} else {
	    $this->oRecvStat = NULL;	// 2019-08-14 this may cause an error, but let's try it
	}
	return $this->oRecvStat;
    }

    // -- INTERNAL STATES -- //
    // ++ CLASSES ++ //

    protected function CartsClass() {
	return 'vctShopCarts';
    }

    // -- CLASSES -- //
    // ++ TABLES ++ //
    
    protected function CartTable() {
	return $this->GetDatabase()->MakeTableWrapper($this->CartsClass());
    }
    
    // -- TABLES -- //
    // ++ RECORDS ++ //

    protected function OrderRecord() {
	$rcCart = $this->CartRecord();
	return $rcCart->OrderRecord_orDie();
    }
    /*----
      RETURNS: the current cart record, if usable; otherwise NULL.
      HISTORY:
	2019-08-05 It doesn't actually make sense to decide that the cart should be read-only after a certain point.
    */
    public function GetCartRecord() {
	$rcSess = $this->GetSessionStatus();
	$rcCart = $rcSess->GetCartRecord_ifKnown();
	if (is_null($rcCart)) {
	    throw new exception("VbzCart internal error: We somehow arrived at checkout without a cart.");
	}
	return $rcCart;
    }

    // -- RECORDS -- //
    // ++ OTHER OBJECTS ++ //

    private $oForm = NULL;
    /*----
      PUBLIC because lots of other objects need to access it
      HISTORY:
	2019-08-05 rewrote this to get Cart ID directly from Session object
    */
    public function GetFormBlobject() {	// should it be called GetBlobFormObject()?
        if (is_null($this->oForm)) {
	    //$idCart = $this->GetCartRecord()->GetKeyValue();
	    
	    $rcSess = $this->GetSessionStatus();
	    $idCart = $rcSess->GetCartID();
	    
            $this->oForm = new vcfCartBlob($idCart);	// cart ID is just for debugging
            //$sBlob = $this->GetCartRecord()->GetSerialBlob();
            //$this->oForm->GetBlobField()->SetString($sBlob);
        }
        return $this->oForm;
    }
    protected function GetShipCountry() : vcShipCountry {
	$sCode = $this->GetCartRecord()->GetZoneCode();
	$oZone = vcShipCountry::Spawn($sCode);
	return $oZone;
    }
    protected function LoadFields() {
	// copy blob data to field objects
	$oForm = $this->GetFormBlobject();
	$oBlob = $oForm->GetBlobField();
	//echo 'BLOB CLASS: '.get_class($oBlob).'<br>';
	$oBlob->Load();
	$oForm->LoadFields_fromBlob();
	//echo $oBlob->Dump();	// debugging
	$this->SetIsLoaded();	// set flag: ok to receive form data now
    }
    protected function SaveFields() {
	$oForm = $this->GetFormBlobject();
	// copy field objects to data blob
	$oForm->SaveFields_toBlob();
	$oBlob = $oForm->GetBlobField();
	$oBlob->Save();	// redundant?
	//$sBlob = $oBlob->GetString();
	
	//$rcCart = $this->GetCartRecord();
	//$rcCart->SetSerialBlob($sBlob);
	//$rcCart->Save();
    }
    /*----
      TODO 2019-06-29 It would probably be best if this were done in the same place where the cart data is originally loaded.
		...wherever that may be. Figure it out later.
      NOTE 2019-06-11 I'm not sure why this is needed and yet there's no LoadCart() function that needs to be called...?
    */
    protected function SaveCart() {
	//echo 'ENDING ROW DATA:'.fcArray::Render($this->GetCartRecord()->GetFieldValues());
	$this->GetCartRecord()->Save();
    }
    /*----
      ACTION: Do whatever needs to be done to make the order officially received.
      TODO: Probably move this to the Content object.
    */
    protected function ReceiveOrder() {
	$rcCart = $this->GetCartRecord();
	//$rcOrd = $rcCart->OrderRecord_orDie();	// get existing order record
	$rcOrd = $rcCart->OrderRecord_orConvert($this);	// if order ID set, use that record; otherwise get new order record
	$rcOrd->MarkAsPlaced();
    }
        
    // -- OTHER OBJECTS -- //
    // ++ EXECUTIVE ++ //
    
    /*----
      DOCS: https://htyp.org/VbzCart/v1/class/vcPageContent_ckout/DoPage
      HISTORY:
	2019-06-29 I had been dividing this into 2 phases, "receive" and "show", but it turns out we
	  need to do a lot of operations for both phases together.
	2019-06-30 In fact, we pretty much *have* to just load all the controls all the time:
	  - We need to load the received-page controls so we know whether we can advance
	  - We need to load the data for that page before we know what page to display
	  - Once we know what page to display, we need to load the controls for that page too.
	  ...although I'm first going to try always loading the wanted-show page, because if
	  we *don't* advance, then we're just displaying the received-data page again, which
	  is already loaded.
    */
    protected function DoPage() {
    
	$nPgData = $this->GetPageKey_forData();		// detect submitted page
	$nPgWant = $this->GetPageKey_forShowRequest();	// determine page requested to show
	
	$this->LoadControlsForPage($nPgData,FALSE);		// load controls to receive previous page form data
	$this->LoadControlsForPage($nPgWant,TRUE);		// load controls to display current page form data

	$this->LoadFields();	// copy data from cart's blob field
	$this->GetFormStats();	// this actually also receives the form data. I think.
	// It needs to be done here, before the fields get saved back to the DB.
	
	if ($this->ShouldSaveCart()) {
	    $this->SaveFields();	// save values from form fields objects to cart blob field
	    $this->SaveCart();	// save cart record back to database (thereby saving blob field)
	}
    }
    
    protected function GetIsPageDataExpected() {
	$nPgData = $this->GetPageKey_forData();
	switch ($nPgData) {
	  case KI_SEQ_NONE: $out = FALSE; break;	// direct link from confirmation page
	  case KI_SEQ_CART: $out = FALSE; break;	// [update] button on cart page
	  case KI_SEQ_CONF: $out = FALSE; break;	// [place order] button
	  default: $out = TRUE;
	}
	return $out;
    }
    protected function ShouldSaveCart() {
	if ($this->GetPageKey_forData() == KI_SEQ_CONF) {
	    return FALSE;	// no changes can be made on the CONFirm page
	} else {
	    return TRUE;
	}
    }
    
    private $arFields = array();
    /*----
      HISTORY:
	2019-06-30 Added $isShow (was $isDisp) arg so we can load controls that couldn't have been filled in yet (for display)
    */
    protected function GetFieldFor($isShow,$sName) {
	if (!array_key_exists($sName,$this->arFields)) {
	    $oField = $this->BuildFieldFor($sName);
	    if ($isShow) {
		$oField->GetControlObject()->SetRequired(FALSE);	// display controls don't have to be entered yet
	    }
	    $this->arFields[$sName] = $oField;
	}
	return $this->arFields[$sName];
    }

    private $sPgSReq = NULL;
    /*----
      HISTORY:
	2019-05-05 name & logic changed; changed from public to PROTECTED until I know why it needs to be public
    */
    protected function GetPageKey_forShowRequest() {
	if (is_null($this->sPgSReq)) {
	    $this->sPgSReq = $this->CalculatePageRequest();
	}
	return $this->sPgSReq;
    }
    
    private $sPgSAct = NULL;
    protected function GetPageKey_forShowActual() {
	if (is_null($this->sPgSAct)) {
	    $this->sPgSAct = $this->CalculatePageToShow();
	}
	return $this->sPgSAct;
    }
    /*----
      ACTION: Checks form input to see which (if any) navigation button was pressed,
	and therefore which form is being requested for display next.
	
	Does not enforce rules about whether to advance or not; just figures out what page the user is requesting.
      HISTORY:
	2019-05-05 Renaming from DetectPageRequest() to CalculatePageRequest() - restructuring the logic
	  Now returns the result instead of storing it, which is handled by the caller (standard value-cache pattern).
    */
    protected function CalculatePageRequest() {
	$gotPgDest = FALSE;
	if (fcHTTP::Request()->KeyExists(self::KSQ_ARG_PAGE_DEST)) {
	    // This happens when the user clicks on a section-edit link on the confirmation page
	    $sPgReq = fcHTTP::Request()->GetString(self::KSQ_ARG_PAGE_DEST);
	} else {
	    // page request is relative -- calculate it from data/source page plus direction:
	    if (fcHTTP::Request()->GetBool('btn-go-prev')) {
	    
		// PREVIOUS page has been requested
	    
		switch ($this->GetPageKey_forData()) {
		  case KI_SEQ_CART:
		    $sPgReq = KI_SEQ_CART;	// can't go back any further
		    break;
		  case KI_SEQ_SHIP:
		    $sPgReq = KI_SEQ_CART;
		    break;
		  case KI_SEQ_PAY:
		    $sPgReq = KI_SEQ_SHIP;
		    break;
		  case KI_SEQ_CONF:
		    $sPgReq = KI_SEQ_PAY;
		    break;
		  default:	// source page name not recognized; default to cart
		    $sPgReq = KI_SEQ_CART;
		}
	    } elseif (fcHTTP::Request()->GetBool('btn-go-next')) {
	    
		// NEXT page has been requested
	    
		switch ($this->GetPageKey_forData()) {
		  case KI_SEQ_CART:
		    $sPgReq = KI_SEQ_SHIP;
		    break;
		  case KI_SEQ_SHIP:
		    $sPgReq = KI_SEQ_PAY;
		    break;
		  case KI_SEQ_PAY:
		    $sPgReq = KI_SEQ_CONF;
		    break;
		  case KI_SEQ_CONF:
		    $sPgReq = KI_SEQ_RCPT;
		    break;
		  default:	// not sure how we got here; use default
		    $sPgReq = KI_SEQ_DEFAULT;
		}
	    } elseif (fcHTTP::Request()->GetBool('btn-go-same')) {
	    
		// SAME page requested
	    
		$sPgReq = $this->GetPageKey_forData();
	    } elseif (fcHTTP::Request()->GetBool('btn-go-order')) {
	    
		// SUBMIT ORDER page requested
	    
		$sPgReq = KI_SEQ_RCPT;		// receipt page - submits order too
	    } else {
	    
		// no request found - go to default
	    
		$sPgReq = KI_SEQ_DEFAULT;	// default page to display
	    }
	}
	return $sPgReq;
    }
    /*----
      HISTORY:
	2019-05-12 Written but not tested
	2019-08-24 seems to be working
    */
    protected function CalculatePageToShow() {
	$nPageOld = $this->GetPageKey_forData();
	$nPageReq = $this->GetPageKey_forShowRequest();
	$oRecvStat = $this->GetFormStats();
	$isAdvance = ($nPageReq > $nPageOld);
	$hasStats = !is_null($oRecvStat);
	$tryAdvance = $isAdvance && $hasStats;
	
	$ok = TRUE;
	//echo "IS ADVANCE: [$isAdvance] HAS STATS: [$hasStats] TRY ADVANCE: [$tryAdvance]<br>";
	if ($tryAdvance) {
	    // make sure there are no missing required fields
	    //echo 'ANY ARE BLANK: ['.$oRecvStat->AnyAreBlank().']<br>';
	    if ($oRecvStat->AnyAreBlank()) {
		$oMissing = $oRecvStat->GetBlanksRequired();
		//echo 'MISSING COUNT: ['.$oMissing->GetCount().']<br>';
		if ($oMissing->GetCount() > 0) {
		    // there are -- so stay where we were
		    $ok = FALSE;
		    $nPageShow = $nPageOld;
		    //echo 'BLANKS: '.$oMissing->ListString().'<br>';	// DEBUGGING; should be added to page somehow
		    $this->AddErrorMessage(
		      "Some information we need is missing: <b>"
		      .$oMissing->ListString()
		      .'</b>'
		      );
		}
	    }
	}
	if ($ok) {
	    // if $isAdvance is FALSE, then we're not trying to move forward, so incomplete form is ok
	    // if $oRecvStat is NULL, then we're not responsible for processing form data
	    //	so no need to worry if it's complete
	    // if $oMissing has no entries, then required fields were all filled in
	    $nPageShow = $nPageReq;
	}
	return $nPageShow;
    }
   
    // -- EXECUTIVE -- //
    // ++ CAPTURE ++ //
    
    /*----
      HISTORY:
	2016-03-27 Making this read-only - it gets the string from $_POST by itself,
	  and does any necessary defaulting
	  Also making protected until we know who else needs it.
	2018-07-22 Moved from Page class to Content class.
	2019-08-14 KI_SEQ_CART was the default, because it was thought to be the only page
	  that doesn't identify itself, but actually we can get direct links from the
	  order-confirmation page and those won't identify the source either.
	  In all cases, though, if the page doesn't identify itself, then it also doesn't
	  send any data. Changing to KI_SEQ_NULL (new constant).
	  (Display pages should still default to KI_SEQ_CART, however, since we need to
	  display *something*.)
    */
    protected function GetPageKey_forData() {
	//return fcHTTP::Request()->GetString(self::KSQ_ARG_PAGE_DATA,KI_SEQ_CART);
	return fcHTTP::Request()->GetString(self::KSQ_ARG_PAGE_DATA,KI_SEQ_NONE);
    }
    
    // -- CAPTURE -- //
    // ++ RENDER ++ //
    
    /*----
      ACTION: 
      OUTPUT:
	$doBackBtn: if TRUE, show the BACK navigation button
	$doRefrBtn:
	$doNavCtrl:
    */
    protected function RenderContent() {
	$out = NULL;
	$this->SetShowNavigation(TRUE);	// default
	//$doNav = TRUE;

	$nKeyShow = $this->GetPageKey_forShowActual();
	
	//echo "PAGE FOR ACTUAL DISPLAY=[$nKeyShow]<br>";
	//$oCDMgr = $this->GetCartRecord()->FieldsManager();
	switch ($nKeyShow) {
	  case KI_SEQ_CART:	// shopping cart
	    //echo 'RENDERING CART PAGE<br>';
	    $this->SetLeftNavigationIsRefresh();	// discourage going back from cart page
	    $htMain = $this->RenderCart();
	    break;
	  case KI_SEQ_SHIP:	// shipping information
	    //echo 'RENDERING SHIPMENT PAGE<br>';
	    $htMain = $this->RenderShippingPage();
	    break;
	  case KI_SEQ_PAY:	// billing information
	    //echo 'RENDERING PAYMENT PAGE<br>';
	    $htMain = $this->RenderBillingPage();
	    break;
	  case KI_SEQ_CONF:	// confirm order
	    $this->SetShowNavigation(FALSE);
	    //$doNav = FALSE;
	    $htMain = $this->RenderConfirm();
	    break;
	  case KI_SEQ_RCPT:	// order receipt
	    $this->SetShowNavigation(FALSE);
	    //$doNav = FALSE;
	    $this->ReceiveOrder();		// mark order as received
	    $htMain = $this->RenderReceipt();	// display receipt & send by email
	    break;
	  default:
// The normal shopping cart does not specify a target within the checkout sequence
// ...so show the first page which follows the cart page:
	    $htMain = $this->RenderShippingPage();
	}
	
	//$htNavStatus = $this->RenderNavigationStatus();
	
	$out =
	  $this->RenderContentHeader()
	  // this leaves us inside a cell of a single-column table
	  // So, start a new table for the actual 2-column contents
	  // TODO: why isn't the table wrapper in parent::Render()?
	  .'<table><tr><td colspan=2>'
	  .parent::Render()
	  .'</td></tr></table>'
	  
	  .$htMain
	  .$this->RenderContentFooter()
	  ;
	return $out;
    }
    
    // -- RENDER -- //
    // ++ RENDER PAGES ++ //

    /*----
      PURPOSE: This displays the cart page in the checkout sequence.
	It only shows up if the user navigates back from the shipping page.
      HISTORY:
	2018-09-17 moved from Page object to Content object
	2019-05-17 retrieved from ckout-content.php (in "NOT USED")
    */
    public function RenderCart() {
	$rcCart = $this->GetCartRecord();
	if ($rcCart->HasLines()) {
	    // this is the read-only confirmation version of the cart
	    $htCore = $rcCart->Render(FALSE);
	    return $htCore;
	} else {
	    return 'No items in cart!';
	    // TODO: log this as a critical error - how did the user get to the checkout with no items?
	}
    }
    /*----
      HISTORY:
	2019-05-30 moved here from obsolete Data Manager
	2019-06-04 moved from form.php to content.php
    */
    protected function RenderShippingPage() {
	$out =
	  (new fcSectionHeader('Buyer information:'))->Render()
	  .$this->RenderContactForm(TRUE)	// edit email/phone
	  .(new fcSectionHeader('Shipping information:'))->Render()
	  .$this->RenderShippingForm(TRUE)	// edit shipping address / instructions
	  ;
	return $out;
    }
    /*----
      HISTORY:
	2019-06-05 reconstructing for rewrite
    */
    protected function RenderBillingPage() {
	$out =
	  (new fcSectionHeader('Payment information:'))->Render()
	  .$this->RenderPaymentForm(TRUE)	// edit payment information
	  ;
	return $out;
    }
    protected function RenderPaymentForm($doEdit) {
	$oTplt = $this->BuildPaymentTemplate($doEdit);
	$arCtrls = $this->GetFormBlobject()->RenderControls($doEdit);
	$oTplt->SetVariableValues($arCtrls);
	return $oTplt->RenderRecursive();
    }
    
    //// REWRITE THIS SECTION ////

    protected function RenderPayCardNumberSection($doEdit) {
	$oForm = $this->GetFormBlobject();
	$oTplt = $this->PayCardNumberTemplate($doEdit);
	$arCtrls = $oForm->RenderControls($doEdit);
	$oTplt->SetVariableValues($arCtrls);
	return $oTplt->RenderRecursive();
    }
    protected function RenderPayCardAddrSection($doEdit) {
	$sCAType = $this->Value_forCardAddrType();
	$doOld = ($sCAType == self::KS_CARDADDRTYPE_EXISTING);
	if ($doOld) {
	    $out = $this->RenderPayCardAddr_Old($doEdit);
	} else {
	    $out = $this->RenderPayCardAddr_New($doEdit);
	}
	return $out;
    }
    protected function RenderPayCardAddr_New($doEdit) {
	$oTplt = $this->NameAddressTemplate();
	$arCtrls = $this->GetFormBlobject()->RenderControls($doEdit);
	$oTplt->SetVariableValues($arCtrls);
	return $oTplt->RenderRecursive();
    }
    //// END REWRITING ////

    /*----
      ACTION: Render the "confirm this order" page from the Cart data.
	This is the order as recorded by the CART, just before being converted to an Order.
	It should have navigation buttons and links to allow editing of the data.
      HISTORY:
	2014-10-07 Adapting from clsPageCkout::RenderConfirm()
	2015-10-08 Commented out (was deprecated because confirmations are rendered by Order records, not Cart records)
	2019-07-12 Reinstated because we're moving the cart-to-order conversion into the admin panel.
	2019-07-16 Moved from Cart object to Content object
    */
    protected function RenderConfirm_fromCart() {
	
	$rcCart = $this->GetCartRecord();
	$oArbi = $this->GetFormBlobject()->GetBlobField();
	
	//$out = $rcCart->RenderConfirm_page();
	
	$out = NULL;

	// 2019-07-16 Experimenting with doing this all here:
	
	//$isShipCard = $this->IsShipToCard();
	$sCustShipMsg = $oArbi->ShipMessage()->value;
	$custCardNum = $oArbi->CardNumber()->value;
	$custCardExp = $oArbi->CardExpiry()->value;
	$isShipToCard = $oArbi->Is_CardAddr_SameAs_Shipping();
	$sBuyerEmail = $oArbi->BuyerEmail()->value;
	$sBuyerPhone = $oArbi->BuyerPhone()->value;	// phone # is optional
	if (is_null($sBuyerPhone)) {
	    $sBuyerPhone = '<i>none</i>';
	}

	$htLink = $this->HtmlEditLink(KI_SEQ_CART);
	$out .=
	  "\n<tr class=section-title><td>ITEMS ORDERED:</td><td align=right>$htLink</td></tr>"
	  ."\n<tr><td colspan=2>\n"
	  .$rcCart->Render(FALSE)	// non-editable display of cart contents & totals
	  ."\n</td></tr>";

	$htLink = $this->HtmlEditLink(KI_SEQ_SHIP);
	$out .= "<tr class=section-title><td>SHIP TO:</td><td align=right>$htLink</td></tr>";

	//$this->doFixedCard = TRUE;
	//$this->doFixedSelf = TRUE;		// 2015-09-01 nothing uses this anymore
	//$this->doFixedName = TRUE;
	//$this->htmlBeforeAddress = '';	// 2015-09-01 nothing uses this anymore
	//$this->htmlBeforeContact = '';	// 2015-09-01 nothing uses this anymore
	//echo 'BLOB DATA DUMP:'.$oArbi->Dump();
	//echo 'CLASS: '.get_class($oArbi).'<br>';
	//$out .= $rsCD->RecipFields()->RenderAddress(
	$arFields = vcCheckoutOptions::CopyFields(
	  $oArbi->GetFields(),
	  array(
	    'name'	=> KSF_RECIP_ADDR_NAME,
	    'street'	=> KSF_RECIP_ADDR_STREET,
	    'town'	=> KSF_RECIP_ADDR_TOWN,
	    'state'	=> KSF_RECIP_ADDR_STATE,
	    'zip'	=> KSF_RECIP_ADDR_ZIP,
	    'country'	=> KSF_RECIP_ADDR_COUNTRY,
	    'email'	=> KSF_RECIP_ADDR_EMAIL,
	    'phone'	=> KSF_RECIP_ADDR_PHONE,
	  )
	);
	// add a few more values
	$arOpts = array(
	  'do.ship.zone'	=> TRUE,
	  'do.fixed.all'	=> TRUE,
	  'do.fixed.name'	=> TRUE,
	    /*
	    // field names
	    'fld.name.name'	=> KSF_RECIP_ADDR_NAME,		// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_NAME);
	    'fld.name.street'	=> KSF_RECIP_ADDR_STREET,	// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_STREET);
	    'fld.name.town'	=> KSF_RECIP_ADDR_TOWN,		// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_CITY);
	    'fld.name.state'	=> KSF_RECIP_ADDR_STATE,	// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_STATE);
	    'fld.name.zip'	=> KSF_RECIP_ADDR_ZIP,		// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_ZIP);
	    'fld.name.country'	=> KSF_RECIP_ADDR_COUNTRY,	// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_COUNTRY);
	    'fld.name.email'	=> KSF_RECIP_ADDR_EMAIL,	// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_EMAIL);
	    'fld.name.phone'	=> KSF_RECIP_ADDR_PHONE,	// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_PHONE);
	    // field values
	    'fld.value.name'	=> $oArbi->GetValue_forShipToName(),
	    'fld.value.street'	=> $oArbi->GetValue_forShipToStreet(),
	    'fld.value.town'	=> $oArbi->GetValue_forShipToTown(),
	    'fld.value.state'	=> $oArbi->GetValue_forShipToState(),
	    'fld.value.zip'	=> $oArbi->GetValue_forShipToZipCode(),
	    'fld.value.country'	=> $oArbi->GetValue_forShipToCountry(),
	    'fld.status.email'	=> $oArbi->GetStatus_ShipToEmail(),
	    'fld.status.phone'	=> $oArbi->GetStatus_ShipToPhone(),
	    */
	);
	$oOpts = new vcCheckoutOptions($arOpts,$arFields);
	
	$out .= $oOpts->RenderAddress($rcCart->ShipZoneObject());

	$htLink = $this->HtmlEditLink(KI_SEQ_PAY);

	$out .= <<<__END__
<tr><td align=right valign=top>
  Special Instructions:<br>
  </td>
  <td>$sCustShipMsg</td>
  </tr>
<tr class=section-title><td>ORDERED BY:</td><td align=right>$htLink</td></tr>
<!-- <tr><td align=right valign=middle>Email:</td><td>$sBuyerEmail</td></tr>
<tr><td align=right valign=middle>Phone:</td><td>$sBuyerPhone</td></tr> -->
<tr><td align=right valign=middle>Card Number:</td>
  <td><b>$custCardNum</b>
  - Expires: <b>$custCardExp</b>
  </td></tr>
__END__;
// if card address is different from shipping, then show it too:
// if not shipping to self, then show recipient's phone and/or email:
	if ($isShipToCard) {
	    //$this->ShowForAddress('Credit card address <b>same as shipping address</b>');
	    //$rsCD->BuyerFields()->ShowForAddress('Credit card address <b>same as shipping address</b>');
	    $htAddrText = 'Credit card address <b>same as shipping address</b>';
	} else {
	    $htAddrText = NULL;
	}

	//if ($isShipSelf) {
	//    $this->strInsteadOfCont = 'Recipient contact information <b>same as buyer\'s -- shipping to self</b>';
	//}

	// TODO 2012-05-21: this probably won't look right, and will need fixing
	//	also, are strInsteadOf* strings ^ used in confirmation?
	//$out .= $rsCD->BuyerFields()->RenderAddress(
	
	// TODO 2019-07-28 if card-addr-name is blank, fill in buyer-addr-name
	$arFields = vcCheckoutOptions::CopyFields(
	  $oArbi->GetFields(),
	  array(
	    'name'	=> KSF_CARD_ADDR_NAME,		// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_NAME);
	    'addr'	=> KSF_CARD_ADDR_BLOCK,		// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_STREET);
	    'email'	=> KSF_BUYER_ADDR_EMAIL,	// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_EMAIL);
	    'phone'	=> KSF_BUYER_ADDR_PHONE,	// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_PHONE);
	  )
	);
	//echo 'BUYER INFO ARRAY:'.\fcArray::Render($arOpts);
	$arOpts = array(
	  'do.ship.zone'	=> TRUE,
	  'do.fixed.all'	=> TRUE,
	  'do.fixed.name'	=> TRUE,
	  'ht.addr.text'	=> $htAddrText,
	);
	$oOpts = new vcCheckoutOptions($arOpts,$arFields);
	/*
	$oOpts = new vcCheckoutOptions(
	  array(
	    // field names
	    'fld.name.name'	=> KSF_BUYER_ADDR_NAME,		// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_NAME);
	    'fld.name.addr'	=> KSF_CARD_ADDR_BLOCK,	// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_STREET);
	    'fld.name.email'	=> KSF_BUYER_ADDR_EMAIL,	// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_EMAIL);
	    'fld.name.phone'	=> KSF_BUYER_ADDR_PHONE,	// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_PHONE);
	    // field values
	    'fld.value.name'	=> $this->GetValue_forBuyerName(),
	    'fld.value.addr'	=> $this->GetValue_forCardAddress(),
	    'fld.status.email'	=> $this->GetStatus_forBuyerEmail(),
	    'fld.status.phone'	=> $this->GetStatus_forBuyerPhone(),
	    )
	  );*/
	$out .= $oOpts->RenderAddress($rcCart->ShipZoneObject());

	//$out .= $this->RenderConfirm_footer();
	$this->SetNavigationForConfirm();
	$out .= "\n<!-- -".__METHOD__."() in ".__FILE__." -->\n";

	return $out;
    }
    /*----
      2019-07-12 commented this out when I thought order conversion should happen at admin time
      2019-10-20 To limit confusion, I've renamed the two versions of RenderConfirm()
	to distinguish between them --
	* RenderConfirm_fromCart() - renders the confirmation page from the Cart data; conversion to Order is after this
	* RenderConfirm_fromOrder() - converts the cart to an Order, then calls Order->RenderConfirm() to render it
	-- and created a new RenderConfirm() which dispatches to one or the other. (Since I kept flipping back and forth
	in how I wanted to do it, maybe it should ultimately be a config option.)
	
    */
    protected function RenderConfirm_fromOrder() {
    
	//$out = $this->RenderOrder(TRUE);
	$rcCart = $this->GetCartRecord();

	// This renders the confirm page using Cart data, but we normally want Order data. Keeping for reference, but commented out. 2015-09-04
	//$out = $rcCart->RenderConfirm_page();

	// Render the confirm page from Order data:
	$rcOrd = $rcCart->OrderRecord_orConvert($this);	// create the order record if needed, or get existing one
	$out =
	  $rcOrd->RenderConfirm()
	  .self::RenderConfirm_footer()
	  ;
	return $out;
    }
    /*----
      PURPOSE: This function now just determines which process we follow for rendering the confirmation page.
    */
    protected function RenderConfirm() {
	return $this->RenderConfirm_fromOrder();
	//return $this->RenderConfirm_fromCart();
    }
    /*----
      HISTORY:
	2019-08-05 removed
	2019-07-21 moved from logic/order/orders.php to page/ckout/content.php
	2019-10-21 reinstated because RenderConfirm_fromOrder() calls it -- but will comment out the contents for now
    */
    static public function RenderConfirm_footer() {
    /*
	$ksArgPageData = self::KSQ_ARG_PAGE_DATA;
	$ksArgPageShow = KI_SEQ_CONF;
	$out = <<<__END__
  <tr>
    <td colspan=2 align=center bgcolor=ffffff class=section-title>
      <input type=hidden name="$ksArgPageData" value="$ksArgPageShow">
      <input type=submit name="btn-go-prev" value="&lt; Make Changes">
      <input type=submit name="btn-go-order" value="Place the Order!">
    </td>
  </tr>
__END__;
	return $out;
    */
	return '';
    }
    /*----
      ACTION: Receive the order and render order receipt:
	* convert the cart data to an order record
	* send confirmation email
	* display order receipt page
      CALLED BY: $this->RenderContent(), depending on which page is active
      TODO: (2016-08-08) Perhaps the emailing should be a separate function (EmailReceipt()).
      HISTORY:
	2018-07-22 A note said "Probably move this to the Content object."
	2019-08-05 moved to Content object
    */
    protected function RenderReceipt() {
	$rcCart = $this->GetCartRecord();
	$rcOrd = $rcCart->OrderRecord_orDie();	// get existing order record
	
	$oArbi = $this->GetFormBlobject()->GetBlobField();

	$arVars = $rcOrd->TemplateVars($oArbi);		// fetch variables for email
	$arHdrs = $rcOrd->EmailParams($arVars);		// fill in email template with variables

// email the receipt, if email address is available
    // do this before displaying the receipt -- if the customer sees the receipt, the email is sent
	// args: $iReally, $iSendToSelf, $iSendToCust, $iAddrSelf, $iAddrCust, $iSubject, $iMessage
	$arEmail = array(
	  'to-self'	=> TRUE,
	  'to-cust'	=> TRUE,
	  'addr-self'	=> $arHdrs['addr.self'],
	  'addr-cust'	=> $arHdrs['addr.cust'],
	  'subject'	=> $arHdrs['subj'],
	  'message'	=> $arHdrs['msg.body']
	  );

	$rcOrd->EmailConfirm(TRUE,$arEmail);
	
	$out = $rcOrd->RenderReceipt();
	return $out;
    }
    
    // -- RENDER PAGES -- //
    // ++ RENDER FORM PIECES ++ //

    /*----
      ACTION: Renders email/phone controls
      ASSUMES: They have already been invoked (added to the form)
      HISTORY:
	2019-05-30 moved here from obsolete Data Manager
	2019-06-04 moved from form.php to content.php
    */
    protected function RenderContactForm($doEdit) {
	$oTplt = $this->BuildContactTemplate($doEdit);
	$arCtrls = $this->GetFormBlobject()->RenderControls($doEdit);
	$oTplt->SetVariableValues($arCtrls);
	return $oTplt->RenderRecursive();
    }
    /*----
      ACTION: Shows multiple subforms:
	1. If editing and logged in: controls to select OLD or NEW name/address
	2. entered/chosen name/address:
	  if OLD:
	    If editing, shows drop-down for name/address choice
	    If not editing, shows read-only name/address form
	  if NEW: Shows name/address form (editable or not)
	3. Shows special instructions control
	
	...UNLESS user is not logged in, in which case we force NEW mode
	  but show log-in controls.
      HISTORY:
	2019-05-30 moved here from obsolete Data Manager
    */
    protected function RenderShippingForm($doEdit) {
	$rcCart = $this->GetCartRecord();
	$oArbi = $this->GetFormBlobject()->GetBlobField();
	
	$isLoggedIn = fcApp::Me()->UserIsLoggedIn();
	if ($isLoggedIn) {
	    $doOld = !$oArbi->Value_forShipInType_isNew();
	} else {
	    $doOld = FALSE;
	}
	$out = NULL;
	
	if ($doOld) {
	    // allow user to select stored address

	    if ($doEdit) {
		$oUser = fcApp::Me()->GetUserStatus()->GetRow();
		// allow user to select from existing recipient profiles
		$rsCusts = $oUser->ContactRecords();
		
		
		$htBtn = $this->GetShippingIntypeField()->GetControlObject()->Render(TRUE);
		if ($rsCusts->RowCount() > 0) {
		    $out .= 
		      'Choose a destination: '
		      .$rsCusts->Render_DropDown_Addrs(vcGlobals::Me()->GetName_DropDown_ShipToAddresses())
		      .' or '
    //		  .'<input type=submit name=btnEnterRecip value="Enter New Destination">'
		      .$htBtn
		      ;
		} else {
		    $out .= 'You currently have no shipping destinations saved in your profile. '
		      .$htBtn
		      ;
		}
	    } else {
		$oTplt = $this->BuildShipAddrTemplate();
	    
		$out = $oTplt->RenderRecursive();
	    }
	} else {
	    // allow user to enter a new address

	    $out = "\n<div align=center>";
	    if (fcApp::Me()->UserIsLoggedIn()) {
		$htBtn = $this->GetShippingIntypeField()->GetControlObject()->Render(TRUE);
		$out .= "Enter new shipping destination below or $htBtn from your profile.";
	    } else {
		// Without the complication of any other options, no real explanation is needed here.
	    }

	    $oTplt = $this->BuildShipAddrTemplate($doEdit);
	    
	    $out .=
	      '</div>'
	      .$oTplt->RenderRecursive();
	      ;
	}
	
	//$out .= $this->RenderShipping_OrderInstructions($doEdit);	// special instructions for this order only
	return $out;
    }

    // -- RENDER FORM PIECES -- //
    // ++ RENDER PAGE PIECES ++ //
    
    /*----
      PUBLIC so Cart can access it for displaying confirmation page
      RENDERS links to go back to earlier pages, to edit order before submitting
      USED BY: order confirmation page (displayed by Cart object)
    */
    public function HtmlEditLink($sPage,$sText='edit',$sPfx='[',$sSfx=']') {
	$out = $sPfx.'<a href="?'.self::KSQ_ARG_PAGE_DEST.'='.$sPage.'">'.$sText.'</a>'.$sSfx;
	return $out;
    }

    /*----
      PURPOSE: Render top part of {form and outer table, including <td>}
      TODO: move this stuff into the skin, somehow
      CALLED BY: $this->HandleInput()
    */
    protected function RenderContentHeader() {
	$sWhere = __METHOD__."() in ".__FILE__;

	if ($this->GetShowNavigation()) {
	    $htNav = $this->RenderNavigationAbove();
	} else {
	    $htNav = '';
	}
//	$urlTarg = KURL_CKOUT;

	// We need to remove any ?query because that's only used for navigation (direct-go-to links on the order-review page)
	$oURL = fcUrl::GetCurrentObject();
	  // This technique is a *bit* of a kluge, as it isn't semantic; there should be an object for the current URL somewhere.
	$url = $oURL->GetValue_Path();

	$out = <<<__END__

<!-- vv $sWhere vv -->
<form method=post action=$url name=checkout>
<table class="form-block" id="page-ckout">
$htNav
<tr>
<td>
<!-- ^^ $sWhere ^^ -->
__END__;
	return $out;
    }
    /*----
      2018-02-27 Commented out stuff that was conditional on whether user is logged in,
        because login controls now handle that internally
      2019-06-04 moved from form.php to content.php
    */
    protected function RenderUserControls() {
	return fcApp::Me()->GetPageObject()->RenderLogin_Controls();
    }
    /*----
      PURPOSE: Stuff to close out the page contents
      ACTION: Close table row opened in RenderContentHdr(), display standard buttons, close outer table and form
    */
    protected function RenderContentFooter() {
	$sWhere = __METHOD__."() in ".__FILE__;

	$rcCart = $this->GetCartRecord();
	$idSess = $this->GetSessionStatus()->GetKeyValue();
	$idCart = $rcCart->GetKeyValue();
	$idOrd = $rcCart->GetOrderID();
	$sOrd = ($idOrd == 0)?'':" Order ID: <b>$idOrd</b>";
	if ($this->doNavCtrl) {
	    $htLine = NULL;
	} else {
	    $htLine = '<hr>';	// TODO: Make this a Skin function (again)
	}
	$htNav = $this->RenderNavigationBelow();

	$out = <<<__END__

<!-- vv $sWhere vv -->
</td></tr>
<tr><td align=center>
$htNav
</td></tr>
</table>
$htLine
<span class="footer-stats">Cart ID: <b>$idCart</b> Session ID: <b>$idSess</b>$sOrd</span>
</form>

<!-- ^^ $sWhere ^^ -->

__END__;

	return $out;
    }

    // -- RENDER PAGE PIECES -- //
    // ++ RENDER NAVIGATION ++ //

    /* 2019-06-07 This is now obsolete as we're combining the status and buttons
    protected function RenderNavigationButtons() {
	$htLBtn = $this->RenderNavigationLeftButton();
	$htRBtn = $this->RenderNavigationRightButton();
	$out =
	  $this->RenderFormAlert()	// if any
	  .'<tr><td colspan='.KI_CKOUT_COLUMN_COUNT.' align=center bgcolor=ffffff class=section-title>'
	  // 2019-05-19 I think this tells the receiving page which page's data are being received:
	  .'<input type=hidden name="'.self::KSQ_ARG_PAGE_DATA.'" value="'.$this->GetPageKey_forShowActual().'">'
	  .$htLBtn.$htRBtn
	  ;
	return $out;
    } */
    protected function RenderNavigationLeftButton() {
	$htName = $this->GetLeftNavigationName();
	$htText = $this->GetLeftNavigationText();
	return "<input type=submit name='$htName' value='$htText'>";
    }
    protected function RenderNavigationRightButton() {
	$htName = $this->GetRightNavigationName();
	$htText = $this->GetRightNavigationText();
	return "<input type=submit name='$htName' value='$htText'>";
    }
    protected function RenderNavigationStatus() {
	$oNav = $this->CreateNavigationStatusObject();
	
	// TODO: document what these 3 lines mean!
	$sPage = $this->GetPageKey_forShowActual();
	$oNav->States($sPage,1,3);
	$oNav->GetNode($sPage)->State(2);
	
	//return $this->GetSkinObject()->RenderNavbar_H($oNav);	// pretty sure that won't work...
	return $oNav->Render();
    }
    protected function CreateNavigationStatusObject() {
	$oNav = new fcNavbar_flat();
	  $oi = new fcNavText($oNav,KI_SEQ_CART,'Cart');
	  $oi = new fcNavText($oNav,KI_SEQ_SHIP,'Shipping');
	  $oi = new fcNavText($oNav,KI_SEQ_PAY,'Payment');
	  $oi = new fcNavText($oNav,KI_SEQ_CONF,'Final Check');
	  $oi = new fcNavText($oNav,KI_SEQ_RCPT,'Receipt');
	$oNav->Decorate('','',' &rarr; ');
	$oNav->CSSClass('nav-item-past',1);
	$oNav->CSSClass('nav-item-active',2);
	$oNav->CSSClass('nav-item-todo',3);
	return $oNav;
    }
    protected function RenderNavigationBar() {
	$nColums = KI_CKOUT_COLUMN_COUNT;
	$htHidName = self::KSQ_ARG_PAGE_DATA;
	$htHidVal = $this->GetPageKey_forShowActual();
	return
	  //$this->RenderFormAlert()	// if any
	  "\n<tr><td colspan=$nColums align=center bgcolor=ffffff class=section-title>"
	  ."\n<input type=hidden name='$htHidName' value='$htHidVal'>"
	  ."\n<table><tr><td class=nav-bar>"
	  .$this->RenderNavigationLeftButton().'&nbsp;'
	  .$this->RenderNavigationStatus().'&nbsp;'
	  .$this->RenderNavigationRightButton()
	  ."</td></tr></table>"
	  ."\n</td></tr>";
	  ;
    }
    /*----
      PURPOSE: This is a separate method from RenderNavigationBar() in case we decide
	the above and below bars should be different (as they were for awhile).
    */
    protected function RenderNavigationAbove() {
	return $this->RenderNavigationBar();
    }
    protected function RenderNavigationBelow() {
	return $this->RenderNavigationBar();
    }

    // -- RENDER NAVIGATION -- //
}    
