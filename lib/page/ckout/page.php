<?php
/*
  FILE: ckout.php
  HISTORY:
    2010-02-21 split off from shop.php (via checkout/index.php)
    2010-12-24 Fixed call to Update() -- array is now required
    2013-02-20 mostly working, but "ship to card" and "ship to self" logic is becoming unmanageable.
      Going to gut this and significantly rework it as a single form.
    2013-04-12 ended up with two forms still, but somewhat simplified logic
    2015-08-28 vbz-const-user.php no longer needed; constants are defined in ferreteria/page.php
    2019-06-13 renamed to page.php and moved app/kiosk classes into app/ckout.php
*/

//require_once('config-admin.php');
require_once('const.php');

define('KI_CKOUT_COLUMN_COUNT',4);

// these, I think, define the order in which checkout pages are presented
define('KI_SEQ_NONE',0);	// for data only: means page did not send any data == did not identify itself
define('KI_SEQ_CART',1);
define('KI_SEQ_SHIP',2);
define('KI_SEQ_PAY',3);
define('KI_SEQ_CONF',4);
define('KI_SEQ_RCPT',5);
// if no page specified, go to the shipping info page (first page after cart):
define('KI_SEQ_DEFAULT',KI_SEQ_SHIP);


/*::::
  CLASS: vcPageCkOut
  PURPOSE: subclass for generating checkout pages
*/
class vcPageCkout extends vcPage {
    
    // ++ EVENTS ++ //

    protected function OnRunCalculations() {
	$this->UseStyleSheet('ckout');
	//$this->SetContentTitleContext('you have reached the...');	// not supported by checkout page header class
	//$oPage->SetPageTitle('Home Page');
	$sTitle = vcGlobals::Me()->GetText_SiteName_short().' checkout';
	$this->SetBrowserTitle($sTitle);
	$this->SetContentTitle($sTitle);
	
	$this->bCardMatchesShip = NULL;
    }

    // -- EVENTS -- //
    // ++ CLASSES ++ //

    protected function Class_forTagHTML() : string {
	return 'vcTag_html_ckout';
    }

    // -- CLASSES -- //
    // ++ WEB UI: PAGES ++ //

    /* 2019-07-02 does not seem to be called anymore
    public function RenderCart() {
    throw new exception('2018-09-17 This has been moved to the Content object.');
	$rcCart = $this->CartRecord();
	if ($rcCart->HasLines()) {
	    // this is the read-only confirmation version of the cart
	    $htCore = $rcCart->Render(FALSE);
	    return $htCore;
	} else {
	    return 'No items in cart!';
	    // TO DO: log this as a critical error - how did the user get to the checkout with no items?
	}
    } */

    // -- WEB UI: PAGES -- //
    // ++ WEB UI: PAGE ELEMENTS ++ //

    // PUBLIC so Cart forms can use it
    public function RenderLogin_Controls() {
	$rcSess = fcApp::Me()->GetSessionStatus();
	if ($rcSess->UserIsLoggedIn()) {
	    $sUser = $rcSess->UserString();
	    $out = "You are logged in as <b>$sUser</b>.";
	    // TODO: probably should be a logout link here too
	} else {
	    //$oLogin = $this->GetElement_LoginWidget();
	    $oLogin = $this->GetElement_LoginWidget();
	    $oLogin->SetVisible(TRUE);
	    $oLogin->SetInput_ShowLoginForm();
	    $htLoginCore = $oLogin->Render();
	    $oLogin->SetVisible(FALSE);
	    $urlLogin = vcGlobals::Me()->GetWebPath_forLoginPage();
	    
	    $out = <<<__END__
  <b>Shopped here before?</b>
    $htLoginCore
    or <a href="$urlLogin" title="login options: reset password, create account">more options</a>.
__END__;
	}

	return $out;
    }

    // -- WEB UI: PAGE ELEMENTS -- //
    // ++ FORM / DATA ACCESS ++ //

    public function GetFormItem($iName) {
    throw new exception('2018-07-22 How do we get here?');
	if (isset($_POST[$iName])) {
	    return $_POST[$iName];
	} else {
	    // TODO: This is probably the wrong way to log things now.
	    $this->CartRecord()->LogEvent('!FLD','Missing form field: '.$iName);
	    return NULL;
	}
    }

    // -- FORM / DATA ACCESS -- //
    // ++ ACTIONS ++ //

    /*----
      ACTION: Create and populate an order from this cart
	...but only do the bits that are specific to the
	shopping interface; everything else should go in
	Cart::ToOrder() so that the admin UI can use it too.
      CALLED BY: $this->ReceiveOrder()
      HISTORY:
	2011-03-27 fixed bug which was preventing order number from being written to cart.
	  Looking at the cart data, this bug apparently happened on 2010-10-28.
	2013-11-06 Most of the work was being pushed out to clsOrders::CopyCart(),
	  but this seems unnecessary so I'm pulling it back in here.
      TODO: 2018-07-22 Probably move this to another class.
    */
    protected function MakeOrder() {
    throw new exception('2018-07-22 How do we get here?');
	$rcCart = $this->CartRecord();	// throw an exception if no cart found

	$tOrders = $this->OrderTable();
	$idOrd = $rcCart->OrderID();
	$doNewOrd = TRUE;
	if ($idOrd > 0) {
	    // Cart is already connected to an Order - check to see if the Order has been submitted yet:

	    $rcOrd = $tOrders->GetItem($idOrd);
	    if ($rcOrd->IsPlaced()) {
		$arEv = array(
		  'code'	=> 'ORN',
		  'descr'	=> 'Cart is assigned to submitted Order; creating new Order',
		  'params'	=> '\cart='.$rcCart->GetKeyValue().'\ord='.$idOrd,
		  'where'	=> __METHOD__,
		  );
		$rcEv1 = $rcCart->StartEvent($arEv);
	    } else {
		// order has not been placed yet -- so let's assume customer wants to modify it
		$doNewOrd = FALSE;
		$arEv = array(
		  'code'	=> 'ORU',
		  'descr'	=> 'ID_Order already set in cart: updating existing order',
		  'params'	=> '\cart='.$rcCart->GetKeyValue().'\ord='.$idOrd,
		  'where'	=> __METHOD__,
		  );
		$rcEv1 = $rcCart->StartEvent($arEv);
		//$rcCart->Update(array('WhenUpdated'=>'NOW()'));	// 2015-09-04 why?
	    }
	}
	if ($doNewOrd) {
	    $idOrd = $tOrders->Create();

	    if (empty($idOrd)) {
		throw new exception('Internal Error: Order creation did not return order ID.');
	    }

	    $rcOrd = $tOrders->GetItem($idOrd);

	    $arEv = array(
	      'code'	=> 'ORD',
	      'descr'	=> 'assigning order to cart',
	      'params'	=> '\ord='.$idOrd,
	      'where'	=> __METHOD__
	      );
	    $rcEv2 = $rcCart->StartEvent($arEv);
	    $arUpd = array(
	      'WhenOrdered'	=> 'NOW()',
	      'ID_Order'	=> $idOrd
	      );
	    $rcCart->Update($arUpd);
	    $rcEv2->Finish();

	    $rcCart->OrderID($idOrd);	// save Order ID to local Cart in case it's important
	    $rcOrd = $tOrders->GetItem($idOrd);
	}
	$rcCart->ToOrder($rcOrd);	// copy the actual data to the order record

	// set Order ID in session object
	$arUpd = array(
	  'ID_Order'	=> $idOrd,
	  );
	$rcSess = $rcCart->SessionRecord();
	$rcSess->Update($arUpd);

	// log the event
	$sqlSQL = $tOrders->Engine()->SanitizeAndQuote($rcSess->sqlExec);
	$rcCart->LogEvent(
	  __METHOD__,
	  '|ord ID='.$idOrd.'|cart ID='.$rcCart->GetKeyValue(),
	  'Converted cart to order; SQL='.$sqlSQL,
	  'C>O',FALSE,FALSE);

	// log completion of the outer Cart event
	$rcEv1->Finish();

	return $rcOrd;	// this is used by the checkout process
    }

    // -- ACTIONS -- //

}
class vcTag_html_ckout extends vcTag_html {

    // ++ SETUP ++ //

    // CEMENT
    protected function Class_forTag_body() {
	return 'vcTag_body_ckout';
    }

    // -- SETUP -- //
}
class vcNavbar_ckout extends fcMenuFolder {

    // ++ EVENTS ++ //
    
    protected function RenderNodesBlock() {
    throw new exception('2018-02-28 Is this even getting called?');
	return $this->RenderStatusBar();
    }

    // -- EVENTS -- //
}
class vcTag_body_ckout extends vcTag_body {

    // ++ EVENTS ++ //
    
    // OVERRIDE
    protected function OnCreateElements() {
    	$this->GetElement_PageNavigation();
	$this->GetElement_PageHeader();
	$this->GetElement_PageContent();
	$o = $this->GetElement_LoginWidget();
	$o->SetVisible(FALSE);	// we're going to manually insert the rendering of this
    }
    protected function OnRunCalculations(){}
    
    // -- EVENTS -- //
    // ++ CLASSES ++ //
    
    // CEMENT
    protected function Class_forPageHeader() {
	return 'fcContentHeader_login';	// no special header needed (possibly)
    }
    // CEMENT
    protected function Class_forPageNavigation() {
	return 'vcNavbar_ckout';
    }
    // CEMENT
    protected function Class_forPageContent() {
	return 'vcPageContent_ckout';
    }
    // OVERRIDE
    protected function Class_forLoginWidget() {
	return 'fcpeLoginWidget_inline';
    }
    
    // -- CLASSES -- //

}

/*----
  PURPOSE: adds some app-specific data to the blob-form class
  NOTE: ShipZone needs to go here rather than in the formgroup objects because
    wierd things happen with it not getting set in some cases. Insufficient
    time to figure out what was going on.
  HISTORY:
    2019-03-01 Created to hopefully resolve ShipZone sometimes being NULL in formgroups.
    2019-03-05 Changed from being a blob-descendant to a form-descendant
    2019-07-25 Changed from using fcBlobField to using (new) fcArbiRecord_asRows
*/
class vcfCartBlob extends fcForm_blob {

    public function __construct($idCart) {
        //$oBlob = new fcBlobField();
        $oBlob = new \vbzcart\checkout\cArbiRecord($idCart);
        parent::__construct('cart-'.$idCart,$oBlob);
    }
    
}
