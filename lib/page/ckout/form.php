<?php
/*
  USAGE: for vcPageContent_ckout
    These functions are only being defined as a trait so they can be in a separate file, for easier maintainability.
  HISTORY:
    2019-05-25 Started almost as an experiment; completely gutting the checkout process yet again
*/
use \vbzcart\checkout\cOptions as vcCheckoutOptions;

// 2019-07-18 This is a bit of a mess. It *is* used, but...
class vcContactFields extends vcFormFields {
    const KS_EMAIL = 'email';
    const KS_PHONE = 'phone';
}
class vcAddressFields extends vcFormFields {

    // ++ SETUP ++ //

    public function __construct(
      vcShipCountry $oZone,
      array $arFields
      ) {
	$this->SetCountryZone($oZone);
	$this->SetFields($arFields);
    }
    
    // array element names
      // - these don't actually matter
    const KS_NAME = 'name';
    const KS_STREET = 'street';
    const KS_TOWN = 'town';
      // - these do need to be referenced specifically
    const KS_STATE = 'state';
    const KS_ZIP = 'zip';
    const KS_COUNTRY = 'country';

    protected function GetField_State() {
	return $this->GetField(self::KS_STATE);
    }
    protected function GetField_Zip() {
	return $this->GetField(self::KS_ZIP);
    }
    protected function GetField_Country() {
	return $this->GetField(self::KS_COUNTRY);
    }
    
    // -- SETUP -- //
    // ++ TEMPLATE ++ //
    
    public function BuildTemplate() {
	$oZone = $this->GetCountryZone();

	$ofState = $this->GetField_State();
	$ofZip =  $this->GetField_Zip();
	
	$ofState->SetAfter(
	  is_null($oZone->HasState())
	  ?'(if needed)'
	  :NULL
	  );
	$ofState->SetText($oZone->StateLabel());
	$ofZip->SetText($oZone->PostalCodeLabel());

	return parent::BuildTemplate();

    }
}
trait vtCheckoutForm {

    // ++ FIELDS ++ //

    /*----
      ACTION: constructs the requested field
      USAGE:
	* Should only be called by GetFieldFor(), which caches the created objects. This is important
	  in order to preserve each control's state during subsequent uses after it is created. (If new
	  objects are created instead, state changes will be lost and things won't work right.)
	* If an error message says "The field object for [<field name>] has not been configured yet.",
	  then it needs to be defined here.
    */
    protected function BuildFieldFor($sName) {
	$oForm = $this->GetFormBlobject();
	switch ($sName) {
	
	  // BUYER CONTACT fields
	  
	  case KSF_BUYER_ADDR_NAME:
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>50));
		$oCtrl->SetDisplayAlias('name');
	    break;
	  case KSF_BUYER_ADDR_PHONE;
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>20));
		$oCtrl->SetDisplayAlias('phone number');
		$oCtrl->SetRequired(FALSE);
	    break;
	  case KSF_BUYER_ADDR_EMAIL;
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oField->SetDefault($this->GetDefaultEmail());
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>30));
		$oCtrl->SetDisplayAlias('email address');
	    break;
	
	  // BUYER PAYMENT fields
	
	  case KSF_INTYPE_CARD_ADDR:	// card address instamode button-switch
	    $oField = new fcFormField_Text($oForm,$sName);
	      $arBtns = array(
		  self::KS_CARDADDRTYPE_EXISTING => new fcInstaModeButton($sName,"Set billing address same as shipping"),
		  self::KS_CARDADDRTYPE_NEWENTRY => new fcInstaModeButton($sName,"Enter separate billing address"),
		);
	      $oCtrl = new fcFormControl_HTML_InstaMode($oField,$arBtns);
	    break;
	    
	  case KSF_CARD_ADDR_NAME:
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>50));
		$oCtrl->SetDisplayAlias('cardholder name');
	    break;

	  case KSF_CARD_ADDR_BLOCK:
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML_TextArea($oField,array('rows'=>3,'cols'=>50));
		$oCtrl->SetDisplayAlias('street');
	    break;
/*
	  case KSF_CARD_ADDR_TOWN:
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>20));
		$oCtrl->DisplayAlias('town');
	    break;
	  case KSF_CARD_ADDR_STATE:
	    $lenState = $this->GetShipCountry()->StateLength();
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>$lenState));
		$oCtrl->DisplayAlias(strtolower($this->GetShipCountry()->StateLabel()));
	    break;
	  case KSF_CARD_ADDR_ZIP:
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>11));
		$oCtrl->DisplayAlias(strtolower($this->GetShipCountry()->PostalCodeLabel()));
	    break;
	  case KSF_CARD_ADDR_COUNTRY:
	    $oZone = $this->GetShipCountry();
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oField->SetValue($oZone->CountryName());
	      $oField->GetControlObject()->SetRequired(!$oZone->IsDomestic());
		
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>20));
		$oCtrl->SetDisplayAlias('country');
	    break;
		*/
	  
	  case KSF_CARD_NUM:
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>20));
		$oCtrl->SetDisplayAlias('card number');
		$oCtrl->SetRequired(TRUE);
	    break;
	  case KSF_CARD_EXP:
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>5));
		$oCtrl->SetDisplayAlias('card expiration');
		$oCtrl->SetRequired(TRUE);
	    break;
	    
	  // RECIP fields:
	  
	  case KSF_INTYPE_SHIP:
	    $oField = new fcFormField_Text($oForm,$sName);	// TODO: WHY IS THIS A TEXT FIELD??
	      $arBtns = array(
		  self::KS_INTYPE_EXISTING => new fcInstaModeButton($sName,'Select a Destination'),
		  self::KS_INTYPE_NEWENTRY => new fcInstaModeButton($sName,'Enter a New Destination'),
		);
	      // KS_INTYPE_EXISTING: when logged in, by default let the user choose from a list
	      $oCtrl = new fcFormControl_HTML_InstaMode($oField,$arBtns,self::KS_INTYPE_EXISTING);
	    break;
	  case KSF_RECIP_ADDR_NAME:
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>50));
		$oCtrl->SetDisplayAlias('name');
	    break;
	  case KSF_RECIP_ADDR_PHONE;
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>20));
		$oCtrl->SetDisplayAlias('phone number');
		$oCtrl->SetRequired(FALSE);
	    break;
	  case KSF_RECIP_ADDR_EMAIL;
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oField->SetDefault($this->GetDefaultEmail());
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>30));
		$oCtrl->SetDisplayAlias('email address');
	    break;
	  case KSF_RECIP_ADDR_STREET:
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML_TextArea($oField,array('rows'=>3,'cols'=>50));
		$oCtrl->SetDisplayAlias('street');
	    break;
	  case KSF_RECIP_ADDR_TOWN:
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>20));
		$oCtrl->SetDisplayAlias('town');
	    break;
	  case KSF_RECIP_ADDR_STATE:
	    $oCountry = $this->GetShipCountry();
	    $lenState = $oCountry->StateLength();
	    
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>$lenState));
		$oCtrl->SetDisplayAlias(strtolower($oCountry->StateLabel()));
	    break;
	  case KSF_RECIP_ADDR_ZIP:
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>11));
		$oCtrl->SetDisplayAlias(strtolower($this->GetShipCountry()->PostalCodeLabel()));
	    break;
	  case KSF_RECIP_ADDR_COUNTRY:
	    $oCountry = $this->GetShipCountry();
	    $isRequired = !$oCountry->IsDomestic();
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oField->SetValue($oCountry->CountryName());
		
	      $oCtrl = new fcFormControl_HTML($oField,array('size'=>20));
		$oCtrl->SetDisplayAlias('country');
		$oCtrl->SetRequired($isRequired);
	    break;
	  case KSF_RECIP_ADDR_INSTRUX:
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML_TextArea($oField,array('rows'=>2,'cols'=>60));
		$oCtrl->SetRequired(FALSE);
	    break;
	  case KSF_RECIP_ORDER_MSG:
	    $oField = new fcFormField_Text($oForm,$sName);
	      $oCtrl = new fcFormControl_HTML_TextArea($oField,array('rows'=>3,'cols'=>50));
		$oCtrl->SetRequired(FALSE);
	    break;
	  default:
	    throw new exception("VbzCart internal error: The field object for [$sName] has not been configured yet.");
	}
	return $oField;
    }
    /*----
      NOTE 2019-08-07 Tentatively, I think the way the GetFieldFor() call works here is that
	InType fields are *never* required, so we'll just always say "FALSE" for $isShow.
    */
    protected function GetShippingIntypeField() {
	return $this->GetFieldFor(FALSE,KSF_INTYPE_SHIP);
    }
    /*----
      USAGE:
	If error says "Template requires variable [<variable name>], but it was not defined in the array.", then
	  that field needs to be added to the appropriate page definition here.
      NOTE:
	2019-08-07 For now, it's ok that this sometimes gets called twice in a row with the only difference being
	  that isShow changes from FALSE to TRUE. Maybe later we can optimize this, but it doesn't seem to be
	  a source of bugs or significant CPU usage.
      HISTORY:
	2019-05-09 now also loads controls and saves data -- should probably be renamed "HandlePageData" OSLT.
	2019-05-28 moved from data manager class to page content class (major gutting / rewiring)
	2019-06-28 address fields need to be loaded even if we're using existing data, because those fields
	  still need to be displayed
    */
    protected function LoadControlsForPage($nPage,$isShow) {

	switch($nPage) {
	  case KI_SEQ_CART:
	    $sType = 'cart';
	    // no controls needed because form is handled elsewhere
	    // also, the checkout version of the cart isn't edible, so there's no form
	    break;
	  case KI_SEQ_SHIP:
	  //echo "LOADING SHIPPING FIELDS - show:[$isShow]<br>";
	    $sType = 'ship';
	    //$this->GetFieldFor($isShow,KSF_BUYER_ADDR_NAME);	// not currently displaying this
	    $this->GetFieldFor($isShow,KSF_BUYER_ADDR_EMAIL);
	    $this->GetFieldFor($isShow,KSF_BUYER_ADDR_PHONE);
	    $f = $this->GetFieldFor($isShow,KSF_INTYPE_SHIP);
	    //echo 'FIELD CLASS: '.get_class($f).'<br>';
	    
	    // shipping info
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_NAME);
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_PHONE);
	    //$this->GetFieldFor($isShow,KSF_RECIP_ADDR_EMAIL);
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_STREET);
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_TOWN);
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_STATE);
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_ZIP);
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_COUNTRY);

	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_INSTRUX);
	    break;
	 case KI_SEQ_PAY:
	    $sType = 'pay';
	    $this->GetFieldFor($isShow,KSF_CARD_NUM);
	    $this->GetFieldFor($isShow,KSF_CARD_EXP);
	    $this->GetFieldFor($isShow,KSF_CARD_ADDR_NAME);
	    $this->GetFieldFor($isShow,KSF_CARD_ADDR_BLOCK);
	    /*
	    $this->GetFieldFor(KSF_CARD_ADDR_TOWN);
	    $this->GetFieldFor(KSF_CARD_ADDR_STATE);
	    $this->GetFieldFor(KSF_CARD_ADDR_ZIP);
	    */
	    break;
	  case KI_SEQ_CONF:
	  case KI_SEQ_RCPT:
	    $sType = 'conf/rcpt';
	    $this->GetFieldFor($isShow,KSF_BUYER_ADDR_NAME);
	    $this->GetFieldFor($isShow,KSF_BUYER_ADDR_EMAIL);
	    $this->GetFieldFor($isShow,KSF_BUYER_ADDR_PHONE);
	    
	    // shipping info
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_NAME);
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_PHONE);
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_EMAIL);
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_STREET);
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_TOWN);
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_STATE);
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_ZIP);
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_COUNTRY);
	    $this->GetFieldFor($isShow,KSF_RECIP_ADDR_INSTRUX);

	    $this->GetFieldFor($isShow,KSF_CARD_NUM);
	    $this->GetFieldFor($isShow,KSF_CARD_EXP);
	    $this->GetFieldFor($isShow,KSF_CARD_ADDR_NAME);
	    $this->GetFieldFor($isShow,KSF_CARD_ADDR_BLOCK);
	    break;
	  default:
	    // 2019-08-14 we used to consider this an error, but right now the
	    //	order confirmation page doesn't identify itself.
	    //  (This could be changed later if we want to be more careful.)
	    //echo 'NO PAGE SELECTED<br>';
	    // TODO: log an error -- possible cracking attempt
	}
	//echo "LOADED [$sType] CONTROLS for ".($isShow ? 'display' : 'receiving').'<br>';
	//echo 'LEAVING '.__METHOD__.'<br>';
    }

    // -- FIELDS -- //
    // ++ VALUE CALCULATIONS ++ //

    // TODO: this probably belongs in a different section
    protected function GetDefaultEmail() {
	$oApp = fcApp::Me();
	if ($oApp->UserIsLoggedIn()) {
	    return $oApp->GetUserStatus()->GetRow()->EmailAddress();
	} else {
	    return NULL;
	}
    }

    // -- VALUE CALCULATIONS -- //
    // ++ RENDERING ++ //

    
    /* 2019-06-04 I think this is obsolete now.
    protected function RenderNameAddress($doEdit) {
	$oTplt = $this->GetNameAddressTemplate();
	$arCtrls = $this->GetFormObject()->RenderControls($doEdit);
	$oTplt->SetVariableValues($arCtrls);
	return $oTplt->RenderRecursive();
    } */
    /*----
      ACTION: Old Mode output depends on whether we're editing:
	* if editing: show the drop-down and [Enter New Destination] button
	* if read-only: show the selected name/address
      HISTORY:
	2019-05-30 moved here from obsolete Data Manager
    */
    /* 2019-06-03 obsolete I think
    protected function RenderShipping_OldMode($doEdit) {
	if ($doEdit) {
	    $oUser = $this->UserRecord();
	    // allow user to select from existing recipient profiles
	    $rsCusts = $oUser->ContactRecords();
	    $htBtn = $this->GetShippingIntypeField()->ControlObject()->Render(TRUE);
	    if ($rsCusts->RowCount() > 0) {
		$out = 
		  'Choose a destination: '
		  .$rsCusts->Render_DropDown_Addrs(vcGlobals::Me()->GetName_DropDown_ShipToAddresses())
		  .' or '
//		  .'<input type=submit name=btnEnterRecip value="Enter New Destination">'
		  .$htBtn
		  ;
	    } else {
		$htCusts = 'You currently have no shipping destinations saved in your profile. '
		  .$htBtn
		  ;
	    }
	} else {
	    // the pre-existing record values should have been copied to local data when the form was saved
	    $oNAForm = $this->BuildShipAddrTemplate();
	    $oTplt = $oNAForm->BuildTemplate();
	    $arCtrls = $this->GetFormObject()->RenderControls($doEdit);
	    $oTplt->SetVariableValues($arCtrls);
	
	    $out = $oTplt->RenderRecursive();
	}
	return $out;
    }
    */
    /*----
      HISTORY:
	2019-05-30 moved here from obsolete Data Manager
    */
    /* 2019-06-03 obsolete I think
    protected function RenderShipping_NewMode($doEdit) {
	$out = "\n<div align=center>";
	if (fcApp::Me()->UserIsLoggedIn()) {
	    $htBtn = $this->GetShippingIntypeField()->GetControlObject()->Render(TRUE);
	    $out .= "Enter new shipping destination below or $htBtn from your profile.";
	} else {
	    // Without the complication of any other options, no real explanation is needed here.
	}

	$oNAForm = $this->BuildShipAddrTemplate();	// returns vcAddressFields
	$oTplt = $oNAForm->BuildTemplate();
	$arCtrls = $this->GetFormObject()->RenderControls($doEdit);
	$oTplt->SetVariableValues($arCtrls);
	
	$out .=
	  '</div>'
	  //.$this->RenderNameAddress($doEdit)
	  .$oTplt->RenderRecursive();
	  ;

	return $out;
    } */

    // -- RENDERING -- //
    // ++ TEMPLATES ++ //
    
    /* 2019-06-04 I think this is obsolete now.
    protected function GetContactTemplate($doEdit) {
	// NOTE: This might need to be used for recip as well
	$htEmail = KSF_BUYER_ADDR_EMAIL;
	$htPhone = KSF_BUYER_ADDR_PHONE;
	$htPhoneSfx = $doEdit?' (optional)':'';	// only show "optional" when editing
	$htUser = $this->RenderUserControls();

	$sTplt = <<<__END__
<table class="form-block" id="contact">
<tr><td colspan=2 align=center><hr>$htUser<hr></td></tr>
<tr><td align=right valign=middle>Email:</td><td>[[$htEmail]]</td></tr>
<tr><td align=right valign=middle>Phone:</td><td>[[$htPhone]]$htPhoneSfx</td></tr>
</table>
__END__;
	return new fcTemplate_array('[[',']]',$sTplt);
    } */
    protected function BuildContactFieldArray() : array {
	$arFields = array(
	  vcContactFields::KS_EMAIL	=> new vcFormFieldTemplate_inline(
	    KSF_BUYER_ADDR_EMAIL,
	    'Email'
	    ),
	  vcContactFields::KS_PHONE => new vcFormFieldTemplate_inline(
	    KSF_BUYER_ADDR_PHONE,
	    'Phone',
	    ' (optional)'
	    ),
	  'login'			=> new vcFormFieldTemplate_raw(
	    $this->RenderUserControls()	// login status/control
	    ),
	);
	return $arFields;
    }
    /*----
      HISTORY:
	2019-06-05 I had put in code to include the shipping intype button if the user is logged in,
	  but it turns out this duplicates functionality already implemented elsewhere.
	  Search for "Enter new shipping destination" to find that.
	  The key (if logged in) was to check $this->GetShippingIntypeField()->GetValue() vs. self::KS_INTYPE_EXISTING/NEWENTRY,
	    and define an appropriate label ($sIntype) for each.
	    ...then $arFields['intype'] = new vcFormFieldTemplate_inline(KSF_INTYPE_SHIP,$sInType.', or');
	    to add the control.
    */
    protected function BuildShipAddrFieldArray($isForeign) : array {

	$arFields[vcAddressFields::KS_NAME]	= new vcFormFieldTemplate_inline(
	    KSF_RECIP_ADDR_NAME,
	    'Name'
	    );
	$arFields[vcAddressFields::KS_STREET]	= new vcFormFieldTemplate_inline(
	    KSF_RECIP_ADDR_STREET,
	    'Street Address<br>or P.O. Box'
	    );
	$arFields[vcAddressFields::KS_TOWN]	= new vcFormFieldTemplate_inline(
	    KSF_RECIP_ADDR_TOWN,
	    'City'
	    );
	$arFields[vcAddressFields::KS_STATE]	= new vcFormFieldTemplate_inline(
	    KSF_RECIP_ADDR_STATE,
	    ''	// label depends on country
	    );
	$arFields[vcAddressFields::KS_ZIP]	= new vcFormFieldTemplate_inline(
	    KSF_RECIP_ADDR_ZIP,
	    ''	// label depends on country
	    );
	// if shipping non-domestic, add country field as well:
	if ($isForeign) {
	    $arFields[vcAddressFields::KS_COUNTRY] = new vcFormFieldTemplate_block(
	      KSF_RECIP_ADDR_COUNTRY,
	      'Country'
	      );
	}
	$arFields['addr-instrux']	= new vcFormFieldTemplate_block(
	  KSF_RECIP_ADDR_INSTRUX,
	  'Delivery instructions (optional)'
	  );
	return $arFields;
    }
    protected function BuildContactTemplate($doEdit) {
	$arFields = $this->BuildContactFieldArray();
	$oFields = new vcContactFields($arFields);
	
	$oTplt = $oFields->BuildTemplate();
	$arCtrls = $this->GetFormBlobject()->RenderControls($doEdit);
	$oTplt->SetVariableValues($arCtrls);
	
	return $oTplt;
    }
    protected function BuildShipAddrTemplate($doEdit) {
	$oZone = $this->GetShipCountry();
	$isForeign = !$oZone->IsDomestic();
	$arFields = $this->BuildShipAddrFieldArray($isForeign);
	
	$oFields = new vcAddressFields($oZone,$arFields);
	
	$oTplt = $oFields->BuildTemplate();
	$arCtrls = $this->GetFormBlobject()->RenderControls($doEdit);
	$oTplt->SetVariableValues($arCtrls);
	
	return $oTplt;
    }
    protected function BuildPayCardNumberTemplateString($doEdit) {
	$sMsg = $doEdit
	  ?"<tr><td colspan=2><span class=note><b>Tip</b>: It's okay to use dashes or spaces in the card number - reduces typing errors!</span></td></tr>"
	  :''
	  ;

	$og = vcGlobals::Me();
	$wsVisa = $og->GetWebSpec_forVisaCard();
	$wsMCard = $og->GetWebSpec_forMasterCard();
	$wsAmex = $og->GetWebSpec_forAmexCard();
	$wsDisc = $og->GetWebSpec_forDiscoverCard();
	
	$knNum = KSF_CARD_NUM;
	$knExp = KSF_CARD_EXP;

	$sTplt = <<<__END__
<table class="form-block" id="card-address">
    <tr><td align=right valign=middle>We accept:</td>
	<td>
	<img align=absmiddle src="$wsVisa" title="Visa">
	<img align=absmiddle src="$wsMCard" title="MasterCard">
	<img align=absmiddle src="$wsAmex" title="American Express">
	<img align=absmiddle src="$wsDisc" title="Discover / Novus">
	</td></tr>
    <tr><td align=right valign=middle>Card Number:</td>
	<td>[[$knNum]] Expires: [[$knExp]] (mm/yy)
	</td></tr>
	$sMsg
</table>
__END__;
	return $sTplt;
    }
    protected function BuildPaymentAddressArray() : array {
	$arFields = array(

	  // card address
	
	  vcAddressFields::KS_NAME => new vcFormFieldTemplate_inline(
	    KSF_CARD_ADDR_NAME,
	    'Cardholder Name'
	    ),
	  vcAddressFields::KS_STREET => new vcFormFieldTemplate_inline(
	    KSF_CARD_ADDR_BLOCK,
	    'Billing Address<br>(street, city, state,<br>postal code)'
	    ),
	  /*
	  vcAddressFields::KS_TOWN => new vcFormFieldTemplate_inline(
	    KSF_CARD_ADDR_TOWN,
	    'City'
	    ),
	  vcAddressFields::KS_STATE => new vcFormFieldTemplate_inline(
	    KSF_CARD_ADDR_STATE,
	    ''	// label depends on country (vcAddressFields will set)
	    ),
	  vcAddressFields::KS_ZIP => new vcFormFieldTemplate_inline(
	    KSF_CARD_ADDR_ZIP,
	    ''	// label depends on country
	    )*/
	  );
	  
	$oZone = $this->GetShipCountry();
	// if shipping non-domestic, add country field as well:
	if (!$oZone->IsDomestic()) {
	    $arFields[vcAddressFields::KS_COUNTRY] = new vcFormFieldTemplate_block(
	      KSF_CARD_ADDR_COUNTRY,
	      'Country'
	      );
	}
	return $arFields;
    }
    // TODO: Move this when working.
    protected function BuildPaymentTemplate($doEdit) {
	$oPayAddrFields = new vcFormFields($this->BuildPaymentAddressArray());
    
	$sTplt = 
	  $this->BuildPayCardNumberTemplateString($doEdit)
	  .$oPayAddrFields->BuildTemplateString()
	  ;
	
	return new fcTemplate_array('[[',']]',$sTplt);
    }
    /* 2019-06-03 obsolete
    protected function GetNameAddressTemplate() {
	throw new exception('2019-06-01 This has been moved to a class.');
    
    
	$oZone = $this->GetShipCountry();

//	$htName = $this->FieldName_forContactName();
	$htStreet = $this->FieldName_forContactStreet();
	$htCity = $this->FieldName_forContactTown();
	$htState = $this->FieldName_forContactState();
	$htZip = $this->FieldName_forContactZipcode();
	
	$htAftState	= is_null($oZone->HasState())?'(if needed)':NULL;
	$sZipLbl	= $oZone->PostalCodeLabel();
	$sStateLbl	= $oZone->StateLabel();
	
	if ($oZone->IsDomestic()) {
	    $htCountryLine = NULL;
	} else {
	    $htCtry = $this->FieldName_forContactCountry();
	    $htCountryLine = "<tr><td align=right valign=middle>Country:</td><td>[[$htCtry]]</td></tr>";
	}
	$htPrecede = $this->PrecedingLinesForTemplate();
	$htFollow = $this->FollowingLinesForTemplate();

	$sTplt = <<<__END__
<table class="form-block" id="name-address">
    $htPrecede
    <tr><td align=right valign=middle>Street Address<br>or P.O. Box:</td><td>[[$htStreet]]</td></tr>
    <tr><td align=right valign=middle>City:</td><td>[[$htCity]]</td></tr>
    <tr><td align=right valign=middle>$sStateLbl:</td><td>[[$htState]]$htAftState</td></tr>
    <tr><td align=right valign=middle>$sZipLbl:</td><td>[[$htZip]]</td></tr>
    $htCountryLine
    $htFollow
</table>
__END__;

//    <tr><td align=right valign=middle>Name on Card:</td><td>[[$htName]]</td></tr>
	return new fcTemplate_array('[[',']]',$sTplt);
    }
*/

    // -- TEMPLATES -- //
    // ++ DIRECT RENDERING ++ //

    /*----
      PURPOSE: formats an address that's stored in separate fields (street, town, state, etc.)
    */
    protected function RenderAddress_fromParts(array $arOptions) {
	throw new exception('2019-07-21 to be written');
    }

    // -- DIRECT RENDERING -- //
}
