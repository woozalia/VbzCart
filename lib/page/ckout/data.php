<?php namespace vbzcart\checkout;

class ctCartDataTable extends \fctArbiRecTable {

    // ++ CONFIG ++ //

    // CEMENT
    protected function GetString_Source() : string { return 'cart_data'; }
    // CEMENT
    protected function SingleRowClass() : string { return __NAMESPACE__.'\crCartDataRow'; }
    // CEMENT (from fctArbiRecTable)
    protected function GetRecordKeyFieldName()		{ return 'ID_Cart'; }
    // CEMENT (from fctArbiRecTable)
    public function GetRecordNameFieldName()		{ return 'name'; }
    // CEMENT (from fctArbiRecTable)
    public function GetRecordValueFieldName()	{ return 'value'; }

    // -- CONFIG -- //
}
class crCartDataRow extends \fcrArbiRecRow {

    // ++ KEYING ++ //

    /*----
      CEMENT
      ASSUMES: $id is a two-element array, $id[0] => ID_Cart and $id[1] => name
    */
    protected function SetKeyValue($id) {
	$this->SetCartID($id[0]);
	$this->SetRowName($id[1]);
    }
    public function GetKeyValue() {
	$id = array(
	  0 => $this->GetCartID(),
	  1 => $this->GetRowName()
	  );
	return $id;
    }
    protected function GetSelfFilter() {
	$idCart = $this->GetCartID();
	$sqlName = $this->GetDatabase()->SanitizeValue($this->GetRowName());
	return "(ID_Cart=$idCart) AND (name=$sqlName)";
    }

    // -- KEYING -- //
    // ++ FIELDS ++ //
    
    protected function SetCartID($id) {
	$this->SetFieldValue('ID_Cart',$id);
    }
    protected function GetCartID() {
	return $this->GetFieldValue('ID_Cart');
    }

    // -- FIELDS -- //
}
class cArbiRecord extends \fcArbiRecord_asRows {
    public function __construct($idRec) {
	$tbl = \fcApp::Me()->GetDatabase()->MakeTableWrapper($this->GetTableClass());
	parent::__construct($tbl,$idRec);
    }
    
    // ++ CLASSES ++ //
    
    protected function GetTableClass() {
	return __NAMESPACE__.'\\ctCartDataTable';
    }
    
    // -- CLASSES -- //
    // ++ VALUES ++ //

      // buyer
    
    protected function ShipInType() {
	$sfName = KSF_INTYPE_SHIP;
	//echo 'INPUT FIELDS '.$this->Dump();
	return $this->GetStatus($sfName);
    }
    protected function CardAddrType() {
	$sfName = KSF_INTYPE_CARD_ADDR;
	return $this->GetStatus($sfName);
    }
    // USED BY: checkout confirmation
    public function ShipMessage() {
	$sfName = KSF_RECIP_ORDER_MSG;
	return $this->GetStatus($sfName);
    }
    // USED BY: checkout confirmation, order conversion
    public function BuyerName() {
	$sfName = KSF_BUYER_ADDR_NAME;
	return $this->GetStatus($sfName);
    }
    public function CardholderName() {
	$sfName = KSF_CARD_ADDR_NAME;
	return $this->GetStatus($sfName);
    }
    // USED BY: checkout confirmation
    public function CardNumber() {
	$sfName = KSF_CARD_NUM;
	return $this->GetStatus($sfName);
    }
    // USED BY: order conversion
    public function CardAddress() {
	$sfName = KSF_CARD_ADDR_BLOCK;
	return $this->GetStatus($sfName);
    }
    // USED BY: checkout confirmation
    public function CardExpiry() {
	$sfName = KSF_CARD_EXP;
	return $this->GetStatus($sfName);
    }
    public function BuyerEmail() {
	$sfName = KSF_BUYER_ADDR_EMAIL;
	return $this->GetStatus($sfName);
    }
    // USED BY: checkout confirmation
    public function BuyerPhone() {
	$sfName = KSF_BUYER_ADDR_PHONE;
	return $this->GetStatus($sfName);
    }

    // recipient
    
    public function ShipToName() {
	$sfName = KSF_RECIP_ADDR_NAME;
	return $this->GetStatus($sfName);
    }
    public function ShipToStreet() {
	$sfName = KSF_RECIP_ADDR_STREET;
	return $this->GetStatus($sfName);
    }
    public function ShipToTown() {
	$sfName = KSF_RECIP_ADDR_TOWN;
	return $this->GetStatus($sfName);
    }
    public function ShipToState() {
	$sfName = KSF_RECIP_ADDR_STATE;
	return $this->GetStatus($sfName);
    }
    public function ShipToZip() {
	$sfName = KSF_RECIP_ADDR_ZIP;
	return $this->GetStatus($sfName);
    }
    public function ShipToCountry() {
	$sfName = KSF_RECIP_ADDR_COUNTRY;
	return $this->GetStatus($sfName);
    }
    public function ShipToInstrux() {
	$sfName = KSF_RECIP_ADDR_INSTRUX;
	return $this->GetStatus($sfName);
    }    
    
    // -- VALUES -- //
    // ++ CALCULATIONS ++ //

    // PUBLIC because Cart object needs to access it
    public function Value_forShipInType_isNew() {
	$sIntype = $this->ShipInType()->value;
	return is_null($sIntype) || ($sIntype == KS_FORM_INTYPE_NEWENTRY);
    }
    /*----
      PUBLIC because Cart object needs to access it
      RETURNS:
	TRUE = new entry for card address, i.e. not the same as shipping address
	FALSE = same as shipping address
    */
    public function Value_forCardAddrType_isNew() {
	$sIntype = $this->CardAddrType()->value;
	return empty($sIntype) || ($sIntype == KS_FORM_INTYPE_NEWENTRY);
    }
    // ALIAS for clarity
    public function Is_CardAddr_SameAs_Shipping() {
	return $this->Value_forCardAddrType_isNew();
    }
    /*----
      USED BY: checkout confirmation (RenderPerson())
    */
    protected function GetStatus_ShipToEmail() : fcElementResult {
	$s = $this->GetValue_forShipToEmail();
	$oRes = new fcElementResult();
	$oRes->exists = TRUE;
	$oRes->value = $s;
	return $oRes;
    }
    /*----
      USED BY: checkout confirmation (RenderPerson())
    */
    protected function GetStatus_ShipToPhone() : fcElementResult {
	$s = $this->GetValue_forShipToPhone();
	$oRes = new fcElementResult();
	$oRes->exists = TRUE;
	$oRes->value = $s;
	return $oRes;
    }
    
    // -- CALCULATIONS -- //
}
