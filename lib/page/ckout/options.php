<?php namespace vbzcart\checkout;
/*
  PURPOSE: classes for managing internal settings used for displaying the checkout pages
    -- just the order confirmation, at this point, I think.
    * isolates option management from other code, to minimize confusion
    * creates a big semi-global structure that we can just pass around as needed
  HISTORY:
    2019-07-18 Extracted from page/ckout/form.php into page/ckout/options.php (new file)
*/
class cOptions {

    static public function CopyFields(array $arVals, array $arKeys) {
	$arOut = array();
	foreach ($arKeys as $sSrce => $sDest) {
	    if (is_null($sDest)) {
		$sDest = $sSrce;
	    }
	    $oField = new cField($sSrce);
	    $oField->FetchElement($arVals,$sDest);
	    $arOut[$sSrce] = $oField;
	}
	return $arOut;
    }

    // ++ SETUP ++ //

    public function __construct(array $arOpts, array $arFields) {
	$this->SetValues($arOpts);
	$this->SetFields($arFields);
    }

    private $arOpts;
    protected function SetValues(array $ar) {
	$this->arOpts = $ar;
    }
    protected function GetValues() : array {
	return $this->arOpts;
    }
    
    private $arFields;
    protected function SetFields(array $ar) {
	$this->arFields = $ar;
    }
    protected function GetFields() : array {
	return $this->arFields;
    }
    
    public function AddValues(array $ar) {
	$this->SetValues(array_merge($this->GetValues(),$ar));
    }
    
    // -- SETUP -- //
    // ++ OUTPUT ++ //
    
      // GENERIC

      protected function GetExists($sName) {
	  return array_key_exists($sName,$this->arOpts);
      }
      // ASSERTS: the key has been set
      public function GetValue($sName) {
	  if (!$this->GetExists($sName)) {
	      echo 'OPTION ARRAY:'.\fcArray::Render($this->GetValues());
	      throw new \exception("VbzCart internal error: attempting to access option [$sName] before it has been set.");
	  }
	  return $this->arOpts[$sName];
      }
      public function GetValueNz($sName,$vDefault=NULL) {
	  if ($this->GetExists($sName)) {
	      return $this->arOpts[$sName];
	  } else {
	      return $vDefault;
	  }
      }
      public function GetField($sName,$vDefault=NULL) {
	  $arFields = $this->GetFields();
	  if (array_key_exists($sName,$arFields)) {
	      $oField = $arFields[$sName];
	      //$oField->SetExists(TRUE);
	      //$oField->SetValue($this->GetValue($sName));
	  } else {
	      $oField = new cField($sName);
	      $oField->SetExists(FALSE);
	      $oField->SetValue(NULL);
	  }
	  return $oField;
      }
      
      // SPECIFIC
      
        /*----
      HISTORY:
	2019-07-16 rescued from cart/ARCHIVE/cart-data-handler.php
	2019-07-30 Moved from content.php to options.php
    */
    public function RenderAddress(\vcShipZone $oZone) {
	if (!is_object($oZone)) {
	    throw new exception('VbzCart internal error: Could not retrieve Shipping Zone object in RenderAddress().');
	}

	$out = "\n<!-- +".__METHOD__."() in ".__FILE__." -->\n";

	/* 2019-07-16 don't know what this was for
	if (!$this->ShowNormalAddress()) {
	    $out .= '<tr><td colspan=2>'.$this->ShowForAddress().'</td></tr>';
	} else { */
	    $strStateLabel = $oZone->StateLabel();	// "province" etc.
	    if ($oZone->hasState()) {
		$htStateAfter = '';
		$lenStateInput = 3;
		// later, we'll also include how many chars the actual abbr is and use this to say "(use n-letter abbreviation)"
	    } else {
		$htStateAfter = ' (if needed)';
		$lenStateInput = 10;
	    }

	    $isCountryFixed = FALSE;
	    if (empty($strCountry)) {
		if (!$oZone->isDomestic()) {
		    // this code cannot possibly work; it will need rewriting
		    throw new exception('2019-07-18 Some updating is still needed here.');
		    $idxCountry = $oAddr->CountryNode()->DataType();
		    $this->DataItem($idxCountry,$oZone->Country());
		}
		$isCountryFixed = !empty($strCountry);
	    }

	    $wpForSpamPolicy = \vcGlobals::Me()->GetWebPath_forPublicWikiPage('Anti-Spam_Policy');
	    //$hrefForSpam = '<a href="'.KWP_WIKI_PUBLIC.'Anti-Spam_Policy">';
	    $htSpamPolicyLink = "<a href='$wpForSpamPolicy'>anti-spam policy</a>";
	    $this->AddValues(
	      array(
		//'ht.before.addr'	=> NULL,	// for now - was not being used
		//'ht.after.addr'	=> NULL,	// was: "$this->htmlAfterAddress", but is never used
		'ht.after.email'	=> $htSpamPolicyLink,
		'ht.ship.combo'		=> $oZone->ComboBox(),
		'ht.after.state'	=> $htStateAfter,
		'str.zip.label'		=> $oZone->PostalCodeLabel(),	// US='Zip Code', otherwise="postal code"
		'str.state.label'	=> $strStateLabel,
		'len.state.inp'		=> $lenStateInput,
		//'do.fixed.all'	=> $this->doFixedCard,	// TRUE = disable editing
		//'do.fixed.name'	=> $this->doFixedName,
		//'do.fixed.ctry'	=> $this->doFixedCountry,
		)
	      );

	    $out .= $this->RenderPerson();
	//}

	// TODO 2019-07-30: update this or discard if not needed
	if (isset($this->msgAfterAddr)) {
	    $out .= '<td colspan=2>'.$this->msgAfterAddr.'</td>';
	}

	return $out;
    }
    /*----
      PURPOSE: Display contact/shipping info in a nice format as part of the checkout process
	This used to display either shipping or payment info, but payment info doesn't break
	the address down into street/city/state/zip anymore, so we have to do that separately.
      TODO 2019-07-16:
	Maybe this should go in form.php?
	Probably rename to RenderShippingInfo() or RenderRecipInfo(), ocelot
	This could be made considerably less tedious and repetitive using some kind of objects for the fields.
	  A lot of the GetValue_for*() functions could be removed.
      HISTORY:
	2014-10-07 Adapting back to clsPerson after (partially?) adapting it for Checkout Page class
	2015-08-31 Changed do.fixed.all default to FALSE, because normally we do want to show the form
	2016-04-14 This isn't being used in checkout anymore. Is it being used anywhere? (disabled on this date)
	2019-07-16 Rescued from cart/ARCHIVE/cart-data-handler.php because we're returning the checkout process
	  to the earlier idea of displaying only cart data to the customer, and converting to order data later.
	  Now in page/ckout/content.php, but belongs in page/ckout.form.php.
	2019-07-30 Moved from content.php to options.php
    */
    public function RenderPerson() {
	//echo 'in RenderPerson - OPTIONS DUMP:'.$this->Dump();
    
	$htBefAddr	= $this->GetValueNz('ht.before.addr');
	$htAftAddr	= $this->GetValueNz('ht.after.addr');
	$htAftState	= $this->GetValue('ht.after.state');
	$htShipCombo	= $this->GetValue('ht.ship.combo');
	$strZipLbl	= $this->GetValue('str.zip.label');
	$strStateLbl	= $this->GetValue('str.state.label');
	$lenStateInp	= $this->GetValue('len.state.inp');	// "length" attribute to use for user input field for "state"
	$doFixedCtry 	= $this->GetValueNz('do.fixed.ctry',TRUE);
	$doFixedName 	= $this->GetValueNz('do.fixed.name',TRUE);
	$doFixedAll	= $this->GetValueNz('do.fixed.all',FALSE);
	$doShipZone	= $this->GetValueNz('do.ship.zone');

	$ofName		= $this->GetField('name');
	$ofAddr		= $this->GetField('addr');
	$ofStreet	= $this->GetField('street');
	$ofTown		= $this->GetField('town');
	$ofState	= $this->GetField('state');
	$ofZip		= $this->GetField('zip');
	$ofCountry	= $this->GetField('country');
	$ofEmail	= $this->GetField('email');
	$ofPhone	= $this->GetField('phone');
	
	/* 2019-07-18 old
	$strName	= $oOpt->GetValue('fld.value.recip.name');
	$strStreet	= $oOpt->GetValue('fld.value.recip.street');
	$strCity	= $oOpt->GetValue('fld.value.recip.town');
	$strState	= $oOpt->GetValue('fld.value.recip.state');
	$strZip		= $oOpt->GetValue('fld.value.recip.zip');
	$strCountry	= $oOpt->GetValue('fld.value.recip.country');
	$oResEmail	= $oOpt->GetValue('fld.status.recip.email');
	$oResPhone	= $oOpt->GetValue('fld.status.recip.phone');
	/* older
	// field names
	$ksName		= $arOptions['fld.name.recip.name'];
	$ksStreet	= $arOptions['fld.name.recip.street'];
	$ksCity		= $arOptions['fld.name.recip.town'];
	$ksState	= $arOptions['fld.name.recip.state'];
	$ksZip		= $arOptions['fld.name.recip.zip'];
	$ksCountry	= $arOptions['fld.name.recip.country'];
	$ksEmail	= $arOptions['fld.name.recip.email'];
	$ksPhone	= $arOptions['fld.name.recip.phone'];

	// OLDER
	// copy constants over to variables to make it easier to insert in formatted output:
	$ksName		= KSF_RECIP_ADDR_NAME;		// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_NAME);
	$ksStreet	= KSF_RECIP_ADDR_STREET;	// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_STREET);
	$ksCity		= KSF_RECIP_ADDR_TOWN;		// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_CITY);
	$ksState	= KSF_RECIP_ADDR_STATE;		// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_STATE);
	$ksZip		= KSF_RECIP_ADDR_ZIP;		// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_ZIP);
	$ksCountry	= KSF_RECIP_ADDR_COUNTRY;	// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_COUNTRY);
	$ksEmail	= KSF_RECIP_ADDR_EMAIL;		// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_EMAIL);
	$ksPhone	= KSF_RECIP_ADDR_PHONE;		// was: $this->Name_forSuffix(_KSF_CART_SFX_CONT_PHONE);

	$strName	= $this->GetValue_forShipToName();
	$strStreet	= $this->GetValue_forShipToStreet();
	$strCity	= $this->GetValue_forShipToTown();
	$strState	= $this->GetValue_forShipToState();
	$strZip		= $this->GetValue_forShipToZipCode();
	$strCountry	= $this->GetValue_forShipToCountry();

	$oResEmail	= $this->GetStatus_ShipToEmail();
	$oResPhone	= $this->GetStatus_ShipToPhone();
*/	
	
	$doEmail	= $ofEmail->IsntBlank();
	$doPhone	= $ofPhone->IsntBlank();

	$strEmail = $strPhone = NULL;
	if ($doEmail) {
	    $strEmail	= $ofEmail->GetValue();
	}
	if ($doPhone) {
	    $strPhone	= $ofPhone->GetValue();
	}

	if ($doFixedAll) {
	    $doFixedCtry = TRUE;
	}

	if ($doFixedCtry) {
	    $htCountry = '<b>'.$ofCountry->GetValue().'</b>';
	    $htZone = '';
	} else {
	    $htCountry = '<input '.$ofCountry->RenderAttribs().' size=20>';
	    $htBtnRefresh = '<input type=submit name="update" value="Update Form">';
//	    $htZone = " shipping zone: $htShipCombo $htBtnRefresh";
	    // this no longer displays the total, so no need for an update button
	    $htZone = $doShipZone?(" shipping zone: $htShipCombo"):'';
	}
	
	$out = NULL;	// start building output
	
// if this contact saves email and phone, then render those:
	if ($doEmail) {
	    $htAfterEmail = $this->GetValue('ht.after.email');
	    $htEmail = "<b>$strEmail</b> ($htAfterEmail)";
	    $out .= "<tr><td align=right valign=middle>Email:</td><td>$htEmail</td></tr>";
	}
	if ($doPhone) {
	    $htPhone = "<b>$strPhone</b> (optional)";
	    $out .= "<tr><td align=right valign=middle>Phone:</td><td>$htPhone</td></tr>";
	}

	if ($doFixedName) {
	    $sVal = $ofName->GetValue();
	    $out .= <<<__END__
<tr><td align=right valign=middle>Name:</td>
	<td><b>$sVal</b></td>
	</tr>
__END__;
	} else {
	    $htAttr = $ofName->RenderAttribs();
	    $out = <<<__END__
<tr><td align=right valign=middle>Name: </td>
	<td><input $htAttr size=50></td>
	</tr>
__END__;
	}	
	
	$out .= $htBefAddr;
	if ($doFixedAll) {
	    $oTplt = new \fcTemplate_array('[[',']]','<b>[[v]]</b>');

	    if ($ofAddr->GetExists()) {
		$htAddr = $ofAddr->RenderFormat($oTplt);
		$htAddr = str_replace("\n",'<br>',$htAddr);
		$out .= <<<__END__
<tr><td align=right>Card Billing Address:</td><td>$htAddr</td></tr>
__END__;
	    } else {
		$htStreet = $ofStreet->RenderFormat($oTplt);
		$htCity = $ofTown->RenderFormat($oTplt);
		$htState = $ofState->RenderFormat($oTplt);
		$htZip = $ofZip->RenderFormat($oTplt);
		$htCtry = $ofCountry->RenderFormat($oTplt).$htZone;

		$out .= <<<__END__
<tr><td align=right valign=middle>Street Address<br>or P.O. Box:</td><td>$htStreet</td></tr>
<tr><td align=right valign=middle>City:</td><td>$htCity</td></tr>
<tr><td align=right valign=middle>$strStateLbl:</td><td>$htState</td></tr>
<tr><td align=right valign=middle>$strZipLbl:</td><td>$htZip</td></tr>
<tr><td align=right valign=middle>Country:</td><td>$htCtry</td></tr>
__END__;
	    }
	} else {
	    throw new exception('2019-07-20 When does this ever get triggered?');
	    $htStreet = '<textarea name="'.$ksStreet.'" cols=50 rows=3>'.$strStreet.'</textarea>';
	    $htCity = '<input name="'.$ksCity.'" value="'.$strCity.'" size=20>';
	    $htState = '<input name="'.$ksState.'" value="'.$strState.'" size='.$lenStateInp.'>'.$htAftState;
	    $htZip = '<input name="'.$ksZip.'" value="'.$strZip.'" size=11>';
//	    $htCtry = "$htCountry - change shipping zone: $htShipCombo $htBtnRefresh";
	    $htCtry = $htCountry.$htZone;

	    $htEmail = "<input name='$ksEmail' value='$strEmail' size=30> $htAfterEmail";
	    $htPhone = '<input name="'.$ksPhone.'" value="'.$strPhone.'" size=20> (optional)';
	}

	$out .= $htAftAddr;

	return $out;
    }

      
    // -- OUTPUT -- //
    // ++ DEBUG ++ //
    
    public function Dump() {
	return \fcArray::Render($this->GetValues());
    }
    
    // -- DEBUG -- //
}
class cField {
    public function __construct($sName) {
	$this->SetName($sName);
    }

    // ++ SETTINGS ++ //
    
    private $sName;
    protected function SetName($sName) {
	$this->sName = $sName;
    }
    protected function GetName() {
	return $this->sName;
    }
    
    private $sValue;
    public function SetValue($s) {
	$this->sValue = $s;
    }
    public function GetValue() {
	return $this->sValue;
    }
    
    private $bExists;
    public function SetExists($b) {
	$this->bExists = $b;
    }
    public function GetExists() {
	return $this->bExists;
    }
    
    // -- SETTINGS -- //
    // ++ CALCULATIONS ++ //
    
    public function IsntBlank() {
	if ($this->GetExists()) {
	    return strlen($this->GetValue()) > 0;
	} else {
	    return FALSE;
	}
    }
    
    // -- CALCULATIONS -- //
    // ++ ACTIONS ++ //

    public function FetchElement(array $arVals,$sKey) {
	if (is_null($sKey)) {
	    $sKey = $this->GetName();
	}
	if (array_key_exists($sKey,$arVals)) {
	    $this->SetValue($arVals[$sKey]);
	    $this->SetExists(TRUE);
	} else {
	    //echo "KEY [$sKey] NOT FOUND<br>";
	    $this->SetValue(NULL);
	    $this->SetExists(FALSE);
	}
    }

    // -- ACTIONS -- //
    // ++ OUTPUT ++ //

    public function RenderFormat(\fcTemplate $oTplt) {
	$oTplt->SetVariableValue('n',$this->GetName());
	$oTplt->SetVariableValue('v',$this->GetValue());
	return $oTplt->Render();
    }

    // -- OUTPUT -- //
}
/* 2019-07-28 is anything still using this?
class cFieldInfo {

    private $sName, $vValue;
    public function __construct($sName,$vValue) {
	$this->sName = $sName;
	$this->vValue = $vValue;
    }
    
    public function GetName() {
	return $this->sName;
    }
    public function GetValue() {
	return $this->vValue;
    }
    public function DoesExist() {
	return !is_null($this->GetName());
    }
    public function IsntBlank() {
	$sValue = $this->GetValue();
	if (!is_string($sValue)) {
	    if (is_null($sValue)) {
		return FALSE;
	    }
	    // not a string, but not NULL either; throw error
	    $sName = $this->GetName();
	    $sType = gettype($this->GetValue());
	    throw new \exception("VbzCart internal error: IsntBlank() called on non-string [$sName], which is type [$sType].");
	}
	return strlen($this->GetValue()) > 0;
    }
    
    public function RenderAttribs() {
	$sName = $this->GetName();
	$vValue = $this->GetValue();
	return "name='$sName' value='$vValue'"; 
    }
    public function RenderFormat(\fcTemplate $oTplt) {
	$oTplt->SetVariableValue('n',$this->GetName());
	$oTplt->SetVariableValue('v',$this->GetValue());
	return $oTplt->Render();
    }
}*/
