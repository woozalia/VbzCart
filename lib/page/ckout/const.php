<?php
/*
  PURPOSE: constants for checkout process
  HISTORY:
    2013-09-13 split off from cart-data.php
    2013-11-07 reorganizing/renaming cart data constants
    2013-11-10 renamed from cart-const.php to vbz-const-ckout.php
    2016-03-08 Commented out KSF_CART_RECIP_SHIP_ZONE, added KSF_CART_SHIP_ZONE
      We don't need to calculate anything based on the payment address.
    2018-02-19 Moved KSF_CART_SHIP_ZONE to cart.const.php because checkout doesn't seem to use it.

  USED BY:
    cart.php uses KSF_CART_RECIP_SHIP_ZONE
    cart-data.php (OBSOLETE) uses all of them (I think)
      especially in clsCartVars and VbzAdminCartDatum
    admin.cart.php uses a lot but not all
  NOTE: Each KI_CART_* entry should also have an entry
    in VCR_CartField_admin::$arTypeNames (in dropins/cart/fields.php
    so that the admin dropin can display human-friendly field names.
    There's got to be a better way to do this, however...
*/

/* ============================================
 SECTION: field keys for retrieving checkout form data
  These define the HTML names of form controls
    and are also used to retrieve the user-entered values
    from the submitted data.
*/

// format for order lines

/* 2019-05-31 not sure if these are still used
define('KSF_SHIP_IS_CARD'	,'ship-is-billing');	// TRUE = shipping address same as billing/card
define('KSF_SHIP_MESSAGE'	,'ship-message');
*/

// blob data field names - these can change over time, but that will break the ability to convert carts to orders
  // BUYER CONTACT fields
define('KSF_BUYER_ADDR_NAME',	'buyer-addr-name');	// what we should call the customer
define('KSF_BUYER_ADDR_EMAIL',	'buyer-addr-email');	// TODO: should just be BUYER_EMAIL
define('KSF_BUYER_ADDR_PHONE',	'buyer-addr-phone');	// TODO: should just be BUYER_PHONE
  // BUYER PAYMENT fields
define('KSF_INTYPE_CARD_ADDR',	'intype-card-addr');	// card address instamode button-switch
define('KSF_CARD_ADDR_NAME',	'card-addr-name');
define('KSF_CARD_ADDR_BLOCK',	'card-addr-block');	// card address is currently a single field
/* This is only if we decide to break the card address into parts
define('KSF_CARD_ADDR_STREET',	'card-addr-street');
define('KSF_CARD_ADDR_TOWN',	'card-addr-town');
define('KSF_CARD_ADDR_STATE',	'card-addr-state');
define('KSF_CARD_ADDR_ZIP',	'card-addr-zip');
define('KSF_CARD_ADDR_COUNTRY',	'card-addr-country');
*/
define('KSF_CARD_NUM',		'card-num');
define('KSF_CARD_EXP',		'card-exp');
  // RECIP fields:
define('KSF_INTYPE_SHIP',	'intype-ship');
define('KSF_RECIP_ADDR_NAME',	'recip-addr-name');
define('KSF_RECIP_ADDR_PHONE',	'recip-addr-phone');
define('KSF_RECIP_ADDR_EMAIL',	'recip-addr-email');
define('KSF_RECIP_ADDR_STREET',	'recip-addr-street');
define('KSF_RECIP_ADDR_TOWN',	'recip-addr-town');
define('KSF_RECIP_ADDR_STATE',	'recip-addr-state');
define('KSF_RECIP_ADDR_ZIP',	'recip-addr-zip');
define('KSF_RECIP_ADDR_COUNTRY','recip-addr-country');
define('KSF_RECIP_ADDR_INSTRUX','dest-msg');	// instructions that should always be included in the address (e.g. "back door")
define('KSF_RECIP_ORDER_MSG',	'order-msg');	// message to recipient just for this order

/* 2019-05-31 I think these are all obsolete now; field names should be defined in full, not build, for easier searching
// == common prefixes

//define('_KSF_CART_SFX_CONT_CHOICE'	,'choice-id');

// == buyer

define('_KSF_CART_PFX_BUYER'	,'buyer-');			// common prefix for buyer fields
//define('KSF_CART_BUYER_ID'	,_KSF_CART_PFX_BUYER._KSF_CART_SFX_CONT_ID);	// person profile for buyer
define('KSF_CART_BUYER_EMAIL'	,_KSF_CART_PFX_BUYER.'email');
define('KSF_CART_BUYER_PHONE'	,_KSF_CART_PFX_BUYER.'phone');

// == payments
define('_KSF_CART_PFX_PAY'		,'pay-');			// common prefix for payment types

// == payment check info

define('KSF_CART_PAY_CHECK_NUM'	,_KSF_CART_PFX_PAY.'check-num');

// == recipient

//define('_KSF_CART_PFX_RECIP'		,'recip-');			// common prefix for recipient fields
//define('KSF_CART_RECIP_CONT_CHOICE'	,_KSF_CART_PFX_RECIP._KSF_CART_SFX_CONT_CHOICE);
*/

// =====================================
// keys for retrieving stored field data
//

// == recipient

/* 2019-05-31 I think these were rendered obsolete when cart data was moved into blobs
define('KI_CART_SHIP_ZONE'	,100);
//define('KI_CART_RECIP_ID'	,151);	// added 2013-11-07, disabled 2014-09-14 - Dup of KI_CART_RECIP_CHOICE
define('KI_CART_RECIP_INTYPE'	,152);	// added 2014-07-29 (INTYPE = input type)
define('KI_CART_RECIP_CHOICE'	,153);	// added 2014-08-21 (drop-down choice)
define('KI_CART_RECIP_NAME'	,101);
define('KI_CART_RECIP_STREET'	,102);
define('KI_CART_RECIP_CITY'	,103);
define('KI_CART_RECIP_STATE'	,104);
define('KI_CART_RECIP_ZIP'	,105);
define('KI_CART_RECIP_COUNTRY'	,106);
define('KI_CART_RECIP_MESSAGE'	,107);
define('KI_CART_RECIP_IS_BUYER',110);
define('KI_CART_SHIP_IS_CARD'	,113);	// does this still make sense?

define('KI_CART_RECIP_EMAIL'	,111);
define('KI_CART_RECIP_PHONE'	,112);

// == buyer

//define('KI_CART_BUYER_ID'	,251);	// added 2013-11-07, disabled 2014-09-14 - Dup of KI_CART_PAY_CARD_CHOICE
define('KI_CART_BUYER_EMAIL'	,211);
define('KI_CART_BUYER_PHONE'	,212);

// == payment card

define('KI_CART_PAY_CARD_INTYPE'	,261);	// added 2014-07-29; name/number changed 2014-08-24
define('KI_CART_PAY_CARD_CHOICE'	,262);	// added 2014-08-22; name/number changed 2014-08-24
define('KI_CART_PAY_CARD_NAME'		,204);	// 2013-11-07: for now, this is assumed to be the buyer's name. Later, we should differentiate.
define('KI_CART_PAY_CARD_NUM'		,202);
define('KI_CART_PAY_CARD_EXP'		,203);
define('KI_CART_PAY_CARD_ENCR'		,252);	// added 2014-02-13
define('KI_CART_PAY_CARD_STREET'	,205);
define('KI_CART_PAY_CARD_CITY'		,206);
define('KI_CART_PAY_CARD_STATE'		,207);
define('KI_CART_PAY_CARD_ZIP'		,208);
define('KI_CART_PAY_CARD_COUNTRY'	,209);

// == payment check

define('KI_CART_PAY_CHECK_NUM'	,230);

// calculated data
define('KI_CART_CALC_SALE_TOTAL'	,301);	// A. item price total
define('KI_CART_CALC_PER_ITEM_TOTAL'	,302);	// B. per-item shipping total
define('KI_CART_CALC_PER_PKG_TOTAL'	,303);	// C. max per-package shipping total
define('KI_CART_CALC_SHIP_TOTAL'	,304);	// D. shipping total (B+C)
define('KI_CART_CALC_FINAL_TOTAL'	,305);	// E. final total (A+D)
*/
