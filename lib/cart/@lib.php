<?php namespace ferreteria\classloader;

//$fp = dirname( __FILE__ );
//fcCodeModule::BasePath($fp.'/');

$om = $oLib->MakeModule('cart.logic.php');
  $om->AddClass('vctCarts');
  $om->AddClass('vcrCart');
$om = $oLib->MakeModule('cart.shop.php');
  $om->AddClass('vctShopCarts');
  $om->AddClass('vcrShopCart');
$om = $oLib->MakeModule('cart-display.php');
  $om->AddClass('vcCartDisplay');
  $om->AddClass('vcCartDisplay_full');
  $om->AddClass('vcCartDisplay_full_shop');
  $om->AddClass('vcCartDisplay_full_ckout');
  $om->AddClass('vcCartDisplay_full_TEXT');
$om = $oLib->MakeModule('cart-display-line.php');
  $om->AddClass('vcCartLine_form');
  $om->AddClass('vcCartLine_static');
  $om->AddClass('vcCartTotal_admin');
  $om->AddClass('vcCartLine_text');
$om = $oLib->MakeModule('cart-lines.php');
  $om->AddClass('vctShopCartLines');
$om = $oLib->MakeModule('cart-log.php');
  $om->AddClass('vctCartLog');
  $om->AddClass('vcrCartEvent');
