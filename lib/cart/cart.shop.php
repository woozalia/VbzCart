<?php
/*
  PURPOSE: shopping cart classes: shopping UI
  HISTORY:
    2016-03-07 Split off some methods from clsShopCart[s] (later renamed vctShopCart[s])
    2018-02-19 General updating shenanigans; require cart.const.php
*/
require_once('cart.const.php');

define('KS_CLASS_LOGGER','vctCartLog');

class vctShopCarts extends vctCarts {
    use ferreteria\odata\tLoggableTable;

    // ++ SETUP ++ //
    
    // CEMENT
    public function GetActionKey() : string { return 'cart'; }
    // OVERRIDE
    protected function SingleRowClass() : string { return 'vcrShopCart'; }

    // -- SETUP -- //
    // ++ CLASSES ++ //

    protected function GetEventsClass() : string { return KS_CLASS_LOGGER; }

    // -- CLASSES -- //
    // ++ SHOPPING UI ++ //

    /*----
      ACTION: 
      1. We're doing stuff to the current cart, so fetch it or create a new one.
      2. Check form input to see if anything needs to be done to it.
      3. Save any Cart changes; make sure Session record is tracking the Cart ID.
      ASSUMES: We have form input that actually requires a cart record. (Caller should check this;
        don't call if cart not required.)
      HISTORY:
        2013-11-09 moved from clsShopCart to clsShopCarts (later renamed vctShopCarts and then vctCarts)
    */
    public function HandleCartFormInput() {
	$rcCart = $this->GetCartRecord_toWrite();
	// TODO: save Cart ID to Session record!
	$rcCart->HandleFormInput();	// NOTE: this may redirect, exiting the code without returning
    }
    /*----
      ACTION: Renders the current cart
      HISTORY:
	2013-11-10 Significant change to assumptions. A cart object now only exists
	  to represent a cart record in the database. The cart table object now handles
	  situations where there is no cart record.
    */
    public function RenderCart($bEditable) {
	if ($this->CartIsRegistered()) {
	    $rcCart = $this->GetCartRecord_ifWriteable();
	    $rcCart->SetEditable($bEditable);
	    $out = $rcCart->Render();
	} else {
	throw new exception('Is this being called when items are first added?');
	// 2018-02-24 The rest of this can't work anymore...
	    $out = "<font size=4>You have not put anything in your cart yet.</font>";
	    $sDesc = 'displaying cart - nothing yet; zone '.$this->ShipZoneObj()->Abbr();
	    $arEv = array(
	      clsSysEvents::ARG_CODE		=> '0cart',
	      clsSysEvents::ARG_DESCR_START	=> $sDesc,
	      clsSysEvents::ARG_WHERE		=> __FILE__.' line '.__LINE__,
	      );
	    $this->StartEvent($arEv);
	}
	return $out;
    }

    // -- SHOPPING UI -- //

}
class vcrShopCart extends vcrCart {
    //use ferreteria\data\tRecordSaver;

    // ++ STATUS ++ //
    
    private $bEditable;
    public function SetEditable($b) {
	$this->bEditable = $b;
    }
    protected function GetEditable() {
	return $this->bEditable;
    }
    
    // -- STATUS -- //
    // ++ FRAMEWORK ++ //
    
    protected function GetPageObject() {
	return fcApp::Me()->GetPageObject();
    }

    // -- FRAMEWORK -- //
    // ++ CLASSES ++ //

    protected function GetEventsClass() {
	return KS_CLASS_LOGGER;
    }

    // -- CLASSES -- //
    // ++ DISPLAY INPUT ++ //

    static protected function FoundInputButton_AddToCart() {
	return vcGlobals::Me()->FoundInputButton_AddToCart();
    }
    /*----
      ACTION: Looks at the received form data to see if a cart should be required.
	For now, I'm assuming that any in-cart functions (such as delete, recalculate,
	etc.) do *not* require a new cart because the cart would already exist,
	but the rules can be fine-tuned as needed.
    */
    static public function FormInputNeedsCart() {
	return self::FoundInputButton_AddToCart();
    /*
	$yes = array_key_exists(KSF_CART_BTN_ADD_ITEMS,$_POST);
	return $yes;
    */
    }
    /*----
      ACTION: Do whatever needs to be done to the current cart based on the form input
	1. Check form input to see what (if anything) needs to be done.
	2. Save any changes back to the Cart and Lines records.
    */
    public function HandleFormInput() {
    //throw new exception('GOT TO HERE');
// check for input
	// - buttons
	$doAddItems	= self::FoundInputButton_AddToCart();	// array_key_exists(KSF_CART_BTN_ADD_ITEMS,$_POST);
	$doRecalc	= array_key_exists(KSF_CART_BTN_RECALC,$_POST);
	$doCheckout	= array_key_exists(KSF_CART_BTN_CKOUT,$_POST);
	// - other items
	$sShipZone	= fcArray::Nz($_POST,KSF_CART_SHIP_ZONE);
	$doShipZone	= !is_null($sShipZone);
	$sDelete	= fcArray::Nz($_GET,KSF_CART_DELETE);
	$doDelete	= !is_null($sDelete);
	
	$isCart = ($doRecalc || $doCheckout);	// there must already be a cart under these conditions
	$doItems = ($doAddItems || $doRecalc);	// if true, there are items to process
	$doRefresh = FALSE;	// should we refresh the page (clean URL / remove POST)?

	// receive selected shipping zone from form
	if ($doShipZone) {
	    if ($sShipZone != $this->GetZoneCode()) {
		// if it has changed:
		$this->SetZoneCode($sShipZone);	// set it in memory
		//$this->UpdateZone();		// save change back to db
		$this->Save();			// save change back to db
	    }
	}
	
	// check for specific actions
	if ($doItems) {
	    if ($isCart) {
		// zero out all items, so only items in visible cart will be retained:
		$this->ZeroAll();
	    }
	    // get the list of items posted
	    $arItems = vcGlobals::Me()->GetCartItemsInput();
	    // add each non-empty item
	    $db = $this->GetDatabase();
	    foreach ($arItems as $key => $val) {
		if (!empty($val)) {
		    $nVal = (int)0+$val;
		    $sCatNum = $db->SanitizeString($key);	// prevent SQL injection when adding to cart items table
		    $this->AddItem($sCatNum,$nVal);
		}
	    } // END for each item
	    $doRefresh = TRUE;
	// END do add items
	} elseif ($doDelete) {
	    if ($sDelete == KSF_CART_DELETE_ALL) {
		$this->LogEvent('clr','voiding cart');
		//$this->ID = -1;
		$this->SessionRecord()->DropCart();
	    } elseif (is_numeric($sDelete)) {
		$idLine = $sDelete;
		$this->DelLine($idLine);
		$idCart = $rcCart->GetKeyValue();
		$this->LogEvent('del',"deleting line ID $idLine");
	    } else {
		throw new exception('Received unexpected delete value "'.$sDelete.'".');
	    }
	    $doRefresh = TRUE;
	}
	
	if ($doCheckout) {
	    $this->CreateEvent('ck1','going to checkout');
	    fcHTTP::Redirect(vcGlobals::Me()->GetWebPath_forCheckoutPage());
	    // not sure if PHP returns from a redirect; this might never get executed:
	    $this->CreateEvent('ck2','sent redirect to checkout');
	} elseif ($doRefresh) {
	    $this->UpdateTimestamp();	// record that the cart was updated
	    // if we changed anything, redirect to a clean URL with no POST data:
	    fcHTTP::Redirect(vcGlobals::Me()->GetWebPath_forCartPage());
	}
    }
    /*----
      ACTION: update the datablob field from form input
      CALLED BY: checkout page content object
    */
    public function UpdateBlob() {
	$oMgr = $this->FieldsManager();
	// calculate resulting blob
	$oMgr->UpdateBlobs();
    }
    
    // -- DISPLAY INPUT -- //
    // ++ DISPLAY OUTPUT ++ //

    /*----
      FUNCTION: DisplayObject()
      CALLED BY this.Render() (below)
    */
    private $oPainter;
    protected function DisplayObject() {
    
	if (empty($this->oPainter)) {
	    $bEditable = $this->GetEditable();
	    $oZone = $this->ShipZoneObject();
	    if ($bEditable) {
		$oPainter = new vcCartDisplay_full_shop($oZone);
	    } else {
		$oPainter = new vcCartDisplay_full_ckout($oZone);
	    }
	    $rsLine = $this->LineRecords();
	    while ($rsLine->NextRow()->GetIsFound()) {
		if ($bEditable) {
		    $oLine = $rsLine->GetRenderObject_editable();
		} else {
		    $oLine = $rsLine->GetRenderObject_static();
		}
		$oPainter->AddLine($oLine);
	    }
	    $this->oPainter = $oPainter;
	}
	return $this->oPainter;
    }
    /*----
      RETURNS: HTML rendering of cart, including current contents and form controls
      CALLED BY table.RenderCart()
      HISTORY:
	2013-11-10 Significant change to assumptions: A cart object now only exists
	  to represent a cart record in the database. Any functions that need to work
	  when there is no record are now handled by the cart table object.
    */
    public function Render() {
	$id = $this->GetKeyValue();
	$oPage = $this->GetPageObject();
	$oPage->AddFooterStat('cart',$id);
	$oPage->AddFooterStat('sess',$this->GetSessionID());
	$out = NULL;
	if ($this->HasLines()) {
	    $oPainter = $this->DisplayObject();
	    $out .= $oPainter->Render();
	} else {
	    $sMsg = "You have a cart (#$id), but it's empty. We're not quite sure how that happened.";
	    $out = $oPage->AddWarningMessage($sMsg);
	    $sWhat = 'displaying cart - empty; zone '.$this->GetZoneCode();
	    /*
	    $arEv = array(
	      fcrEvent::KF_CODE		=> 'disp',
	      fcrEvent::KF_DESCR_START	=> $sWhat,
	      fcrEvent::KF_WHERE		=> __FILE__.' line '.__LINE__,
	      fcrEvent::KF_IS_ERROR		=> TRUE,
	      );
	    $this->CreateEvent($arEv);
	    */
	    $this->CreateEvent('empty',$sWhat);
	}
	return $out;
    }
    
    // -- DISPLAY OUTPUT -- //
}
