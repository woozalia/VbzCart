<?php
/*::::
  PURPOSE: manager class for Cart field groups
    Handles blob data collectively and per-field
  HISTORY:
    2016-06-16 split off from cart.data.fg.php (formerly cart.xdata.php)
*/
class vcCartDataManager {

    // ++ SETUP ++ //

//    public function __construct(vcrCart $rcCart, vcShipCountry $oZone) {
    public function __construct(vcrCart $rcCart, vcCartForm $oForm) {
	$this->SetCartRecord($rcCart);
	$this->SetFormObject($oForm);
	//$this->GetFormObject()->SetShipZone($oZone);
    }
    private $rcCart;
    protected function SetCartRecord(vcrCart $rcCart) {
	$this->rcCart = $rcCart;
    }
    protected function GetCartRecord() {
	return $this->rcCart;
    }
    private $oForm;
    protected function SetFormObject(vcCartForm $oForm) {
	$this->oForm = $oForm;
    }
    protected function GetFormObject() : vcCartForm {
	return $this->oForm;
    }
    
    // -- SETUP -- //
    // ++ INTERNAL OBJECTS ++ //
    
    protected function GetBlobObject() : fcBlobField {
	// GetFormObject() returns vcCartForm which inherits fcForm_blob
	return $this->GetFormObject()->GetBlobField();
    }
    protected function GetBlobString() {
    //echo 'SERIAL BLOB:<pre>'.$this->GetCartRecord()->GetSerialBlob().'</pre>';
	return $this->GetCartRecord()->GetSerialBlob();
    }
    protected function SetBlobString($s) {
	$this->GetCartRecord()->SetSerialBlob($s);
    }
    /*----
      ACTION: unserialize the blob and store it locally as an array
      PUBLIC because... well, maybe there's a better way to do this,
	but I don't know what it is. Cart objects need to be able to
	update the blob...
      HISTORY:
	2016-06-06 I have no idea why everything but the first line was commented out. Uncommented.
	2019-03-08 Not sure why the entire function was commented out -- it's needed. Uncommented.
    */
    public function FetchBlob() {
	$sBlob = $this->GetBlobString();
	if (is_null($sBlob)) {
	    $this->SetBlobArray(array());	// no data yet
	} else {
	    $arBlob = unserialize($sBlob);
	    $this->SetBlobArray($arBlob);
	}
    }
    /*----
      ACTIONS:
	* serialize the local array
	* save serialized array back to the blob object
	* save serialized array back to recordset field
	(Don't update recordset; caller should do that.)
      PUBLIC because... see FetchBlob()
    */
    public function StoreBlob() {
	$sBlob = serialize($this->GetBlobArray());
	$this->SetBlobString($sBlob);
    }
//    private $arBlob;

    protected function GetBlobArray() {
	return $this->GetBlobObject()->GetArray();
    }
    /*----
      USED BY: $this->FetchBlob()
    */
    protected function SetBlobArray(array $ar) {
	$this->arBlob = $ar;
    }
    
    // -- INTERNAL OBJECTS -- //
    // ++ INTERNAL DATA ++ //

    /* 2019-05-04 pretty sure this is the wrong way to do it now
    private $arMissed = array();
    protected function AddMissing(array $arMissed) {
	$this->arMissed = array_merge($this->GetMissingArray(),$arMissed);
    }
    public function GetMissingArray() {
	return $this->arMissed;
    }
    */

    // -- INTERNAL DATA -- //
    // ++ DISPATCH ++ //

    /*----
      PUBLIC because it's only called from vcPageContent_ckout::DoPage()
      HISTORY:
	2019-05-09 now also loads controls and saves data -- should probably be renamed "HandlePageData" OSLT.
    */
    public function LoadControlsForPage($nPage) {
	//echo 'ENTERING '.__METHOD__." FOR PAGE [$nPage]<br>";
	switch($nPage) {
	  case KI_SEQ_CART:
	    // tentatively, no controls needed because form is handled elsewhere
	    break;
	  case KI_SEQ_SHIP:
	    $this->GetRecipObject();
	    break;
	 case KI_SEQ_PAY:
	    $this->GetBuyerObject();
	    break;
	  case KI_SEQ_CONF:
	    $this->GetBuyerObject();
	    $this->GetRecipObject();
	    break;
	  case KI_SEQ_RCPT:
	    $this->GetBuyerObject();
	    $this->GetRecipObject();
	    break;
	  default:
	    echo 'NO PAGE SELECTED<br>';
	    // TODO: log an error -- possible cracking attempt
	}
	//echo 'LEAVING '.__METHOD__.'<br>';
    }

    // -- DISPATCH -- //
    // ++ SUBSETS ++ //
    
    private $oBuyer = NULL;
    // PUBLIC so the shopping cart object can access
    public function GetBuyerObject() {
	echo 'CREATING BUYER OBJECT<br>';
	if (is_null($this->oBuyer)) {
	    $this->oBuyer = new vcCartData_Buyer($this->GetFormObject());
	}
	return $this->oBuyer;
    }
    
    private $oRecip = NULL;
    // PUBLIC so the shopping cart object can access
    public function GetRecipObject() {
	if (is_null($this->oRecip)) {
	    $this->oRecip = new vcCartData_Recip($this->GetFormObject());
	}
	return $this->oRecip;
    }

    // -- SUBSETS -- //
    // ++ PAGES ++ //
        
    /*
      NOTES:
	* *rendering* a page includes loading any existing values so they can be displayed.
	* *capturing* a page includes loading any existing values so they are not overwritten by blanks.
	  This has to be done by the objects, though, since the field objects are not yet defined here.
    */
    
      // SHIPPING //
    
    public function RenderShippingPage() {
	//echo 'GOT TO RenderShippingPage()<br>';
	//$oCDMgr = $this->FieldsManager();
	$oCDMgr = $this;
	$oCDMgr->FetchBlob();

	$oCD_Buyer = $oCDMgr->GetBuyerObject();
	$oCD_Recip = $oCDMgr->GetRecipObject();

	$out =
	  (new fcSectionHeader('Buyer information:'))->Render()
	  .$oCD_Buyer->RenderContact(TRUE)	// edit email/phone
	  .(new fcSectionHeader('Shipping information:'))->Render()
	  .$oCD_Recip->RenderShipping(TRUE)	// edit shipping address / instructions
	  ;
	return $out;
    }
    public function CaptureShippingPage() {
	//echo 'GOT TO CaptureShippingPage()<br>';
	//$oCDMgr = $this->FieldsManager();
	$oCDMgr = $this;
	$oCDMgr->FetchBlob();
	
	$oCD_Buyer = $oCDMgr->GetBuyerObject();
	$oCD_Recip = $oCDMgr->GetRecipObject();
	
	//$arStat = $this->GetFormObject()->Receive($_POST);
	//$oFormStat = $this->GetFormObject()->Receive($_POST);

	/* 2019-05-05 This seems to be redundant now
	$oCD_Buyer->CaptureContact();	// email/phone
	$oCD_Recip->CaptureShipping();	// shipping address / instructions
	*/

	/* 2019-05-04 pretty sure this is the wrong way to do it now
	$this->AddMissing($oCD_Buyer->GetMissingArray());
	$this->AddMissing($oCD_Recip->GetMissingArray());
	*/
	
	// calculate resulting blob
	//echo 'BLOB AFTER FETCH: '.$oCDMgr->RenderBlob();
	//echo 'UPDATING BUYER -> BLOB<br>';
	$oCDMgr->UpdateBlob($oCD_Buyer);
	//echo 'UPDATING RECIP -> BLOB<br>';
	$oCDMgr->UpdateBlob($oCD_Recip);
	//echo 'STORING BLOB<br>';
	$oCDMgr->StoreBlob();
	//echo 'SHIPPING VALUES:'.fcArray::Render($this->Values());
	//echo 'SHIPPING UPDATE ARRAY:'.fcArray::Render($this->UpdateArray());
	
	$rcCart = $this->GetCartRecord();
	$rcCart->Save();
	//echo 'SHIPPING SAVE SQL: ['.$this->sqlExec.']<br>';
	//die();
    }
    
      // BILLING //
    
    public function RenderBillingPage() {
	//echo 'GOT TO RenderBillingPage()<br>';
	//$oCDMgr = $this->FieldsManager();
	$oCDMgr = $this;
	$oCDMgr->FetchBlob();	// fetch blob data from db
	
	$oCD_Buyer = $oCDMgr->GetBuyerObject();
	
	$oHdr = new fcSectionHeader('Payment information:');
	
	$out =
	  $oHdr->Render()
	  .$oCD_Buyer->RenderPayment(TRUE)	// edit payment information
	  ;
	return $out;
    }
    public function CaptureBillingPage() {
	//echo 'GOT TO CaptureBillingPage()<br>';
	//$oCDMgr = $this->FieldsManager();
	$oCDMgr = $this;
	$oCDMgr->FetchBlob();
	
	$oCD_Buyer = $oCDMgr->GetBuyerObject();
	$oCD_Buyer->CapturePayment();	// card #/exp, and I *think* name/address
	/* 2019-05-05 pretty sure this is redundant now
	$this->AddMissing($oCD_Buyer->GetMissingArray());
	*/
	
	// calculate resulting blob
	$oCDMgr->UpdateBlob($oCD_Buyer);
    }
    
      // PAYMENT TYPE //
    
    public function RenderPayTypeSection() {
	$oPage = $this->GetPageObject();
	//$oCDMgr = $this->FieldsManager();
	$oCDMgr = $this;
	$oCD_PayType = $oCDMgr->PayTypeObject();
	
	$sWhere = __METHOD__.'() in '.__FILE__;
	$out =
	  "\n<!-- vv $sWhere vv -->"
	  .$oPage->SectionHeader('Payment type:')
	  ."\n<table class='form-block' id='pay-type'>"
	  ;

	$isShipCardSame = $this->FieldsObject()->IsShipToCard();
	$htChecked = $isShipCardSame?' checked':'';

	$out .= "\n<tr><td align=center>\n"
	  .$this->Skin()->RenderPaymentIcons()
	  ."<table><tr><td>"
	  .'<input name=payType value="'.KSF_CART_PTYP_CARD_HERE.'" type=radio checked disabled> Visa / MasterCard / Discover / American Express - pay here'
	  .'<br>&emsp;<input name="'.KSF_SHIP_IS_CARD.'" type=checkbox value=1'.$htChecked.'>billing address is same as shipping address above'
	  ."</td></tr></table>\n"
	  ."<hr>More payment options will be available soon.\n"
	  ."</td></tr>";

	$out .=
	  "\n</table>"
	  .$this->Skin()->SectionFooter()
	  ."\n<!-- ^^ $sWhere ^^ -->"
	  ;
	return $out;
    }
    
    // -- PAGES -- //
    // ++ FORM I/O ++ //
    
    /*----
      ACTION:
      USAGE: Call FetchBlob() before calling this, and StoreBlob() when done with all updates.
    */
    public function UpdateBlob(vcCartDataFieldGroup $oData) {
	$arForm = $oData->GetFormArray();
	//echo 'FORM DATA FOR BLOB:'.fcArray::Render($arForm);
	if (is_array($arForm)) {
//	    $this->SetBlobArray(array_merge($this->GetBlobArray(),$ar));
	    $oBlob = $this->GetBlobObject();
	    $oBlob->MergeArray($arForm);
	}
    }
    /*----
      ACTION: Call UpdateBlob() for any data groups that have been modified.
	For now, we just call it for both of them. Maybe this is more maintainable regardless.
    */
    public function UpdateBlobs() {
	$this->UpdateBlob($this->GetBuyerObject());
	$this->UpdateBlob($this->GetRecipObject());
	$this->StoreBlob();
    }
    
    // -- FORM I/O -- //
}

