<?php
/*
  PURPOSE: handles extended cart data stored in the cart's FieldData BLOB field.
  HISTORY:
    2016-03-08 started
    2016-06-16 renamed from cart.xdata.php to cart.data.fg.php
*/

class vcCartDataFieldGroup {
    use ftFrameworkAccess;

    // ++ SETUP ++ //
    
    /*----
      HISTORY:
	2019-05-10 This seems to never get called, and I don't know why.
	2019-05-17 Now it does. Maybe we just weren't getting far enough along in the process?
    */
    public function __construct(vcCartForm $oForm) {
	echo '2019-05-10 GOT TO '.__METHOD__.'<br>';
	$this->SetFormObject($oForm);
	$this->CreateFields();
    }
    // STUB
    public function CreateFields() {}
    
    // -- SETUP -- //
    // ++ I/O ++ //
    //++internal data++//

    /*
    private $oBlob;
    protected function SetBlobObject(vcCartBlob $oBlob) {
	$this->oBlob = $oBlob;
    }
    */
    protected function GetBlobObject() : fcBlobField {
	return $this->GetFormObject()->GetBlobField();
    }
    // PURPOSE: debugging
    public function DumpArray() {
	return fcArray::Render($this->GetBlobObject()->GetArray());
    }
    protected function GetValue($id) {
	return $this->GetBlobObject()->GetValue($id);
    }
    protected function SetValue($id,$sValue) {
	$this->GetBlobObject()->SetValue($id,$sValue);
    }
    
    //--internal data--//
    // -- I/O -- //
    // ++ FORMS ++ //

    private $oForm;
    protected function SetFormObject(vcCartForm $o) {
        $this->oForm = $o;
    }
    protected function GetFormObject() {
        return $this->oForm;
    }
    /*----
      HISTORY:
	2018-07-24 Replaed call to GetRecordValues_asNative() with call to GetFieldArray_toWrite_native().
	  The former does not seem to exist in the blob-form class. Hope the latter is appropriate.
        2019-03-05 Wasn't sure if this was being called by anything, so throwing an exception in the first line.
        2019-03-09 Found it's being called by vcCartDataFieldGroup; removed exception.
    */
    public function GetFormArray() {
	$oForm = $this->GetFormObject();
	return $oForm->GetFieldArray_toWrite_native();
    }
    /* 2019-04-13 now using an object for this
    private $arMissed = array();
    protected function AddMissing(array $ar=NULL) {
	if (!is_null($ar)) {
	    $this->arMissed = array_merge($this->GetMissingArray(),$ar);
	}
    }
    */
    /*----
      NOTE: These two methods ^^ vv are essentially duplicates of methods in vcrShopCart.
	I *think* the way it works is that the Cart object is collating missing-lists from
	vcCartDataFieldGroup forms. Maybe missing-lists should be their own class?
    */
    /* 2019-04-13 now using an object for tracking missing fields
    public function GetMissingArray() {
	return $this->arMissed;
    } */
    /*
      NOTE: These methods are kind of a kluge necessitated by limitations in Ferreteria Forms v2.
    */
    
    protected function CopyFieldToArray(fcFormField $oField) {
	$id = $oField->GetNameString();
	$val = $oField->GetValue();
	$this->SetValue($id,$val);
    }
    protected function CopyArrayToField(fcFormField $oField) {
	$id = $oField->GetNameString();
	$oField->SetValue($this->GetValue($id));
    }
    /*----
      NOTES:
	I originally thought we'd only want to return values we actually received, otherwise
	stored values kept in one form-instance might overwrite values actually received by
	another form-instance (though I'm not sure what actual usage this might reflect).
	
	I also wrote --
	
	However, if we do that here, then we can't set anything beforehand; it gets wiped out.
	Canonically, the way around this would just be to have a form field for any additional
	data -- but in the case of the input-mode thingy (new vs. old), the value is set by
	pressing a button, which doesn't really conform to any pre-existing kind of form control.
	So I'd have to write one just for this.
	
	-- but I'm kind of vague on what I thought I meant.
	
	It now (2016-06-19) turns out that we *need* to keep the existing data because otherwise
	a form which is being re-displayed after having been saved earlier won't show the saved
	values, which is definitely problematic.
      HISTORY:
	2016-06-19 Commenting out the $this->GetBlobObject()->ClearArray() line (see NOTES).
    */
    protected function CopyFieldsToArray() {
	$oForm = $this->GetFormObject();
	// see NOTE - clear the output array before copying received data:
	//$this->GetBlobObject()->ClearArray();
	foreach ($oForm->GetFieldArray() as $key => $oField) {
	    $this->CopyFieldToArray($oField);
	}
    }
    // NOTE: Copies only the fields for which there are objects defined
    // TODO: Is this the same functionality as $this->FormObject()->LoadFields_fromBlob()?
    protected function CopyArrayToFields() {
	$oForm = $this->GetFormObject();
	foreach ($oForm->GetFieldArray() as $key => $oField) {
	    $this->CopyArrayToField($oField);
	}
    }
    
    // -- FORMS -- //
}



