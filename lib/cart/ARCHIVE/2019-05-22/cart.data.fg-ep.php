<?php
/*
  PURPOSE: Email-Phone form group for Cart Data
  HISTORY:
    2016-06-16 split off from cart.xdata.forms.php
*/
/*::::
  REQUIRES: PhoneNumber(), EmailAddress(), FieldName_Prefix()
*/
trait vtCartData_EmailPhone {

    // ++ SETUP ++ //

    // UNSTUB
    public function CreateFields() {
        parent::CreateFields();
	// invoke controls to add them to the form
	$this->GetEmailField();
	$this->GetPhoneField();
    }
    
    // -- SETUP -- //
    // ++ FIELD NAMES ++ //

    protected function FieldName_forContactPhone() {
	return $this->FieldName_Prefix().'addr-phone';
    }
    protected function FieldName_forContactEmail() {
	return $this->FieldName_Prefix().'addr-email';
    }
    
    // -- FIELD NAMES -- //
    // ++ FIELD OBJECTS ++ //

    private $oField_email = NULL;
    protected function GetEmailField() {
	if (is_null($this->oField_email)) {
	    echo 'CREATING EMAIL FIELD FOR '.get_class($this).'<br>';
	    $oField = new fcFormField_Text(
	      $this->GetFormObject(),
	      $this->FieldName_forContactEmail()
	      );
	      
	    $oField->SetDefault($this->DefaultEmail());

	    $oCtrl = new fcFormControl_HTML($oField,array('size'=>30));
	      $oCtrl->DisplayAlias('email address');

	      $this->oField_email = $oField;
	}

	return $this->oField_email;
    }
    private $oField_phone = NULL;
    protected function GetPhoneField() {
	if (is_null($this->oField_phone)) {
	    $oField = new fcFormField_Text(
	      $this->GetFormObject(),
	      $this->FieldName_forContactPhone()
	      );

	    $oCtrl = new fcFormControl_HTML($oField,array('size'=>20));
	      $oCtrl->DisplayAlias('phone number');
	      $oCtrl->SetRequired(FALSE);

	    $this->oField_phone = $oField;
	}
	return $this->oField_phone;
    }
    /*----
      HISTORY:
        2019-02-26 moving the field-invocations to CreateFields() because all fields for all subforms need to be loaded
          before we try to receive form data.
	2019-05-04 If this is just loading all active controls from the blob field, it shouldn't be happening here.
	  Commenting out.
    */
    /*
    protected function LoadContactFields() {
	// copy blob data to field objects
	$this->GetFormObject()->LoadFields_fromBlob();
    }*/

    // -- FIELD OBJECTS -- //
    // ++ FORM: FIELD VALUES ++ //
    
    public function GetEmailFieldValue() {
	$sfName = $this->FieldName_forContactEmail();
	return $this->GetValue($sfName);
    }
    public function GetPhoneFieldValue() {
	$sfName = $this->FieldName_forContactPhone();
	return $this->GetValue($sfName);
    }

    // -- FORM: FIELD VALUES -- //
    // ++ FORM I/O : CAPTURE ++ //

    /*	2019-05-04 See LoadContactFields()
    public function CaptureContact(fcFormResult $oFormStat) {
	$this->LoadContactFields();
	
	$oFormStat->AddBlank(	// 2019-05-04 this would be the current format, if it belonged here, which it kinda doesn't
	$this->AddMissing($arStat['blank']);
    }
    */
    
    // -- FORM I/O : CAPTURE -- //
    // ++ FORM I/O : RENDER ++ //
    
    /*----
      ACTION: Renders email/phone controls
      ASSUMES: They have already been invoked (added to the form)
    */
    public function RenderContact($doEdit) {
	// 2019-05-17 This shouldn't be needed anymore because fields should be already loaded
	//$this->LoadContactFields();
	
	$oTplt = $this->ContactTemplate($doEdit);
	$arCtrls = $this->GetFormObject()->RenderControls($doEdit);
	$oTplt->SetVariableValues($arCtrls);
	return $oTplt->RenderRecursive();
    }
    /*----
      2018-02-27 Commented out stuff that was conditional on whether user is logged in,
        because login controls now handle that internally
    */
    protected function RenderUserControls() {
	return $this->PageObject()->RenderLogin_Controls();
    }
    
    // -- FORM I/O : RENDER -- //
    // ++ FORM TEMPLATE ++ //
    
    protected function ContactTemplate($doEdit) {
	$htEmail = $this->FieldName_forContactEmail();
	$htPhone = $this->FieldName_forContactPhone();
	$htPhoneSfx = $doEdit?' (optional)':'';	// only show "optional" when editing
	$htUser = $this->RenderUserControls();

	$htPrecede = $this->ContactTemplate_PrecedingLines();
	$htFollow = $this->ContactTemplate_FollowingLines();

	$sTplt = <<<__END__
<table class="form-block" id="contact">
$htPrecede
<tr><td colspan=2 align=center><hr>$htUser<hr></td></tr>
<tr><td align=right valign=middle>Email:</td><td>[[$htEmail]]</td></tr>
<tr><td align=right valign=middle>Phone:</td><td>[[$htPhone]]$htPhoneSfx</td></tr>
$htFollow
</table>
__END__;
	return new fcTemplate_array('[[',']]',$sTplt);
    }
    // PURPOSE: for user-classes to override if they want to add additional lines before the main block
    protected function ContactTemplate_PrecedingLines() {
	return NULL;
    }
    // PURPOSE: for user-classes to override if they want to add additional lines after the main block
    protected function ContactTemplate_FollowingLines() {
	return "GOT TO HERE";
	return NULL;
    }
    
    // -- FORM TEMPLATE -- //

}
