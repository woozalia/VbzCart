<?php namespace ferreteria\classloader;

//$fp = dirname( __FILE__ );
//fcCodeModule::BasePath($fp.'/');

$om = $oLib->MakeModule('stock.php');
  $om->AddClass('vctStockLines');
$om = $oLib->MakeModule('stock-bin.info.php');
  $om->AddClass('vcqtStockBinsInfo');
$om = $oLib->MakeModule('stock-bin.logic.php');
  $om->AddClass('vctStockBins');
  $om->AddClass('vcrStockBin');
$om = $oLib->MakeModule('stock-line.info.php');
  $om->AddClass('vcqtStockLinesInfo');
$om = $oLib->MakeModule('stock-item.info.php');
  $om->AddClass('vcqtStockItemsInfo');
$om = $oLib->MakeModule('stock-title.info.php');
  $om->AddClass('vcqtStockTitlesInfo');
