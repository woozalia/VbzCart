<?php
/*
  HISTORY:
    2019-09-29 created for extracting relevant classes from app/home.php, to minimize confusion
      I kept looking for them in this folder and getting confused which class I was looking at.
*/
class vcqtStockTitlesInfo extends ferreteria\data\cSourcelessQuery {

    // ++ SETUP ++ //
/*
    // CEMENT
    public function GetKeyName() {
	return 'ID_Title';
    }
*/
    // CEMENT: cTabloid
    protected function SingleRowClass() : string { return 'vcqrStockTitleInfo'; }

    // -- SETUP -- //
    // ++ SQL ++ //
    
    static public function SQL_forTitleStatus() {
	$sql = <<<__END__
SELECT 
    ID_Title,
    SUM(sl.QtyForSale) AS QtyForSale,
    SUM(sl.QtyForShip) AS QtyForShip,
    SUM(sl.QtyExisting) AS QtyExisting
FROM
    (SELECT 
        st.ID,
            st.ID_Bin,
            i.ID_Title,
            IF(sb.isForSale, st.Qty, 0) AS QtyForSale,
            IF(sb.isForShip, st.Qty, 0) AS QtyForShip,
            st.Qty AS QtyExisting,
            st.CatNum,
            st.WhenAdded,
            st.WhenChanged,
            st.WhenCounted,
            st.Notes,
            sb.Code AS BinCode,
            sb.ID_Place,
            sp.Name AS WhName
    FROM
        ((stk_lines AS st
    LEFT JOIN stk_bins AS sb ON sb.ID = st.ID_Bin)
    LEFT JOIN stk_places AS sp ON sb.ID_Place = sp.ID)
    LEFT JOIN cat_items AS i ON st.ID_Item=i.ID
    WHERE
        (sb.WhenVoided IS NULL)
            AND (st.Qty <> 0)
            AND (sp.isActivated)) AS sl
WHERE
    QtyForSale > 0
GROUP BY sl.ID_Title
__END__;
	return $sql;
    }
    
    // -- SQL -- //
    // ++ RECORDS ++ //
    
    public function SelectRecords_forTitleStockStatus() {
	$sql = self::SQL_forTitleStatus();
	$rs = $this->FetchRecords($sql);
	return $rs;
    }
}
class vcqrStockTitleInfo extends ferreteria\data\cRecordDeluxe {
/* actually, not used... never mind
    public function GetQuantityForSale() {
	return $this->GetFieldValue('QtyForSale');
    } */
}
