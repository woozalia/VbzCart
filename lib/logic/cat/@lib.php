<?php namespace ferreteria\classloader;

//$fp = dirname( __FILE__ );
//fcCodeModule::BasePath($fp.'/');

define('KS_LOGIC_CLASS_LC_SUPPLIERS','vctSuppliers');
define('KS_LOGIC_CLASS_LC_ITEM_TYPES','vctItTyps');
define('KS_LOGIC_CLASS_LC_ITEM_OPTIONS','vctItemOptions');

$om = $oLib->MakeModule('dept.logic.php');
  $om->AddClass('vctDepts');
  $om->AddClass('vtTableAccess_Department');
$om = $oLib->MakeModule('dept.query.php');
  $om->AddClass('vctCatDepartments_queryable');

define('KS_CLASS_FOLDERS','vctFolders');
$om = $oLib->MakeModule('folder.logic.php');
  $om->AddClass(KS_CLASS_FOLDERS);

$om = $oLib->MakeModule('image.php');
  $om->AddClass('vctImages');
$om = $oLib->MakeModule('image-info.php');
  $om->AddClass('vcqtImagesInfo');
  $om->AddClass('vtTableAccess_ImagesInfo');

$om = $oLib->MakeModule('item.php');
  $om->AddClass('vcrItem');
  $om->AddClass('vctItems');
  $om->AddClass('vtTableAccess_Item');
$om = $oLib->MakeModule('item-type.php');
  $om->AddClass(KS_LOGIC_CLASS_LC_ITEM_TYPES);
  $om->AddClass('vtTableAccess_ItemType');
$om = $oLib->MakeModule('item-info.php');
  $om->AddClass('vcqtItemsInfo');
$om = $oLib->MakeModule('item-opt.php');
  $om->AddClass(KS_LOGIC_CLASS_LC_ITEM_OPTIONS);
    $om->AddClass('vtTableAccess_ItemOption');

$om = $oLib->MakeModule('ship-cost.php');
  $om->AddClass('clsShipCosts');
$om = $oLib->MakeModule('stats.php');
  $om->AddClass('fcStatsMgr');

$om = $oLib->MakeModule('supp.logic.php');
  $om->AddClass(KS_LOGIC_CLASS_LC_SUPPLIERS);
  $om->AddClass('vtTableAccess_Supplier');
$om = $oLib->MakeModule('supp.ittyps.php');
  $om->AddClass('vcqtSuppliertItemTypes');
$om = $oLib->MakeModule('supp.query.php');
  $om->AddClass('vctCatSuppliers_queryable');
  
$om = $oLib->MakeModule('title.logic.php');
  $om->AddClass('vctTitles');
  $om->AddClass('vcrTitle');
  $om->AddClass('vtTableAccess_Title');
$om = $oLib->MakeModule('title.info.php');
  $om->AddClass('vtQueryableTable_Titles');
  $om->AddClass('vcqtTitlesInfo');
  $om->AddClass('vtTitles_status');
$om = $oLib->MakeModule('title.dept.info.php');
  $om->AddClass('vcqtTitlesInfo_forDept');
$om = $oLib->MakeModule('title.topic.info.php');
  $om->AddClass('vcqtTitlesInfo_forTopic');
$om = $oLib->MakeModule('title.topics.info.php');
  $om->AddClass('vcqtTitlesInfo_forTopics');
$om = $oLib->MakeModule('title-topic.php');
  $om->AddClass('vctTitlesTopics');

$om = $oLib->MakeModule('topic.php');
  $om->AddClass('vctTopics');
