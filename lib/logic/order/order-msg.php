<?php
/*
  HISTORY:
    2014-09-28 split off from orders.php
    2016-11-04 updated to db.v2
*/

/*----------
  CLASS PAIR: order messages (table ord_msg)
*/
class vctOrderMsgs extends vcBasicTable {

    // ++ CEMENTING ++ //
    
    protected function GetTableName() : string { return 'ord_msg'; }
    protected function SingleRowClass() : string { return 'vcrOrderMsg'; }

    // -- CEMENTING -- //
    // ++ ACTIONS ++ //

    /*----
      ACTION: Adds a message to the order
      INPUT:
	$iMedia: ord_msg.ID_Media
    */
    public function Add(
      $idOrder,
      $idPackage,
      $idMedia,
      $sTxtFrom,
      $sTxtTo,
      $sSubject,
      $sMessage) {
	
	$db = $this->GetDatabase();
	$arIns = array(
	  'ID_Ord'	=> $idOrder,
	  'ID_Pkg'	=> $db->SanitizeValue($idPackage),	// might be NULL
	  'ID_Media'	=> $db->SanitizeValue($idMedia),
	  'TxtFrom'	=> $db->SanitizeValue($sTxtFrom),
	  'TxtTo'	=> $db->SanitizeValue($sTxtTo),
	  'TxtRe'	=> $db->SanitizeValue($sSubject),
	  'doRelay'	=> 'FALSE',	// 2010-09-23 this field needs to be re-thought
	  'WhenCreated'	=> 'NOW()',	// later: add this as an optional argument, if needed
	  'WhenEntered'	=> 'NOW()',
	  'WhenRelayed' => 'NULL',
	  'Message'	=> $db->SanitizeValue($sMessage)
	  );
	return $this->Insert($arIns);
    }

    // -- ACTIONS -- //
    // ++ CALCULATIONS ++ //

    function Record_forOrder($idOrder,$idType=NULL) {
    
	/* 2016-11-05 old code
	$sqlTbl = $this->NameSQL();
	$sqlFilt = "ID_Ord=$idOrder";
	if (!is_null($idType)) {
	    $sqlFilt = "($sqlFilt) AND (ID_Media=$idType)";
	}
	$rc = $this->DataSet("WHERE $sqlFilt ORDER BY WhenCreated DESC LIMIT 1");
	*/
	
	$sqlFilt = "ID_Ord=$idOrder";
	if (!is_null($idType)) {
	    $sqlFilt = "($sqlFilt) AND (ID_Media=$idType)";
	}
	$rc = $this->SelectRecords($sqlFilt,'WhenCreated DESC','LIMIT 1');
	
	$rc->NextRow();
	return $rc;
    }

    // -- CALCULATIONS -- //
}
class vcrOrderMsg extends vcBasicRecordRow {

    // ++ FIELD VALUES ++ //

    public function MessageText() {
	return $this->GetFieldValue('Message');
    }

    // -- FIELD VALUES -- //

}
