<?php
/*
  PART OF: VbzAdmin
  PURPOSE: classes for handling title-topic assignments
  HISTORY:
    2013-11-06 split off from SpecialVbzAdmin.main.php
*/
class vctTitlesTopics_admin extends vctTitlesTopics {
    protected function TitlesClass() : string { return KS_CLASS_CATALOG_TITLES; }
    protected function TopicsClass() : string { return KS_CLASS_CATALOG_TOPICS; }
}
