<?php
/*
  PURPOSE: business logic for managing Warehouses
  HISTORY:
    2016-01-08 started
    2017-03-27 now descending from vcBasic* classes instead of vcAdmin*
*/

class vctlWarehouses extends vcBasicTable {

    // CEMENT
    protected function SingleRowClass() : string { return 'vcrlWarehouse'; }
    // CEMENT
    protected function GetTableName() : string { return 'stk_whse'; }

}

class vcrlWarehouse extends vcBasicRecordRow {
    // PUBLIC so Restocks can display it
    public function NameString() : string { return $this->GetFieldValue('Name'); }
}
