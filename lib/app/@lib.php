<?php namespace ferreteria\classloader;
/*
PURPOSE: define locations for libraries using modloader.php, and load parts of Ferreteria that are always needed
FILE SET: VbzCart libraries
HISTORY:
  2013-08-29 created
  2014-01-20 moved MediaWiki-specific files into separate config-libs.php
  2018-01-31 now assuming that Ferreteria base has already been loaded
*/
//fcCodeLibrary::Load_byName('ferreteria.db.2');

//$fp = dirname( __FILE__ );
//fcCodeModule::BasePath($fp.'/');

$om = $oLib->MakeModule('app.php');
  $om->AddClass('vtApp');
$om = $oLib->MakeModule('admin.php');
  $om->AddClass('vcAppAdmin');
  $om->AddClass('vcMenuKiosk_admin');
  $om->AddClass('vcPageAdmin');
$om = $oLib->MakeModule('ajax.php');
  $om->AddClass('vcPageAJAX');
  $om->AddClass('vcPage_AJAX_ProcessStatus');
$om = $oLib->MakeModule('cart.php');
  $om->AddClass('vcAppShop_cart');
  $om->AddClass('vcPage_Cart');
$om = $oLib->MakeModule('cat.php');
  $om->AddClass('vcAppShop_catalog');
  $om->AddClass('vcMenuKiosk_catalog');
  $om->AddClass('vcCatalogPage');
$om = $oLib->MakeModule('ckout.php');
  $om->AddClass('vcAppShop_checkout');
$om = $oLib->MakeModule('home.php');
  $om->AddClass('vcAppShop_home');
  $om->AddClass('vcPageHome');
$om = $oLib->MakeModule('search.php');
  $om->AddClass('vcAppShop_search');
  $om->AddClass('vcPageSearch');
$om = $oLib->MakeModule('shop.php');
  $om->AddClass('vcAppShop');
  $om->AddClass('vcPage_shop');
  $om->AddClass('vcTag_body_shop');
  $om->AddClass('vcPageContent_shop');
$om = $oLib->MakeModule('static.php');
  $om->AddClass('vcAppStatic');
$om = $oLib->MakeModule('topic.php');
  $om->AddClass('vcAppShop_topic');
  $om->AddClass('vcPageTopic');
