<?php
/*
  FILE: ckout.php
  HISTORY:
    2019-06-13 split off app & kiosk classes from page/ckout/page.php
*/
class vcAppShop_checkout extends vcAppShop {
    protected function GetPageClass() {
	return 'vcPageCkout';
    }
    // TODO: not sure a Kiosk is needed for this class
    protected function GetKioskClass() {
	return 'vcMenuKiosk_ckout';	// apparently this is actually needed...
    }
}
class vcMenuKiosk_ckout extends fcMenuKiosk_admin {
    public function GetBasePath() {
	return vcGlobals::Me()->GetWebPath_forCheckoutPage();
    }
}
