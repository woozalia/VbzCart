<?php
/*
  PURPOSE: handles retrieval of static pages
    This could probably be less computing-intensive, especially if no security is needed,
    but for now I'l going with just setting it up as a standard app class. Refine later.
    (Simplest refinement is to make it descend from fcApp instead of fcAppShop,
    but fcApp needs to be split a bit more so that it doesn't require page, kiosk, and
    database classes.)
  HISTORY:
    2019-06-13 created
    2019-06-15 now descends from vcAppShop instead of fcAppStandardBasic
*/
class vcAppStatic extends vcAppShop {
}
