<?php
/*
  FILE: vbz-data.php
  PURPOSE: vbz-specific data classes - generic
  HISTORY:
    2011-01-25 split off from store.php in an attempt to resolve dependency conflicts
    2013-11-15 moved clsVbzData from store.php to here (vbz-data.php)
    2014-04-04 moved clsCacheFile_vbz here from store.php (not sure if this is *exactly* the place for it...(
    2015-09-06 splitting a couple of classes:
        clsVbzTable:
            vcVbzTable_base will define a couple of common methods
            vcVbzTable_shop won't have link-building stuff built in anymore
            vcAdminTable (was vcVbzTable_admin) will use traits to add admin functionality
        clsVbzRecs:
            vcVbzRecs_shop won't have link-building stuff built in anymore
            vcVbzRecs_admin will use traits to add admin functionality
                and will no longer descend from clsDataRecord_Menu
    2016-10-01 Changes to ferreteria:app-user.php (switching from db.v1 to db.v2) require changes here.
    2016-10-24 ...aaaand apparently the conversion was not complete; finishing that. More name and structure changes.
      vcVbzTable_base -> vcBasicTable
      vcVbzTable_shop -> vcShopTable
      vcVbzTable_admin -> vcAdminTable (2017-01-04)
    2020-02-24 Added vcAdminRecordset (empty class for now) just so I'd stop wondering which class to descend admin recordsets from.
      Removed some commented-out classes.
      Removed Touch($iCaller) from vcBasicTable, because nothing uses it (we're not doing that kind of caching anymore).
*/

/*----
  PURPOSE: standard table wrapper class for VbzCart -- adds a service useful for caching
  TODO: maybe that should be in a descendant
*/
abstract class vcBasicTable extends ferreteria\data\cStandardTableSingleKey implements ferreteria\data\ifWriteToTable {}
abstract class vcBasicRecordset extends ferreteria\data\cRecordRowsKeyed {}
abstract class vcBasicRecordRow extends ferreteria\data\cRecordDeluxe {}

// SHOPPING data types - just aliases for now

abstract class vcShopTable extends vcBasicTable {}
class vcShopRecordRow extends vcBasicRecordRow {}
class vcShopRecordset extends vcBasicRecordset {}

// ADMIN data types

abstract class vcAdminTable extends vcBasicTable implements fiEventAware, fiLinkableTable {
    use ftLinkableTable;
}
class vcAdminRecordRow extends vcBasicRecordRow implements fiLinkableRecord, ferreteria\data\ifEditableRecord {
    use ftLinkableRecord;
}
abstract class vcAdminRecordset extends vcBasicRecordset {
    use ferreteria\data\tRowsViewer;
}
