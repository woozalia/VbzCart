<?php namespace ferreteria\classloader;

define('KS_CLASS_SHOP_SUPPLIERS','vctSuppliers_shop');
define('KS_CLASS_SHOP_DEPARTMENTS','vctDepts_shop');

//$fp = dirname( __FILE__ );
//fcCodeModule::BasePath($fp.'/');

$om = $oLib->MakeModule('dept.php');
  $om->AddClass(KS_CLASS_SHOP_DEPARTMENTS);
  $om->AddClass('vcrDept_shop');
$om = $oLib->MakeModule('image.php');
  $om->AddClass('vctImages_StoreUI');
$om = $oLib->MakeModule('supp.php');
  $om->AddClass(KS_CLASS_SHOP_SUPPLIERS);
$om = $oLib->MakeModule('supp.trait.php');
  $om->AddClass('vtrSupplierShop');
$om = $oLib->MakeModule('title.php');
  $om->AddClass('vtTableAccess_Title_shop');
  $om->AddClass('vctShopTitles');
  $om->AddClass('vtrTitle_shop');
$om = $oLib->MakeModule('title.dept.info.php');
  $om->AddClass('vcqtTitlesInfo_forDept_shop');
$om = $oLib->MakeModule('title.topic.info.php');
  $om->AddClass('vcqtTitlesInfo_forTopic_shop');
$om = $oLib->MakeModule('title-topic.php');
  $om->AddClass('vctTitlesTopics_shop');
$om = $oLib->MakeModule('topic.php');
  $om->AddClass('vctShopTopics');
  $om->AddClass('vcrShopTopic');
$om = $oLib->MakeModule('widgets.php');
  $om->AddClass('vcHideableSection');
